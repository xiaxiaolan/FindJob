SET SESSION FOREIGN_KEY_CHECKS=0;

/* Drop Tables */

DROP TABLE IF EXISTS SCAN_RECORD;
DROP TABLE IF EXISTS ANTI_FAKE;
DROP TABLE IF EXISTS QR_CODE;
DROP TABLE IF EXISTS MEDICINE_INFO;
DROP TABLE IF EXISTS COMPANY_INFO;
DROP TABLE IF EXISTS ACCOUNT_INFO;
DROP TABLE IF EXISTS quanxian;
DROP TABLE IF EXISTS role;
DROP TABLE IF EXISTS user;




/* Create Tables */

CREATE TABLE ACCOUNT_INFO
(
	accountId int(11) NOT NULL AUTO_INCREMENT,
	accountName varchar(20) NOT NULL,
	password varchar(45) NOT NULL,
	PRIMARY KEY (accountId),
	UNIQUE (accountName)
);


CREATE TABLE ANTI_FAKE
(
	anti_fake_Id int(11) NOT NULL AUTO_INCREMENT,
	anti_fake_code varchar(20) NOT NULL,
	flag int NOT NULL,
	medicineId int(11) NOT NULL,
	PRIMARY KEY (anti_fake_Id),
	UNIQUE (anti_fake_code)
);


CREATE TABLE COMPANY_INFO
(
	comId int(11) NOT NULL AUTO_INCREMENT,
	accountId int(11) NOT NULL,
	comName varchar(30),
	faren varchar(20),
	phone varchar(15),
	registerTime varchar(15),
	comSize int(11),
	businessLicense varchar(30),
	industry varchar(20),
	describe1 text,
	address varchar(50),
	comType varchar(20),
	PRIMARY KEY (comId),
	UNIQUE (accountId)
);


CREATE TABLE MEDICINE_INFO
(
	medicineId int(11) NOT NULL AUTO_INCREMENT,
	medName varchar(30),
	content text,
	function1 text,
	usage1 text,
	effect text,
	jinji varchar(50),
	notify text,
	medSize varchar(10),
	factoryName varchar(30),
	pici int,
	permissionCode varchar(30),
	expirationDate varchar(10),
	produceDate varchar(20),
	comId int(11) NOT NULL,
	PRIMARY KEY (medicineId)
);


CREATE TABLE QR_CODE
(
	qrCodeId int(11) NOT NULL AUTO_INCREMENT,
	codeImg varchar(50) NOT NULL,
	medicineId int(11) NOT NULL,
	PRIMARY KEY (qrCodeId),
	UNIQUE (codeImg),
	UNIQUE (medicineId)
);


CREATE TABLE quanxian
(
	id int(11) NOT NULL,
	roleid int(11) NOT NULL
);


CREATE TABLE role
(
	roleid int(11) NOT NULL AUTO_INCREMENT,
	rolename varchar(20) NOT NULL,
	miaoshu varchar(45),
	PRIMARY KEY (roleid),
	UNIQUE (rolename)
);


CREATE TABLE SCAN_RECORD
(
	id int(11) NOT NULL,
	wechat_account varchar(20) NOT NULL,
	anti_fake_Id int(11) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (anti_fake_Id)
);


CREATE TABLE user
(
	id int(11) NOT NULL AUTO_INCREMENT,
	username varchar(20) NOT NULL,
	password varchar(100) NOT NULL,
	PRIMARY KEY (id),
	UNIQUE (username)
);



/* Create Foreign Keys */

ALTER TABLE COMPANY_INFO
	ADD FOREIGN KEY (accountId)
	REFERENCES ACCOUNT_INFO (accountId)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE SCAN_RECORD
	ADD FOREIGN KEY (anti_fake_Id)
	REFERENCES ANTI_FAKE (anti_fake_Id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE MEDICINE_INFO
	ADD FOREIGN KEY (comId)
	REFERENCES COMPANY_INFO (comId)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE ANTI_FAKE
	ADD FOREIGN KEY (medicineId)
	REFERENCES MEDICINE_INFO (medicineId)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE QR_CODE
	ADD FOREIGN KEY (medicineId)
	REFERENCES MEDICINE_INFO (medicineId)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE quanxian
	ADD FOREIGN KEY (roleid)
	REFERENCES role (roleid)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;


ALTER TABLE quanxian
	ADD FOREIGN KEY (id)
	REFERENCES user (id)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
;



