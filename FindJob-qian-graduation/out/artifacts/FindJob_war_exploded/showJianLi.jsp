<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<html>
<head>
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>简历</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>

	<%--模态框--%>
	<link rel="stylesheet" href="ditu/bootstrap1.min.css">
	<script src="ditu/bootstrap.min.js"></script>

	<style type="text/css">
		/*form*/
	    h1, form, fieldset, legend, ol, li {
			margin: 0;
			padding: 0;
		}
		/*定义内容的样式*/
		form#jianli {
			background:#CCCCCC;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			padding: 20px;
			width: 400px;
			margin:auto;
		}
		form#jianli fieldset {
			border: none;
			margin-bottom: 10px;
		}
		form#jianli fieldset:last-of-type { margin-bottom: 0; }
		form#jianli legend {
			color:#993399;
			font-size: 16px;
			font-weight: bold;
			padding-bottom: 10px;
			text-shadow: 0 1px 1px #c0d576;
		}

		form#jianli fieldset fieldset legend {
			color: #111111;
			font-size: 13px;
			font-weight: normal;
			padding-bottom: 0;
		}
		form#jianli ol li {
			background: #b9cf6a;
			background: rgba(255, 255, 255, .3);
			border-color: #e3ebc3;
			border-color: rgba(255, 255, 255, .6);
			border-style: solid;
			border-width: 2px;
			-webkit-border-radius: 5px;
			line-height: 30px;
			list-style: none;
			padding: 5px 10px;
			margin-bottom: 2px;
		}
		form#jianli ol ol li {
			background: none;
			border: none;
			float: left;
		}
		form#jianli label {
			float: left;
			font-size: 13px;
			width: 100px;
			vertical-align:top;
		}
		form#jianli fieldset fieldset label {
			background: none no-repeat left 50%;
			line-height: 20px;
			padding: 0 0 0 3px;
			width: auto;
		}
		form#jianli fieldset fieldset label:hover { cursor: pointer; }
		form#jianli input:not([type=radio]), form#payment textarea {
			background: #ffffff;
			border: #FC3 solid 1px;
			-webkit-border-radius: 3px;
			font: italic 13px Georgia, "Times New Roman", Times, serif;
			outline: none;
			padding: 5px;
			width: 200px;
		}
		form#jianli input:not([type=submit]):focus, form#payment textarea:focus {
			background: #eaeaea;
			border: #F00 solid 1px;
		}
		form#jianli input[type=radio] {
			float: left;
			margin-right: 0px;
			display:inline;
			vertical-align: top}
	</style>

	<%--搜索框--%>
	<style type="text/css">
		input[id="name2"],input[id="age2"],input[id="phone2"],input[id="email2"],input[id="hopedzhiwei2"],input[id="hopedhangye2"],input[id="phone1"]{
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 152px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 15px;
		}
		input[id="name2"]:hover,
		input[id="name2"]:focus,
		input[id="age2"]:hover,
		input[id="age2"]:focus,
		input[id="phone2"]:hover,
		input[id="phone2"]:focus,
		input[id="email2"]:hover,
		input[id="email2"]:focus,
		input[id="hopedzhiwei2"]:hover,
		input[id="hopedzhiwei2"]:focus,
		input[id="hopedhangye2"]:hover,
		input[id="hopedhangye2"]:focus,
		input[id="phone1"]:hover,
		input[id="phone1"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>

	<style type="text/css">
		body{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>

<body>

<table id="dg" title="全部简历" class="easyui-datagrid" style="width:100%;height:100%"
	   url="/showJianli"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="true" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'jianliid',width:40,align:'center',sortable:true" sorter="numberSort">id</th>
		<th data-options="field:'name',width:65,align:'center'">姓名</th>
		<th data-options="field:'age',width:50,align:'center'">年龄</th>
		<th data-options="field:'sex',width:50,align:'center'">性别</th>
		<th data-options="field:'email',width:100,align:'center'">邮箱</th>
		<th data-options="field:'phone',width:115,align:'center'">电话</th>
		<th data-options="field:'address',width:70,align:'center'">地址</th>
		<th data-options="field:'startworktime',width:90,align:'center'">工作时间</th>
		<th data-options="field:'workstatus',width:90,align:'center'">目前状态</th>
		<th data-options="field:'hopedhangye',width:85,align:'center'">希望行业</th>
		<th data-options="field:'hopedzhiwei',width:100,align:'center'">希望职业</th>
		<th data-options="field:'hopedworktype',width:100,align:'center'">希望工作type</th>
		<th data-options="field:'hopedsalary',width:85,align:'center'">预期薪资</th>
		<th data-options="field:'hopedaddress',width:82,align:'center'">希望地址</th>
		<th data-options="field:'useraccountid',width:100,align:'center',sortable:true" sorter="numberSort">用户账号</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:16px 20px 10px 20px">
	<span>
		<input id="name2" placeholder="姓名" name="name2" onkeyup="nameSearch()"/>
		<input id="age2" placeholder="年龄>=" name="age2" onkeyup="ageSearch()"/>
        <input id="phone2" placeholder="电话" name="phone2" onkeyup="phoneSearch()"/>
        <input id="email2" placeholder="邮箱" name="email2" onkeyup="emailSearch()"/>
        <input id="hopedzhiwei2" placeholder="希望的职位" name="hopedzhiwei2" onkeyup="hopedzhiweiSearch()"/>
        <input id="hopedhangye2" placeholder="希望的行业" name="hopedhangye2" onkeyup="hopedhangyeSearch()"/>
		<div></div>

		<select id="hopedsalary2" onchange="hopedsalarySearch()" style="height: 33px;width: 151.5px;margin-bottom:5px;margin-left:15px;font-family:sans-serif;">
              <option value="" selected="selected"></option>
			  <option value="1">2k以下</option>
              <option value="2">2k-5k</option>
              <option value="3">5k-10k</option>
              <option value="4">10k-15k</option>
              <option value="5">15k-25k</option>
              <option value="6">25k-50k</option>
              <option value="7">50k以上</option>
		</select>
		<select id="startworktime2" onchange="startworktimeSearch()" style="height: 33px;width: 151.5px;margin-bottom:5px;margin-left:15px;font-family:sans-serif;">
              <option value="" selected="selected"></option>
              <option value="1">1年</option>
              <option value="2">2年</option>
              <option value="3">3年</option>
			  <option value="4">4年</option>
              <option value="5">5年</option>
              <option value="6">6年</option>
			  <option value="7">7年</option>
              <option value="8">8年</option>
              <option value="9">9年</option>
			  <option value="10">10年</option>
              <option value="11">10年以上</option>
              <option value="12">应届毕业生</option>
		</select>
		<select id="hopedworktype2" onchange="hopedworktypeSearch()" style="height: 33px;width: 151.5px;margin-bottom:5px;margin-left:15px;font-family:sans-serif;">
              <option value="" selected="selected"></option>
			  <option value="1">实习</option>
              <option value="2">全职</option>
              <option value="3">兼职</option>
		</select>
		<select id="sex2" onchange="sexSearch()" style="height: 33px;width: 151.5px;margin-bottom:5px;margin-left:15px;font-family:sans-serif;">
              <option value="" selected="selected"></option>
              <option value="1">男</option>
              <option value="0">女</option>
		</select>
	</span>
	<input id="phone1" style="margin-left:17px" placeholder="注册phone" name="phone1" onkeyup="phone1Search()"/>
	<div></div>
	<a style="margin-left:15px;;margin-top:3px" href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="searchAll()">Search</a>
	<a style="margin-left:5px;margin-top:3px" href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteJianli()">Detete</a>
	<a style="margin-left:5px;margin-top:3px" href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newJianli()">Add</a>
	<a style="margin-left:5px;margin-top:3px" href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editJianli()">Edit</a>

</div>

<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons"></div>
<div id="dlg-buttons"></div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="newResource">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<%--head--%>
			<div class="modal-header">
				<h4 class="modal-title"  id="gridSystemModalLabel">修改资源</h4>
			</div>
			<%--body--%>
			<div class="modal-body">
				<div class="container-fluid">
					<form id="jianli" action="" method="post" name="jianli">
						<fieldset>
							<legend>简历信息</legend>
							<ol>
								<li style="position: absolute">
									<input id="jianliid" name="jianliid" type="hidden">
								</li>
								<li>
									<label for="name">姓名：</label>
									<input id="name" name="name" type="text" required>
								</li>
								<li>
									<label for="age">年龄：</label>
									<input id="age" name="age" type="text" maxlength="2" required>
								</li>
								<li>
									<fieldset>

										<ol>
											<li>
												<label for="sex" id="sex">性别：</label>
												<input id="men" name="sex"  type="radio" value="1">
												<label for="men" >男</label>
											</li>
											<li>
												<input id="women"  name="sex" type="radio" value="0">
												<label for="women">女</label>
											</li>
										</ol>
									</fieldset>
								</li>
								<li>
									<label for="email">邮箱地址：</label>
									<input id="email" name="email" type="text" required>
								</li>
								<li>
									<label for="phone">电话号码：</label>
									<input id="phone" name="phone" type="text" required>
								</li>
								<li>
									<label for="address">籍贯：</label>
									<input id="address" name="address" type="text" required>
								</li>
								<li>
									<label for="startworktime">工作经历</label>
									<select id="startworktime" name="startworktime">
										<option value =""></option>
										<option value ="应届毕业生">应届毕业生</option>
										<option value ="1年">1年</option>
										<option value ="2年">2年</option>
										<option value ="3年">3年</option>
										<option value ="4年">4年</option>
										<option value ="5年">5年</option>
										<option value ="6年">6年</option>
										<option value ="7年">7年</option>
										<option value ="8年">8年</option>
										<option value ="9年">9年</option>
										<option value ="10年">10年</option>
										<option value ="10年以上">10年以上</option>
									</select>
								</li>
								<li>
									<label for="workstatus">目前状态</label>
									<select id="workstatus" name="workstatus">
										<option value =""></option>
										<option value ="我是应届毕业生">刚毕业，还没工作</option>
										<option value ="我目前正在职，正考虑换个新环境">在职，正在考虑跳槽</option>
										<option value ="我目前已离职，可快速到岗">离职，可快速到岗</option>
										<option value ="我暂时不想找工作">暂时不想找工作</option>
									</select>
								</li>
								<li>
									<label for="hopedhangye">期望行业</label>
									<select id="hopedhangye" name="hopedhangye">
										<option value =""></option>
										<option value ="O2O">O2O</option>
										<option value ="企业服务">企业服务</option>
										<option value ="健康医疗">健康医疗</option>
										<option value ="在线旅游">在线旅游</option>
										<option value ="安全">安全</option>
										<option value ="搜索">搜索</option>
										<option value ="教育">教育</option>
										<option value ="文化艺术">文化艺术</option>
										<option value ="游戏">游戏</option>
										<option value ="生活服务">生活服务</option>
										<option value ="电子商务">电子商务</option>
										<option value ="硬件">硬件</option>
										<option value ="社交">社交</option>
										<option value ="移动互联网">移动互联网</option>
										<option value ="金融互联网">金融互联网</option>

									</select>
								</li>
								<li>
									<label for="hopedzhiwei">期望职位：</label>
									<input id="hopedzhiwei" name="hopedzhiwei" type="text" required>
								</li>
								<li>
									<fieldset>
										<ol>
											<li>
												<label for="hopedworktype" id="hopedworktype">期望工作性质：</label>
												<input id="shixi" name="hopedworktype"  type="radio" value="实习">
												<label for="shixi">实习</label>
											</li>
											<li>
												<input id="jianzhi"  name="hopedworktype" type="radio" value="兼职">
												<label for="jianzhi">兼职</label>
											</li>
											<li>
												<input id="quanzhi"  name="hopedworktype" type="radio" value="全职">
												<label for="quanzhi">全职</label>
											</li>
										</ol>
									</fieldset>
								</li>
								<li>
									<label for="hopedsalary">期望薪资</label>
									<select id="hopedsalary" name="hopedsalary">
										<option value =""></option>
										<option value ="2k以下">小于2000</option>
										<option value ="2k-5k">2000~5000</option>
										<option value ="5k-10k">5000~10000</option>
										<option value ="10k-15k">10000~15000</option>
										<option value ="15k-25k">15000~25000</option>
										<option value ="25k-50k">25000~50000</option>
										<option value ="50k以上" selected="selected">50000以上</option>

									</select>
								</li>
								<li>
									<label for="hopedaddress">期望工作地址</label>
									<select id="hopedaddress" name="hopedaddress">
										<option value =""></option>
										<option value ="上海">上海</option>
										<option value ="北京">北京</option>
										<option value ="南京">南京</option>
										<option value ="天津">天津</option>
										<option value ="太原">太原</option>
										<option value ="广州">广州</option>
										<option value ="成都">成都</option>
										<option value ="昆明">昆明</option>
										<option value ="杭州">杭州</option>
										<option value ="武汉">武汉</option>
										<option value ="沈阳">沈阳</option>
										<option value ="济南">济南</option>
										<option value ="深圳">深圳</option>
										<option value ="苏州">苏州</option>
										<option value ="贵阳">贵阳</option>
										<option value ="郑州">郑州</option>
										<option value ="重庆">重庆</option>
										<option value ="长春">长春</option>
										<option value ="长沙">长沙</option>
										<option value ="青岛">青岛</option>
									</select>
								</li>
								<%--<li>
									<label for="useraccountid">账号id：</label>
									<input id="useraccountid" name="useraccountid" type="text" required>
								</li>--%>
								<li>
									<label for="useraccountid">账号id</label>
									<select id="useraccountid" name="useraccountid">
									</select>
								</li>
							</ol>
						</fieldset>

						<%--<fieldset>
							<button type="submit">提交</button>
						</fieldset>--%>
					</form>
				</div>
			</div>
			<%--bottom保存取消按钮--%>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
				<button type="button" class="btn btn-xs btn-green" id="save" onclick="save()">保 存</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
    //弹出框add
    function newJianli(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length ==0) {
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("添加简历信息");
            $('form[id=jianli]').attr('action',"/addJianli");
            $('#jianli').form('clear');
            document.jianli.useraccountid.disabled=false;

            //去后台请求还没被注册过的用户类型id type=1，且未在简历表中引用;每次加载前先清空
            var length1=document.getElementById("useraccountid").length;
            if (length1!=null){
                for (var i;i<=length1;i++) {
                    $("#useraccountid").options.remove(i);
                }
			}
            $.get("selectJianAccountIds", function (data, status) {
                for(var i=0;i<data.length;i++){
					$("#useraccountid").append('<option value ="'+data[i]+'">'+data[i]+'</option>');
				}
			});
        }else {
            $.messager.alert("提示", "请清空选中的行", "info");
        }
    }


    //check的方法
    function initradio(rName,rValue){
        var rObj = document.getElementsByName(rName);
        for(var i = 0;i < rObj.length;i++){
            if(rObj[i].value == rValue){
                rObj[i].checked =  'checked';
            }
        }
    }
    //弹出框edit
    function editJianli(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行用户进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的用户", "info");
        }else{
            $('#newResource').modal();
            $("#gridSystemModalLabel").text("修改简历信息");
            $('form[id=jianli]').attr('action',"/updateJianli");
            document.jianli.useraccountid.disabled=true;
            //1、获取选中的行
            var id=rows[0].jianliid;
            //2、将存在session的值赋值给表单
            $.get("showJianliById?jianliid=" + id, function (data, status) {
                //3、赋值给表单,必须通过name属性
                document.jianli.jianliid.value=id;
                document.jianli.name.value=data.name;
                document.jianli.age.value=data.age;
                document.jianli.email.value=data.email;
                document.jianli.phone.value=data.phone;
                document.jianli.address.value=data.address;
                document.jianli.hopedzhiwei.value=data.hopedzhiwei;

                initradio('sex',data.sex);
                initradio('hopedworktype',data.hopedworktype);

                //document.jianli.useraccountid.value=data.useraccountid;
                $("#useraccountid").append('<option value ="'+data.useraccountid+'" selected="selected">'+data.useraccountid+'</option>')

                $("#startworktime option[value='"+data.startworktime+"']").attr("selected","selected");
                $("#hopedhangye option[value='"+data.hopedhangye+"']").attr("selected","selected");
                $("#hopedsalary option[value='"+data.hopedsalary+"']").attr("selected","selected");
                $("#hopedaddress option[value='"+data.hopedaddress+"']").attr("selected","selected");
                $("#workstatus option[value='"+data.workstatus+"']").attr("selected","selected");
            });
        }
    }

    //点击模态框的保存之后，请求后台并将这个模态框关闭
    function save() {
        $('#jianli').submit();
        $('#newResource').modal('hide');
        //存一个update/add返回的的值，判断，弹框
    }

//单个注册电话搜索
    function phone1Search(){
        var phone1=$('#phone1').val();
        $("#dg").datagrid("options").url = "/searchJianliByPhone?phone1=" + phone1;
        $("#dg").datagrid('reload');
	}


    //动态匹配的搜索
    function all() {
        var name=$('#name2').val();
        var age=$('#age2').val();
        var phone=$('#phone2').val();
        var email=$('#email2').val();
        var hopedzhiwei=$('#hopedzhiwei2').val();
        var hopedhangye=$('#hopedhangye2').val();

        var sex=$('#sex2 option:selected').val();//0和1
        var hopedsalary=$('#hopedsalary2 option:selected').text();
        var startworktime=$('#startworktime2 option:selected').text();
        var hopedworktype=$('#hopedworktype2 option:selected').text();
        $("#dg").datagrid("options").url = "/searchJianliByConditions?name="+name+"&sex="+sex+"&age="+age+"&phone="+phone+"&email="+email+"&startworktime="+startworktime
			                                +"&hopedhangye="+hopedhangye+"&hopedworktype="+hopedworktype+"&hopedsalary="+hopedsalary+"&hopedzhiwei="+hopedzhiwei;
        $("#dg").datagrid('reload');
	}
    //绑定搜索链接按钮点击事件	，点击搜索时发送请求
    function searchAll(){
        all();
    }
    function nameSearch(){  all();}
    function ageSearch(){   all();}
    function phoneSearch(){  all();}
    function emailSearch(){  all();}
    function hopedzhiweiSearch(){  all();}
    function hopedhangyeSearch(){  all();}
    function hopedsalarySearch(){  all();}
    function startworktimeSearch(){  all();}
    function hopedworktypeSearch(){  all();}
    function sexSearch(){  all();}



//删除的ajax请求
    function deleteJianli(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.jianliid);
            }
        });
        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {
                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
                    $.get("/deleteJianli?jianliIds=" + userid, function (data, status) {
                        if (data == "true") {
                            $.messager.alert("温馨提示", "删除成功", 'info');
                            $("#dg").datagrid('reload');
                        }
                        if (data == "false") {
                            $.messager.alert("温馨提示", "删除失败", 'error');
                        }
                    });
                }
            })
        }else {
            $.messager.alert("温馨提示", "选择要删除的行", 'info');
        }
    }



    //自定义整数排序
    function numberSort(a,b){
        var number1 = parseFloat(a);
        var number2 = parseFloat(b);
        return (number1 > number2 ? 1 : -1);
    }

</script>

<%--返回 add/update  "${sessionScope.user.username}" 的消息--%>
<c:choose>
	<c:when  test="${addJianli  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加成功!','info');
		</script>
	</c:when>

	<c:when  test="${addJianli  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>


<c:choose>
	<c:when  test="${updateJianli  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改成功!','info');
		</script>
	</c:when>

	<c:when  test="${updateJianli  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>
</body>
</html>