﻿<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="shiro" uri="http://shiro.apache.org/tags" %>
<html>
<head>
<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="copyright" content="All Rights Reserved, Copyright (C) 2013, Wuyeguo, Ltd." />
    <title>招聘后台管理系统</title>
    <link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="css/wu.css" />
    <link rel="stylesheet" type="text/css" href="css/icon.css" />
    <script type="text/javascript" src="easyui/jquery.min.js"></script>
    <script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>

    <style>
        #firstPage{
           text-align: center;
            width: 100%;
            height: 100%;
    }

    #firstPage{
        background-image: url("img/welcom.png");
        background-size: 100% 100%;
        background-repeat: no-repeat;
        -moz-background-size: 100% 100%;
    }

    .letter-container i {
        color: #fff;
        filter: alpha(opacity=100);
        -moz-opacity: 1;
        opacity: 1;
        text-shadow: 0px 0px 2px #fff,1px 1px 4px rgba(0,0,0,0.7);
        -webkit-transition: all 0.3s linear;
        -moz-transition: all 0.3s linear;
        -o-transition: all 0.3s linear;
        -ms-transition: all 0.3s linear;
        transition: all 0.3s linear;
        -webkit-animation: sharpen 0.9s linear backwards;
        -moz-animation: sharpen 0.9s linear backwards;
        -ms-animation: sharpen 0.9s linear backwards;
        animation: sharpen 0.9s linear backwards;
        font-size: 50px;
        font-family: 宋体;
        font-style: normal;
    }

    .letter-container i:nth-child(1) {
        -webkit-animation-delay: 0.5s;
        -moz-animation-delay: 0.5s;
        -ms-animation-delay: 0.5s;
        animation-delay: 0.5s;
    }

    .letter-container i:nth-child(2) {
        -webkit-animation-delay: 0.65s;
        -moz-animation-delay: 0.65s;
            -ms-animation-delay: 0.65s;
        animation-delay: 0.65s;
    }

    .letter-container i:nth-child(3) {
        -webkit-animation-delay: 0.8s;
        -moz-animation-delay: 0.8s;
        -ms-animation-delay: 0.8s;
        animation-delay: 0.8s;
    }

    .letter-container i:nth-child(4) {
        -webkit-animation-delay: 0.95s;
        -moz-animation-delay: 0.95s;
        -ms-animation-delay: 0.95s;
        animation-delay: 0.95s;
    }

    .letter-container i:nth-child(5) {
        -webkit-animation-delay: 1.1s;
        -moz-animation-delay: 1.1s;
        -ms-animation-delay: 1.1s;
        animation-delay: 1.1s;
    }

    .letter-container i:nth-child(6) {
        -webkit-animation-delay: 1.25s;
        -moz-animation-delay: 1.25s;
        -ms-animation-delay: 1.25s;
        animation-delay: 1.25s;
    }

    .letter-container i:nth-child(7) {
        -webkit-animation-delay: 1.4s;
        -moz-animation-delay: 1.4s;
        -ms-animation-delay: 1.4s;
        animation-delay: 1.4s;
    }

    .letter-container i:nth-child(8) {
        -webkit-animation-delay: 1.55s;
        -moz-animation-delay: 1.55s;
        -ms-animation-delay: 1.55s;
        animation-delay: 1.55s;
    }

    .letter-container i:nth-child(9) {
        -webkit-animation-delay: 1.7s;
        -moz-animation-delay: 1.7s;
        -ms-animation-delay: 1.7s;
        animation-delay: 1.7s;
    }

    .letter-container i:nth-child(10) {
        -webkit-animation-delay: 1.85s;
        -moz-animation-delay: 1.85s;
        -ms-animation-delay: 1.85s;
        animation-delay: 1.85s;
    }

    .letter-container i:nth-child(11) {
        -webkit-animation-delay: 2s;
        -moz-animation-delay: 2s;
        -ms-animation-delay: 2s;
        animation-delay: 2s;
    }

    .letter-container i:nth-child(12) {
        -webkit-animation-delay: 2.15s;
        -moz-animation-delay: 2.15s;
        -ms-animation-delay: 2.15s;
        animation-delay: 2.15s;
    }

    .letter-container i:nth-child(13) {
        -webkit-animation-delay: 2.3s;
        -moz-animation-delay: 2.3s;
        -ms-animation-delay: 2.3s;
        animation-delay: 2.3s;
    }

    .letter-container i:nth-child(14) {
        -webkit-animation-delay: 2.45s;
        -moz-animation-delay: 2.45s;
        -ms-animation-delay: 2.45s;
        animation-delay: 2.45s;
    }

    .letter-container i:nth-child(15) {
        -webkit-animation-delay: 0.75s;
        -moz-animation-delay: 0.75s;
        -ms-animation-delay: 0.75s;
        animation-delay: 0.75s;
    }

    .letter-container i:nth-child(16) {
        -webkit-animation-delay: 0.8s;
        -moz-animation-delay: 0.8s;
        -ms-animation-delay: 0.8s;
        animation-delay: 0.8s;
    }

    .letter-container i:nth-child(17) {
        -webkit-animation-delay: 0.85s;
        -moz-animation-delay: 0.85s;
        -ms-animation-delay: 0.85s;
        animation-delay: 0.85s;
    }

    .letter-container i:nth-child(18) {
    setInterval("show_time0.innerHTML=new Date().toLocaleString()+' 星期'+'日一二三四五六'.charAt(new Date().getDay());",1000);
        -webkit-animation-delay: 0.9s;
        -moz-animation-delay: 0.9s;
        -ms-animation-delay: 0.9s;
        animation-delay: 0.9s;
    }
    @keyframes sharpen {
        0% {
            filter: alpha(opacity=0);
            -moz-opacity: 0;
            opacity: 0;
            text-shadow: 0px 0px 100px #fff;
            color: transparent;
        }

        90% {
            opacity: 0.9;
            text-shadow: 0px 0px 10px #fff;
            color: transparent;
        }

        100% {
            color: #fff;
            filter: alpha(opacity=100);
            -moz-opacity: 1;
            opacity: 1;
            text-shadow: 0px 0px 2px #fff,1px 1px 4px rgba(0,0,0,0.7);
        }
    }
    </style>
</head>

<body class="easyui-layout">
	<!-- begin of header -->
	<div class="wu-header" data-options="region:'north',border:false,split:true">
    	<div class="wu-header-left">
        	<h1>招聘后台管理系统</h1>
        </div>
        <div class="wu-header-right">
        	<span style="position: relative;top:15px">
                <strong class="easyui-tooltip">
                    <shiro:user>
                        欢迎[<shiro:principal/>]登录
                    </shiro:user>
                </strong>
           </span>
            <span style="position: relative;top:15px"><a href="/userLogout"><strong>退出</strong></a></span>
        </div>
    </div>
    <!-- end of header -->
    <!-- begin of sidebar -->
	<div class="wu-sidebar" data-options="region:'west',split:true,border:true,title:'导航菜单'">
    	<div class="easyui-accordion" data-options="border:false,fit:true,selected:999">


            <div title="账号管理" data-options="iconCls:'icon-user-group'" style="padding:5px;">
                <ul class="easyui-tree wu-side-tree">
                    <shiro:hasRole name="role2">
                        <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-user-group" data-link="showAdmin.jsp" iframe="1">系统用户</a></li>
                        <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-user-group" data-link="showShiro.jsp" iframe="1">用户权限</a></li>
                    </shiro:hasRole>
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-user-group" data-link="showAccount.jsp" iframe="1">前台用户</a></li>
                </ul>
            </div>

            <div title="企业信息" data-options="iconCls:'icon-application-cascade'" style="padding:5px;">
    			<ul class="easyui-tree wu-side-tree">
                	<li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-application-cascade" data-link="showCompany.jsp" iframe="1">公司信息</a></li>
                </ul>
            </div>

            <div title="招聘管理" data-options="iconCls:'icon-cog'" style="padding:5px;">
    			<ul class="easyui-tree wu-side-tree">
                	<%--<li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-cog" data-link="showOrders.jsp" iframe="0">职位分类</a></li>
                    --%>
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-cog" data-link="showJob.jsp" iframe="1">职位记录</a></li>
                </ul>
            </div>

            <div title="用户信息" data-options="iconCls:'icon-user'" style="padding:5px;">
    			<ul class="easyui-tree wu-side-tree">
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-users" data-link="showJianLi.jsp" iframe="1">求职简历</a></li>
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-users" data-link="showEducation.jsp" iframe="1">教育经历</a></li>
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-users" data-link="showWorkExperience.jsp" iframe="1">工作经验</a></li>
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-users" data-link="showProjectExperience.jsp" iframe="1">项目经验</a></li>
                </ul>
            </div>

            <div title="公告" data-options="iconCls:'icon-creditcards'" style="padding:5px;">
    			<ul class="easyui-tree wu-side-tree">
                	<li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-creditcards" data-link="showMessage.jsp" iframe="1">通知公告</a>
                  <%--  <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-creditcards" data-link="ShowDelete.jsp" iframe="1">职场快讯</a>
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-creditcards" data-link="UpdateShop.jsp" iframe="1">职业指导</a>
                --%>
                </ul>
            </div>

            <div title="其他" data-options="iconCls:'icon-creditcards'" style="padding:5px;">
                <ul class="easyui-tree wu-side-tree">
                    <li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-creditcards" data-link="https://xy.51job.com" iframe="1">友情链接</a>
                    <%--<li iconCls="icon-chart-organisation"><a href="javascript:void(0)" data-icon="icon-creditcards" data-link="ShowDelete.jsp" iframe="1">在线统计</a>--%>
                </ul>
            </div>
        </div>
    </div>
    <!-- end of sidebar -->
    <!-- begin of main -->
    <div class="wu-main" data-options="region:'center'">
        <div id="wu-tabs" class="easyui-tabs" data-options="border:false,fit:true">
            <div title="首页" data-options="closable:false,iconCls:'icon-tip',cls:'pd3'">
                <div id="firstPage"><%--style="border: 0px solid cornflowerblue"--%>
                    <p class="letter-container" style="margin: 0px;padding: 40px">
                        <i class="char1">欢</i>
                        <i class="char2">迎</i>
                        <i class="char3">进</i>
                        <i class="char4">入</i>
                        <i class="char5">招</i>
                        <i class="char6">聘</i>
                        <i class="char7">网</i>
                        <i class="char8">后</i>
                        <i class="char9">台</i>
                        <i class="char10">管</i>
                        <i class="char11">理</i>
                        <i class="char12">系</i>
                        <i class="char13">统</i>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <!-- end of main -->
    <!-- begin of footer -->
	<div class="wu-footer" data-options="region:'south',border:true,split:true">
    	&copy; 后台管理
    </div>
    <!-- end of footer -->

    <script type="text/javascript">

		$(function(){
			$('.wu-side-tree a').bind("click",function(){
				var title = $(this).text();
				var url = $(this).attr('data-link');
				var iconCls = $(this).attr('data-icon');
				var iframe = $(this).attr('iframe')==1?true:false;
				addTab(title,url,iconCls,iframe);
			});
		})

		/**
		* Name 载入树形菜单
		*/
		$('#wu-side-tree').tree({
			url:'temp/menu.php',
			cache:false,
			onClick:function(node){
				var url = node.attributes['url'];
				if(url==null || url == ""){
					return false;
				}
				else{
					addTab(node.text, url, '', node.attributes['iframe']);
				}
			}
		});

		/**
		* Name 选项卡初始化
		*/
		$('#wu-tabs').tabs({
			tools:[{
				iconCls:'icon-reload',
				border:false,
				handler:function(){
					$('#wu-datagrid').datagrid('reload');
				}
			}]
		});

		/**
		* Name 添加菜单选项
		* Param title 名称
		* Param href 链接
		* Param iconCls 图标样式
		* Param iframe 链接跳转方式（true为iframe，false为href）
		*/
		function addTab(title, href, iconCls, iframe){
			var tabPanel = $('#wu-tabs');
			if(!tabPanel.tabs('exists',title)){
				var content = '<iframe scrolling="auto" frameborder="0"  src="'+ href +'" style="width:100%;height:100%;"></iframe>';
				if(iframe){
					tabPanel.tabs('add',{
						title:title,
						content:content,
						iconCls:iconCls,
						fit:true,
						cls:'pd3',
						closable:true
					});
				}
				else{
					tabPanel.tabs('add',{
						title:title,
						href:href,
						iconCls:iconCls,
						fit:true,
						cls:'pd3',
						closable:true
					});
				}
			}
			else
			{
				tabPanel.tabs('select',title);
			}
		}
		/**
		* Name 移除菜单选项
		*/
		function removeTab(){
			var tabPanel = $('#wu-tabs');
			var tab = tabPanel.tabs('getSelected');
			if (tab){
				var index = tabPanel.tabs('getTabIndex', tab);
				tabPanel.tabs('close', index);
			}
		}
	</script>

<script>
		function alert1(title,message){
			$.messager.alert(title,message);
		}
		
		function errorMessage(title,message){
			$.messager.alert(title,message,'error');
		}
		
		function infoMessage(title,message){
			$.messager.alert(title,message,'info');
		}
		function questionMessage(title,message){
			$.messager.alert(title,message,'question');
		}
		function warningMessage(title,message){
			$.messager.alert(title,message,'warning');
		}
	</script>
	
    <c:if test="${requestScope.Message eq 'deleteFail'}">
         <script type="text/javascript">
        //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','删除失败!','error');
         </script>
    </c:if>

    <c:if test="${requestScope.Message eq 'deleteSuccess'}">
         <script type="text/javascript">
           infoMessage("消息","删除成功");
           $('#tt').datagrid('reload');
         </script>
    </c:if>

</body>

</html>
