<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<html>
<head>
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>学历</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>


	<style type="text/css">
		input[id="schoolname"],input[id="profession"],input[id="name"],input[id="studyabroad"]{
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 130px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 45px;
		}
		input[id="schoolname"]:hover,
		input[id="schoolname"]:focus,
		input[id="profession"]:hover,
		input[id="profession"]:focus,
		input[id="name"]:hover,
		input[id="name"]:focus,
		input[id="studyabroad"]:hover,
		input[id="studyabroad"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>
	<style type="text/css">
		body{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>

<body>

<table id="dg" title="学历" class="easyui-datagrid" style="width:100%;height:100%"
	   url="/showEducation"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="false" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'id',width:100,align:'center',sortable:true" sorter="numberSort">id</th>
		<th data-options="field:'starttime',width:160,align:'center'">上学时间</th>
		<th data-options="field:'endtime',width:120,align:'center'">毕业时间</th>
		<th data-options="field:'schoolname',width:130,align:'center'">学校名</th>
		<th data-options="field:'degree',width:100,align:'center'">学历</th>
		<th data-options="field:'profession',width:130,align:'center'">专业</th>
		<th data-options="field:'studyabroad',width:100,align:'center'">国外学习</th>
		<th data-options="field:'jianliid',width:100,align:'center',sortable:true" sorter="numberSort">简历id</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:20px 20px 17px 20px;">
	<span>
		<input id="schoolname" placeholder="学校" name="schoolname" onkeyup="schoolnameSearch()"/>
		<input id="profession" placeholder="专业" name="profession" onkeyup="professionSearch()"/>

		<select id="studyabroad"  onchange="studyabroadSearch()" style="height: 33px;width: 80px;margin-bottom:5.6px;margin-left:43px;font-family:sans-serif;" >
              <option value="" selected="selected"></option>
              <option value="1">国外就读</option>
              <option value="0">未在国外学习</option>
		</select>
		<select id="degree"  onchange="degreeSearch()" style="height: 33px;width: 80px;margin-bottom:5.6px;margin-left:43px;font-family:sans-serif;" >
              <option value="" selected="selected"></option>
              <option value="1">专科生</option>
              <option value="2">本科生</option>
              <option value="3">研究生</option>
			  <option value="4">博士生</option>
		</select>
	</span>
	<input id="name" placeholder="求职者姓名" name="name" onkeyup="nameSearch()"/>
	<a style="margin-left:40px;margin-bottom:5px" href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="searchall()">Search</a>
	<a style="margin-left:20px;margin-bottom:5px" href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteAccount()">Detete</a>
	<%--<a style="margin-left:20px;margin-bottom:5px" href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newAccount()">Add</a>--%>
	<%--<a style="margin-left:20px;margin-bottom:5px" href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editAccount()">Edit</a>--%>
</div>

<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons">
</div>
<div id="dlg-buttons">
	<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveAccount()">Save</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
	<%--<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancel</a>--%>
</div>

<script type="text/javascript">
    //弹出框add
    var url;
    function newAccount(){
        $('#dlg').dialog('open').dialog('setTitle','New User');
        $('#fm').form('clear');
        url = 'admin/ShoeAction!addshoe.action';
    }
    //弹出框edit
    function editAccount(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行用户进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的用户", "info");
        }else{
            $('#dlg').dialog('open').dialog('setTitle','Edit User');
            $('#fm').form('load',rows);
            url = 'admin/ShoeAction!updateshoe.action';
        }
    }


    //简历姓名的搜索
    function nameSearch(){
        var content = $("#name").val();
        $("#dg").datagrid("options").url = "/searchEducationByName?name=" + content;
        $("#dg").datagrid('reload');
    }

    function all() {
        var schoolname = $("#schoolname").val();
        var profession = $("#profession").val();
        var degree=$('#degree option:selected').text();
        var studyabroad=$('#studyabroad option:selected').val();
        $("#dg").datagrid("options").url = "/searchEducationByConditions?schoolname="+schoolname+"&profession="+profession+"&degree="+ degree+"&studyabroad="+studyabroad;
        $("#dg").datagrid('reload');
    }
    function degreeSearch(){  all()}
    function schoolnameSearch(){  all()}
    function professionSearch(){  all()}
    function searchall(){  all()}
    function studyabroadSearch(){  all()}

    //删除的ajax请求
    function deleteAccount(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.id);
            }
        });
        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {
                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
                    $.get("/deleteEducation?educationIds=" + userid, function (data, status) {
                        if (data == "true") {
                            $.messager.alert("温馨提示", "删除成功", 'info');
                            $("#dg").datagrid('reload');
                        }
                        if (data == "false") {
                            $.messager.alert("温馨提示", "删除失败", 'error');
                        }
                    });
                }
            })
        }else {
            $.messager.alert("温馨提示", "选择要删除的行", 'info');
        }
    }

    //自定义整数排序
    function numberSort(a,b){
        var number1 = parseFloat(a);
        var number2 = parseFloat(b);
        return (number1 > number2 ? 1 : -1);
    }

</script>

</body>
</html>