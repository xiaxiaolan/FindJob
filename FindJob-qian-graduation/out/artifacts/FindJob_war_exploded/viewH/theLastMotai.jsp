<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html>
<head>
	   <base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">
	   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
       <title>模态框</title>
	   <link rel="stylesheet" href="../../FindJob/web/ditu/bootstrap1.min.css">
       <script src="../ditu/jquery.min.js"></script>
       <script src="../ditu/bootstrap.min.js"></script>
	   <script type="text/javascript" src="../../FindJob/out/artifacts/web_war_exploded/ditu/addToolbar.js"></script>

	<style type="text/css">
		/*分别定义HTML中和标记之的距离样式*/
		h1, form, fieldset, legend, ol, li {
			margin: 0;
			padding: 0;
		}
		/*定义内容的样式*/
		form#account {
			background:#CCCCCC;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			padding: 20px;
			width: 400px;
			margin:auto;
		}
		form#account fieldset {
			border: none;
			margin-bottom: 10px;
		}
		form#account fieldset:last-of-type { margin-bottom: 0; }
		form#account legend {
			color:#993399;
			font-size: 16px;
			font-weight: bold;
			padding-bottom: 10px;
			text-shadow: 0 1px 1px #c0d576;
		}

		form#account fieldset fieldset legend {
			color: #111111;
			font-size: 13px;
			font-weight: normal;
			padding-bottom: 0;
		}
		form#account ol li {
			background: #b9cf6a;
			background: rgba(255, 255, 255, .3);
			border-color: #e3ebc3;
			border-color: rgba(255, 255, 255, .6);
			border-style: solid;
			border-width: 2px;
			-webkit-border-radius: 5px;
			line-height: 30px;
			list-style: none;
			padding: 5px 10px;
			margin-bottom: 2px;
		}
		form#account ol ol li {
			background: none;
			border: none;
			float: left;
		}
		form#account label {
			float: left;
			font-size: 13px;
			width: 110px;
		}
		form#account fieldset fieldset label {
			background: none no-repeat left 50%;
			line-height: 20px;
			padding: 0 0 0 30px;
			width: auto;
		}
		form#account fieldset fieldset label:hover { cursor: pointer; }
		form#account input:not([type=radio]), form#payment textarea {
			background: #ffffff;
			border: #FC3 solid 1px;
			-webkit-border-radius: 3px;
			font: italic 13px Georgia, "Times New Roman", Times, serif;
			outline: none;
			padding: 5px;
			width: 200px;
		}
		form#account input:not([type=submit]):focus, form#payment textarea:focus {
			background: #eaeaea;
			border: #F00 solid 1px;
		}
		form#account input[type=radio] {
			float: left;
			margin-right: 5px;
		}
	</style>
</head>
<body>

 <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="newResource">
	  <div class="modal-dialog" role="document">
			<div class="modal-content">
<%--head--%>
				<div class="modal-header">
					<h4 class="modal-title"  id="gridSystemModalLabel">修改资源</h4>
				</div>
<%--body--%>
				<div class="modal-body">
					<div class="container-fluid">
						<form id="account">
							<fieldset>
								<legend>初级信息</legend>
								<ol>
									<li style="visibility:hidden; height:0; width:0; position:absolute;">
										<label for="name">用户id：</label>
										<input id="name" name="name" type="text" placeholder="请输入用户名" required autofocus>
									</li>
									<li>
										<label for="phone">账号：</label>
										<input id="phone" name="phone" type="tel" placeholder="输入11位手机号"   maxlength="11" required>
									</li>
									<li>
										<label for="password">密码：</label>
										<input id="password" name="password" type="text" placeholder="输入密码" required>
									</li>
									<li>
										<fieldset>
											<ol>
												<li>
													<label for="status">状态：</label>
													<input id="active" name="status" type="radio" value="1">
													<label for="active">已激活</label>
												</li>
												<li>
													<input id="notactive" name="status" type="radio" value="0">
													<label for="notactive">未激活</label>
												</li>
											</ol>
										</fieldset>
									</li>
									<li>
										<fieldset>
											<ol>
												<li>
													<label for="type">类型：</label>
													<input id="corporate" name="type" type="radio" value="1">
													<label for="corporate">公司</label>
												</li>
												<li>
													<input id="personal" name="type" type="radio" value="0">
													<label for="personal">个人</label>
												</li>
											</ol>
										</fieldset>
									</li>
								</ol>
							</fieldset>
							<fieldset>
								<button type="submit" value="submit">提交</button>
							</fieldset>
						</form>
					</div>
				</div>
<%--bottom保存取消按钮--%>
				<div class="modal-footer">
					<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
					<button type="button" class="btn btn-xs btn-green" id="save">保 存</button>
				</div>
			</div><!-- /.modal-content -->
	  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">

		     $('#newResource').modal();
</script>
</body>
</html>