<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>职位</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
	<%--模态框--%>
	<%--这个问题搞了半天主要是因为 我引用了eaeasyUi的包 又引用了jquery的包 --%>
	<link rel="stylesheet" href="ditu/bootstrap1.min.css">
	<script src="ditu/bootstrap.min.js"></script>

	<style type="text/css">
		/*分别定义HTML中和标记之的距离样式*/
		h1, form, fieldset, legend, ol, li {
			margin: 0;
			padding: 0;
		}
		/*定义内容的样式*/
		form#position {
			background:#CCCCCC;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			padding: 20px;
			width: 400px;
			margin:auto;
		}
		form#position fieldset {
			border: none;
			margin-bottom: 10px;
		}
		form#position fieldset:last-of-type { margin-bottom: 0; }
		form#position legend {
			color:#993399;
			font-size: 16px;
			font-weight: bold;
			padding-bottom: 10px;
			text-shadow: 0 1px 1px #c0d576;
		}

		form#position fieldset fieldset legend {
			color: #111111;
			font-size: 13px;
			font-weight: normal;
			padding-bottom: 0;
		}
		form#position ol li {
			background: #b9cf6a;
			background: rgba(255, 255, 255, .3);
			border-color: #e3ebc3;
			border-color: rgba(255, 255, 255, .6);
			border-style: solid;
			border-width: 2px;
			-webkit-border-radius: 5px;
			line-height: 30px;
			list-style: none;
			padding: 5px 10px;
			margin-bottom: 2px;
		}
		form#position ol ol li {
			background: none;
			border: none;
			float: left;
		}
		form#position label {
			float: left;
			font-size: 13px;
			width: 100px;
			vertical-align:top;
		}
		form#position fieldset fieldset label {
			background: none no-repeat left 50%;
			line-height: 20px;
			padding: 0 0 0 20px;
			width: auto;
		}
		form#position fieldset fieldset label:hover { cursor: pointer; }
		form#position input:not([type=radio]), form#payment textarea {
			background: #ffffff;
			border: #FC3 solid 1px;
			-webkit-border-radius: 3px;
			font: italic 13px Georgia, "Times New Roman", Times, serif;
			outline: none;
			padding: 5px;
			width: 200px;
		}
		form#position input:not([type=submit]):focus, form#payment textarea:focus {
			background: #eaeaea;
			border: #F00 solid 1px;
		}
		form#position input[type=radio] {
			float: left;
			margin-right: 0px;
			display:inline;
			vertical-align: top}
		form#position select{
			height:24px;
			width:120px;}
	</style>


	<style type="text/css">
		input[id="comname2"],input[id="positionname2"],input[id="positionlable2"],input[id="needed2"],input[id="typename2"]{
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 130px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 45px;
		}
		input[id="comname2"]:hover,
		input[id="comname2"]:focus,
		input[id="positionname2"]:hover,
		input[id="positionname2"]:focus,
		input[id="positionlable2"]:hover,
		input[id="positionlable2"]:focus,
		input[id="needed2"]:hover,
		input[id="needed2"]:focus,
		input[id="typename2"]:hover,
		input[id="typename2"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>

	<style type="text/css">
		body{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>

<body>

<table id="dg" title="职位记录" class="easyui-datagrid" style="width:100%;height:100%"
	   url="/showPosition"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="true" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'positionid',width:40,align:'center',sortable:true" sorter="numberSort">id</th>
		<th data-options="field:'positionname',width:100,align:'center'">职位名称</th>
		<th data-options="field:'needed',width:80,align:'center'">需求人数</th>
		<th data-options="field:'salary',width:70,align:'center'">薪资</th>
		<th data-options="field:'experience',width:80,align:'center'">工作经验</th>
		<th data-options="field:'edu',width:70,align:'center'">学历</th>
		<th data-options="field:'address',width:90,align:'center'">地址</th>
		<th data-options="field:'pubtime',width:90,align:'center'">发布时间</th>
		<th data-options="field:'positionlable',width:100,align:'center'">职位标签</th>
		<th data-options="field:'positionmsg',width:120,align:'center'">职位信息</th>
		<th data-options="field:'combasemsgid',width:90,align:'center'">发布公司id</th>
		<th data-options="field:'poitiontypeid',width:90,align:'center',sortable:true" sorter="numberSort">职位类型id</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:27px 20px 17px 25px;">
	<span>
		<input id="positionname2" placeholder="职位名称" name="positionname2" onkeyup="positionnameSearch()"/>
		<input id="positionlable2" placeholder="职位标签" name="positionlable2" onkeyup="positionlableSearch()"/>
        <input id="needed2" placeholder="需求人数范围" name="needed2" onkeyup="neededSearch()"/>

		<select id="edu2"  onchange="eduSearch()" style="height: 33px;width: 80px;margin-bottom:5.6px;margin-left:43px;font-family:sans-serif;" >
              <option value="" selected="selected"></option>
              <option value="1">专科以上</option>
              <option value="2">中专以上</option>
              <option value="3">本科</option>
			  <option value="4">本科以上</option>
		</select>
	</span>
	<div></div>
	<input id="comname2" placeholder="公司名" name="comname2" onkeyup="comnameSearch()"/>
	<a style="margin-left:43.5px;margin-top:5px" href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="searchall()">Search</a>
	<a style="margin-left:20px;margin-top:5px" href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteAccount()">Detete</a>
	<a style="margin-left:20px;margin-top:5px" href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newPosition()">Add</a>
	<a style="margin-left:20px;margin-top:5px" href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editPosition()">Edit</a>

</div>

<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons"></div>
<div id="dlg-buttons"></div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="newResource">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<%--head--%>
			<div class="modal-header">
				<h4 class="modal-title"  id="gridSystemModalLabel">修改资源</h4>
			</div>
			<%--body--%>
			<div class="modal-body">
				<div class="container-fluid">
					<form id="position" action="" method="post" name="position">
						<fieldset>
							<legend>职位信息</legend>
							<ol>
								<li style="position: absolute">
									<input id="positionid" name="positionid" type="hidden">
								</li>
								<li>
									<label for="positionname">职位名称：</label>
									<input id="positionname" name="positionname" type="text" required>
								</li>
								<li>
									<label for="needed">需求人数：</label>
									<input id="needed" name="needed" type="text" placeholder="如：20" maxlength="3" required>
								</li>
								<li>
									<label for="positionmsg">职位简介：</label>
									<input id="positionmsg"  name="positionmsg" type="text" required>
								</li>
								<li>
									<label for="salary">提供薪资范围：</label>
									<input id="salary" name="salary" type="text" placeholder="如：1-1.5万/月" required>
								</li>
								<li>
									<label for="pubtime">发布时间：</label>
									<input id="pubtime" name="pubtime" type="text"  placeholder="年/月/日" required>
								</li>
								<li>
									<label for="experience">需求工作经验</label>
									<select name="experience" id="experience">
										<option value =""></option>
										<option value ="0">应届毕业生</option>
										<option value ="1">1年</option>
										<option value ="2">2年</option>
										<option value ="3">3年</option>
										<option value ="4">4年</option>
										<option value ="5">5年</option>
										<option value ="6">6年</option>
										<option value ="7">7年</option>
										<option value ="8">8年</option>
										<option value ="9">9年</option>
										<option value ="10">10年及以上</option>
									</select>
								</li>
								<li>
									<label for="edu">需求学历</label>
									<select name="edu" id="edu">
										<option value =""></option>
										<option value ="本科以上">本科以上</option>
										<option value ="专科以上">专科以上</option>
										<option value ="中专以上">中专以上</option>
									</select>
								</li>

								<li>
									<label for="address">工作地点：</label>
									<input id="address" name="address" type="text" placeholder="XX省XX市" required>
								</li>
								<li>
									<label for="positionlable">标签</label>
									<input id="positionlable" name="positionlable" type="text" placeholder="多标签以逗号隔开" required>
								</li>
                                <li>
                                    <label for="combasemsgid">公司id</label>
                                    <select id="combasemsgid" name="combasemsgid">
                                    </select>
                                </li>
                                <li>
                                    <label for="poitiontypeid">职位类型id</label>
                                    <select id="poitiontypeid" name="poitiontypeid">
                                    </select>
                                </li>
							</ol>
						</fieldset>

						<%--<fieldset>
							<button type="submit">提交</button>
						</fieldset>--%>
					</form>
				</div>
			</div>
			<%--bottom保存取消按钮--%>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
				<button type="button" class="btn btn-xs btn-green" id="save" onclick="save()">保 存</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
    //弹出框add
    function newPosition(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length ==0) {
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("添加职位信息");
            $('form[id=position]').attr('action', "/addPosition");
            $('#position').form('clear');

            document.position.combasemsgid.disabled=false;//修改：combasemsgid不可编辑
            document.position.poitiontypeid.disabled=false;//修改：combasemsgid不可编辑

            //去后台请求公司账号id，添加职位用到
            var length1=document.getElementById("combasemsgid").length;
            var length2=document.getElementById("poitiontypeid").length;

            if (length1!=null){
                for (var i;i<=length1;i++) {
                    $("#combasemsgid").options.remove(i);
                }
            }
            if (length2!=null){
                for (var i;i<=length2;i++) {
                    $("#poitiontypeid").options.remove(i);
                }
            }
            $.get("selectAllCompanyId", function (data, status) {
                for(var i=0;i<data.length;i++){
                    $("#combasemsgid").append('<option value ="'+data[i]+'">'+data[i]+'</option>');
                }
            });
            $.get("selectAllPosirionTypeId", function (data, status) {
                for(var i=0;i<data.length;i++){
                    $("#poitiontypeid").append('<option value ="'+data[i]+'">'+data[i]+'</option>');
                }
            });
        }else {
            $.messager.alert("提示", "请清空选中的行", "info");
        }
    }

    //弹出框edit
    function editPosition(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行用户进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的用户", "info");
        }else{
            $('#newResource').modal("show");
            $("#gridSystemModalLabel").text("修改职位信息");
            $('form[id=position]').attr('action',"/updatePosition");

            document.position.combasemsgid.disabled=true;//修改：combasemsgid不可编辑
            document.position.poitiontypeid.disabled=true;//修改：combasemsgid不可编辑

            //1、获取选中的行
            var id=rows[0].positionid;
            //2、将存在session的值赋值给表单
            $.get("showPositionById?positionid=" + id, function (data, status) {
                //3、赋值给表单,必须通过name属性
                document.position.positionid.value=id;
                document.position.positionname.value=data.positionname;
                document.position.needed.value=data.needed;
                document.position.salary.value=data.salary;
                document.position.positionmsg.value=data.positionmsg;
                document.position.address.value=data.address;
                document.position.positionlable.value=data.positionlable;
                document.position.pubtime.value=data.pubtime;

                $("#combasemsgid").append('<option value ="'+data.combasemsgid+'" selected="selected">'+data.combasemsgid+'</option>')
                $("#poitiontypeid").append('<option value ="'+data.poitiontypeid+'" selected="selected">'+data.poitiontypeid+'</option>')

                $("#experience option[value='"+data.experience+"']").attr("selected","selected");
                $("#edu option[value='"+data.edu+"']").attr("selected","selected");
            });
        }
    }
//保存按钮
    //点击模态框的保存之后，请求后台并将这个模态框关闭
    function save() {
        $('#position').submit();
        $('#newResource').modal('hide');
        //存一个update/add返回的的值，判断，弹框
    }

    function CnameTname() {
        //键盘输入搜索事件
        //var typename = $("#typename").val();
		var comname = $("#comname2").val();
        $("#dg").datagrid("options").url = "/searchPositionByCT?comname="+comname;
        $("#dg").datagrid('reload');
    }
    function comnameSearch() {
        CnameTname();
    }

//     function typenameSearch() {
//         CnameTname();}


//绑定搜索链接按钮点击事件，点击搜索时发送请求
    function searchall(){
        all()
    }
    //ajax多条件
    function all() {
        var positionname = $("#positionname2").val();
        var positionlable = $("#positionlable2").val();
        var needed = $("#needed2").val();
		var edu=$('#edu2 option:selected').text();
        $("#dg").datagrid("options").url = "/searchJobByConditions?positionname="+positionname+"&positionlable="+positionlable+"&needed="+ needed+"&edu="+edu;
        $("#dg").datagrid('reload');
    }
    function positionnameSearch(){
        all();
	}
    function positionlableSearch(){
        all();
	}
    function neededSearch(){
        all();
	}
    function eduSearch(){
        all();
    }


    //删除的ajax请求
    function deleteAccount(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.positionid);
            }
        });
        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {
                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
                    $.get("/deletePosition?positionIds=" + userid, function (data, status) {
                        if (data == "true") {
                            $.messager.alert("温馨提示", "删除成功", 'info');
                            $("#dg").datagrid('reload');
                        }
                        if (data == "false") {
                            $.messager.alert("温馨提示", "删除失败", 'error');
                        }
                    });
                }
            })
        }else {
            $.messager.alert("温馨提示", "选择要删除的行", 'info');
		}
    }



    //自定义整数排序
    function numberSort(a,b){
        var number1 = parseFloat(a);
        var number2 = parseFloat(b);
        return (number1 > number2 ? 1 : -1);
    }

</script>


<%--返回 add/update  "${sessionScope.user.username}" 的消息--%>
<c:choose>
	<c:when  test="${addPosition  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加成功!','info');
		</script>
	</c:when>

	<c:when  test="${addPosition  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>


<c:choose>
	<c:when  test="${updatePosition  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改成功!','info');
		</script>
	</c:when>

	<c:when  test="${updatePosition  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>
</body>
</html>