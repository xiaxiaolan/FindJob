<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<html>
<head>
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>公司</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
	<%--模态框--%>
	<%--这个问题搞了半天主要是因为 我引用了eaeasyUi的包 又引用了jquery的包 --%>
	<link rel="stylesheet" href="ditu/bootstrap1.min.css">
	<script src="ditu/bootstrap.min.js"></script>

	<style type="text/css">
		/*分别定义HTML中和标记之的距离样式*/
		h1, form, fieldset, legend, ol, li {
			margin: 0;
			padding: 0;
		}
		/*定义内容的样式*/
		form#combase {
			background:#CCCCCC;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			padding: 20px;
			width: 400px;
			margin:auto;
		}
		form#combase fieldset {
			border: none;
			margin-bottom: 10px;
		}
		form#combase fieldset:last-of-type { margin-bottom: 0; }
		form#combase legend {
			color:#993399;
			font-size: 16px;
			font-weight: bold;
			padding-bottom: 10px;
			text-shadow: 0 1px 1px #c0d576;
		}

		form#combase fieldset fieldset legend {
			color: #111111;
			font-size: 13px;
			font-weight: normal;
			padding-bottom: 0;
		}
		form#combase ol li {
			background: #b9cf6a;
			background: rgba(255, 255, 255, .3);
			border-color: #e3ebc3;
			border-color: rgba(255, 255, 255, .6);
			border-style: solid;
			border-width: 2px;
			-webkit-border-radius: 5px;
			line-height: 30px;
			list-style: none;
			padding: 5px 10px;
			margin-bottom: 2px;
		}
		form#combase ol ol li {
			background: none;
			border: none;
			float: left;
		}
		form#combase label {
			float: left;
			font-size: 13px;
			width: 100px;
			vertical-align:top;
		}
		form#combase fieldset fieldset label {
			background: none no-repeat left 50%;
			line-height: 20px;
			padding: 0 0 0 20px;
			width: auto;
		}
		form#combase fieldset fieldset label:hover { cursor: pointer; }
		form#combase input:not([type=radio]), form#combase textarea{
			background: #ffffff;
			border: #FC3 solid 1px;
			-webkit-border-radius: 3px;
			font: italic 13px Georgia, "Times New Roman", Times, serif;
			outline: none;
			padding: 5px;
			width: 200px;
		}
		form#combase input:not([type=submit]):focus, form#combase textarea:focus {
			background: #eaeaea;
			border: #F00 solid 1px;
		}
		form#combase input[type=radio] {
			float: left;
			margin-right: 0px;
			display:inline;
			vertical-align: top}
		form#combase select{
			height:24px;
			width:120px;}
	</style>

<%--搜索框--%>
	<style type="text/css">
		input[id="comname1"],input[id="faren1"],input[id="phonec"],input[id="industry1"],input[id="comsize1"],input[id="phone1"] {
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 130px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 15px;
		}
		input[id="comname1"]:hover,
		input[id="comname1"]:focus,
		input[id="faren1"]:hover,
		input[id="faren1"]:focus,
		input[id="phonec"]:hover,
		input[id="phonec"]:focus,
		input[id="industry1"]:hover,
		input[id="industry1"]:focus,
		input[id="comsize1"]:hover,
		input[id="comsize1"]:focus,
		input[id="phone1"]:hover,
		input[id="phone1"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>

	<style type="text/css">
		body{
			margin:0px;
			padding:0px;
		}
	</style>
</head>

<body>

<table id="dg" title="公司信息" class="easyui-datagrid" style="width:100%;height:100%"
	   url="/showCompany"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="false" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'combasemsgid',width:40,align:'center',sortable:true" sorter="numberSort">id</th>
		<th data-options="field:'comname',width:120,align:'center'">公司名称</th>
		<th data-options="field:'faren',width:80,align:'center'">法人名称</th>
		<th data-options="field:'phone',width:120,align:'center'">联系电话</th>
		<th data-options="field:'registertime',width:130,align:'center'">注册时间</th>
		<th data-options="field:'businesslicense',width:100,align:'center',formatter:showImg">营业执照</th>
		<th data-options="field:'industry',width:100,align:'center'">所属行业</th>
		<th data-options="field:'address',width:100,align:'center'">公司地址</th>
		<th data-options="field:'comsize',width:70,align:'center'">公司规模</th>
		<th data-options="field:'comtype',width:70,align:'center'">公司等级</th>
		<th data-options="field:'des',width:100,align:'center'">公司标语</th>
		<th data-options="field:'accountid',width:70,align:'center',sortable:true" sorter="numberSort">账户id</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:27px 20px 17px 25px">
	<div id="img" style="width:300px;height:320px;display:none;position:fixed;
	      margin-left: 170px;margin-top:90px ;z-index:191;border: 1px solid #9fcdff">
		<img  id="showImg" width="100%" height="100%">
	</div>
	<span>
		<input id="comname1" placeholder="公司名" name="comname1" onkeyup="comnameSearch()"/>
		<input id="faren1" placeholder="法人name" name="faren1" onkeyup="farenSearch()"/>
        <input id="phonec" placeholder="公司电话" name="phonec" onkeyup="phonecSearch()"/>
        <input id="industry1" placeholder="行业" name="industry1" onkeyup="industrySearch()"/>
        <input id="comsize1" placeholder="人员规模>=" name="comsize1" onkeyup="comsizeSearch()"/>

		<select id="comtype1"  onchange="comtypeSearch()" style="height: 33px;width: 80px;margin-bottom:5.6px;margin-left:12px;font-family:sans-serif;" >
              <option value="" selected="selected"></option>
              <option value="1">等级1</option>
              <option value="2">等级2</option>
              <option value="3">等级3</option>
		</select>
		<a style="margin-bottom:5px" href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="searchall()">Search</a>
	</span>
	<div></div>
	<input id="phone1" placeholder="注册phone" name="phone" onkeyup="phoneSearch()"/>
	<a style="margin-left:15px" href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteAccount()">Detete</a>
	<a style="margin-left:15px" href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newCompany()">Add</a>
	<a style="margin-left:15px" href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editCompany()">Edit</a>
</div>

<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons" ></div>
<div id="dlg-buttons"></div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="newResource">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<%--head--%>
			<div class="modal-header">
				<h4 class="modal-title"  id="gridSystemModalLabel">修改资源</h4>
			</div>
			<%--body--%>
			<div class="modal-body">
				<div class="container-fluid">
					<form id="combase" action="" method="post" name="combase">
						<fieldset>
							<legend>公司信息</legend>
							<ol>
								<li style="position: absolute">
									<input id="combasemsgid" name="combasemsgid" type="hidden"/>
								</li>
								<li>
									<label for="comname">公司名称：</label>
									<input id="comname" name="comname" type="text" required>
								</li>
								<li>
									<label for="faren">公司法人：</label>
									<input id="faren" name="faren" type="text" maxlength="5" required>
								</li>
								<li>
									<label for="phone">电话：</label>
									<input id="phone" name="phone" type="text" required>
								</li>
								<li>
									<label for="registertime">公司注册时间：</label>
									<input id="registertime" name="registertime"  type="text" placeholder="年/月/日" required>
								</li>
								<li>
									<label for="industry">所属行业：</label>
									<input id="industry"  name="industry" type="text" required>
								</li>
								<li>
									<label for="address">公司地址：</label>
									<input id="address" name="address" type="text" placeholder="XX省XX市" required>
								</li>
								<li>
									<label for="comsize">公司规模：</label>
									<input id="comsize" name="comsize" type="text" placeholder="公司总人数" required>
								</li>
								<li>
									<label for="comtype">公司等级</label>
									<select id="comtype" name="comtype">
										<option value =""></option>
									    <option value ="1">等级1</option>
										<option value ="2">等级2</option>
										<option value ="3">等级3</option>
									</select>
								</li>
								<li>
									<label for="des">公司标语：</label>
									<input id="des" name="des" type="text" required>
								</li>
								<li>
									<label for="accountid">账号id</label>
									<select id="accountid" name="accountid">
									</select>
								</li>
							</ol>
						</fieldset>
						<%--<fieldset>
							<button type="submit">提交</button>
						</fieldset>--%>

					</form>
				</div>
			</div>
			<%--bottom保存取消按钮--%>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
				<button type="button" class="btn btn-xs btn-green" id="save" onclick="save()">保 存</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    //弹出框add
    function newCompany(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length ==0) {
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("添加公司信息")
            $('form[id=combase]').attr('action', "/addCompany");
            $('#combase').form('clear');
            document.combase.accountid.disabled=false;//修改：accountid不可编辑

            //去后台请求还没被注册过的公司账号，类型id type=0，且未在公司表中引用;每次加载前先清空
            var length1=document.getElementById("accountid").length;
            if (length1!=null){
                for (var i;i<=length1;i++) {
                    $("#accountid").options.remove(i);
                }
            }
            $.get("selectComAccountIds", function (data, status) {
                for(var i=0;i<data.length;i++){
                    $("#accountid").append('<option value ="'+data[i]+'">'+data[i]+'</option>');
                }
            });
        }
        else {
            $.messager.alert("提示", "请清空选中的行", "info");
		}
    }

    //弹出框edit
    function editCompany(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行用户进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的用户", "info");
        }else{
            //模态框打开之前,ajax先要去请求后台 id获取bean信息显示在form
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("修改公司信息")
            $('form[id=combase]').attr('action',"/updateCompany");
            document.combase.accountid.disabled=true;//修改：accountid不可编辑
            //1、获取选中的行
            var id=rows[0].combasemsgid;
            //2、将存在session的值赋值给表单
            $.get("showCompanyById?companyId=" + id, function (data, status) {
                    //3、赋值给表单,必须通过name属性
				    document.combase.combasemsgid.value=id;
                    document.combase.comname.value=data.comname;
                    document.combase.faren.value=data.faren;
                    document.combase.phone.value=data.phone;
                    document.combase.registertime.value=data.registertime;
                    document.combase.industry.value=data.industry;
                    document.combase.address.value=data.address;
                    document.combase.comsize.value=data.comsize;
                    document.combase.des.value=data.des;
                    //document.combase.accountid.value=data.accountid;
                    $("#accountid").append('<option value ="'+data.accountid+'" selected="selected">'+data.accountid+'</option>')

				    $("#comtype option[value='"+data.comtype+"']").attr("selected","selected");
            });
          }
	}

    //点击模态框的保存之后，请求后台并将这个模态框关闭
    function save() {
        $('#combase').submit();
        $('#newResource').modal('hide');
        //存一个update/add返回的的值，判断，弹框
    }


    //单独phone动态搜索的请求 keyup
    function phoneSearch(){
        var phone = $("#phone1").val();
        $("#dg").datagrid("options").url = "/searchCompanyByPhone?phone=" + phone;
        $("#dg").datagrid('reload');
    }

    //点击多条件搜索按钮的请求
    function searchall(){
       all();
    }

    function all() {
        var comname = $("#comname1").val();
        var faren = $("#faren1").val();
        var phonec = $("#phonec").val();
        var comtype=$('#comtype1 option:selected').val();
        var industry = $("#industry1").val();
        var comsize = $("#comsize1").val();
        $("#dg").datagrid("options").url = "/searchCompanyByConditions?comname="+comname+"&faren="+faren+"&phonec="+
            phonec+"&comtype="+comtype+"&industry="+industry+"&comsize="+comsize;
        $("#dg").datagrid('reload');
    }
    //多个条件动态匹配的搜索
    function comnameSearch(){ all();}
    function farenSearch(){ all();}
    function phonecSearch(){ all();}
    function industrySearch(){ all();}
    function comsizeSearch(){ all();}
    function comtypeSearch(){ all();}


    //删除的ajax请求
    function deleteAccount(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.combasemsgid);
            }
        });
        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {
                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
					$.get("deleteCompany?companyIds=" + userid, function (data, status) {
						if (data == "true") {
							$.messager.alert("温馨提示", "删除成功", 'info');
							$("#dg").datagrid('reload');
						}
						if (data == "false") {
							$.messager.alert("温馨提示", "删除失败", 'error');
						}
					});
                }
            })
        }else {
            $.messager.alert("温馨提示", "请选择要删除的行", 'info');
        }
    }


    // 图片执照显示的js事件;
    function showImg(businesslicense){
        var img=businesslicense.replace(/[^0-9]/ig,"");
        return '<img id="img1" src="'+businesslicense+'" style="width:30px;height:30px;"  onmouseover="largeImage(' + img + ')" onmouseout="minImage()"/>';
    }
    function largeImage(img,event) {
		var path="images/zhizhao/"+img+".jpg";
		$('#showImg').attr("src",path);
        $('#img').css("display","block");
    }
    function minImage() {
        $('#img').css("display","none");
    }


    //自定义整数排序
    function numberSort(a,b){
        var number1 = parseFloat(a);
        var number2 = parseFloat(b);
        return (number1 > number2 ? 1 : -1);
    }
</script>

<%--返回 add/update  "${sessionScope.user.username}" 的消息--%>
<c:choose>
	<c:when  test="${addCompany  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加成功!','info');
		</script>
	</c:when>

	<c:when  test="${addCompany  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>


<c:choose>
	<c:when  test="${updateCompany  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改成功!','info');
		</script>
	</c:when>

	<c:when  test="${updateCompany  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>

</body>
</html>