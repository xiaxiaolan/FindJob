<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>前台用户</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
	<%--模态框--%>
	<link rel="stylesheet" href="ditu/bootstrap1.min.css">
	<script src="ditu/bootstrap.min.js"></script>

	<style type="text/css">
		/*分别定义HTML中和标记之的距离样式*/
		h1, form, fieldset, legend, ol, li {
			margin: 0;
			padding: 0;
		}
		/*定义内容的样式 模态弹出框里面的表单*/
		form#account {
			background:#CCCCCC;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			padding: 20px;
			width: 400px;
			margin:auto;
		}
		form#account fieldset {
			border: none;
			margin-bottom: 10px;
		}
		form#account fieldset:last-of-type { margin-bottom: 0; }
		form#account legend {
			color:#993399;
			font-size: 16px;
			font-weight: bold;
			padding-bottom: 10px;
			text-shadow: 0 1px 1px #c0d576;
		}

		form#account fieldset fieldset legend {
			color: #111111;
			font-size: 13px;
			font-weight: normal;
			padding-bottom: 0;
		}
		form#account ol li {
			background: #b9cf6a;
			background: rgba(255, 255, 255, .3);
			border-color: #e3ebc3;
			border-color: rgba(255, 255, 255, .6);
			border-style: solid;
			border-width: 2px;
			-webkit-border-radius: 5px;
			line-height: 30px;
			list-style: none;
			padding: 5px 10px;
			margin-bottom: 2px;
		}
		form#account ol ol li {
			background: none;
			border: none;
			float: left;
		}
		form#account label {
			float: left;
			font-size: 13px;
			width: 110px;
		}
		form#account fieldset fieldset label {
			background: none no-repeat left 50%;
			line-height: 20px;
			padding: 0 13 0 5px;
			width: auto;
		}
		form#account fieldset fieldset label:hover { cursor: pointer; }
		form#account input:not([type=radio]), form#payment textarea {
			background: #ffffff;
			border: #FC3 solid 1px;
			-webkit-border-radius: 3px;
			font: italic 13px Georgia, "Times New Roman", Times, serif;
			outline: none;
			padding: 5px;
			width: 200px;
		}
		form#account input:not([type=submit]):focus, form#payment textarea:focus {
			background: #eaeaea;
			border: #F00 solid 1px;
		}
		form#account input[type=radio] {
			float: left;
			margin-right: 5px;
		}
	</style>

	<style type="text/css">
		input[id="key"]{
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 130px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 15px;
		}

		input[id="key"]:hover,
		input[id="key"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>


	<style type="text/css">
		body{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>

<body>

<table id="dg" title="前台账号" class="easyui-datagrid" style="width:100%;height:100%"
	   url="/showAccount"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="false" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'useraccountid',width:80,align:'center',sortable:true" sorter="numberSort">account编号</th>
		<th data-options="field:'phone',width:130,align:'center'">注册电话</th>
		<th data-options="field:'password',width:200,align:'center'">用户密码</th>
		<th data-options="field:'touxiang',width:100,align:'center',formatter:showImg">用户头像</th>
		<th data-options="field:'status',width:100,align:'center',sortable:true" sorter="numberSort">激活1/未激活0</th>
		<th data-options="field:'type',width:100,align:'center',sortable:true" sorter="numberSort">个人1/公司0</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:27px 20px 17px 40px;">
	<span>
		<input id="key" placeholder="注册phone" name="phone1" onkeyup="dynamicSearch()"/>
	</span>

	<a href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="searchAccount()">Search</a>
	<a href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteAccount()">Detete</a>
	<a href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newAccount()">Add</a>
	<a href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editAccount()">Edit</a>
</div>

<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons"></div>
<div id="dlg-buttons"></div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="newResource">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<%--head--%>
			<div class="modal-header">
				<h4 class="modal-title"  id="gridSystemModalLabel">修改资源</h4>
			</div>
			<%--body--%>
			<div class="modal-body">
				<div class="container-fluid">
					<form id="account" action="" method="post" name="account">
						<fieldset>
							<legend>账户信息</legend>
							<ol>
								<li style="visibility:hidden; height:0; width:0; position:absolute;">
									<label for="useraccountid">用户id：</label>
									<input id="useraccountid" name="useraccountid" type="text" placeholder="id">
								</li>
								<li>
									<label for="phone">账号：</label>
									<input id="phone" name="phone" type="text" placeholder="输入11位手机号"   minlength="11" maxlength="11" required>
								</li>
								<li>
									<label for="password">密码：</label>
									<input id="password" name="password" type="text" placeholder="输入>=6位密码" minlength="6"  required>
								</li>
								<li>
									<fieldset>
										<ol>
											<li>
												<label for="status" id="status">状态：</label>
												<input id="active" name="status" type="radio" value="1">
												<label for="active">已激活</label>
											</li>
											<li>
												<input id="notactive" name="status" type="radio" value="0">
												<label for="notactive">未激活</label>
											</li>
										</ol>
									</fieldset>
								</li>
								<li>
									<fieldset>
										<ol>
											<li>
												<label for="type" id="type">类型：</label>
												<input id="corporate" name="type" type="radio" value="0">
												<label for="corporate">公司</label>
											</li>
											<li>
												<input id="personal" name="type" type="radio" value="1">
												<label for="personal">个人</label>
											</li>
										</ol>
									</fieldset>
								</li>
							</ol>
						</fieldset>
						<%--<fieldset>
							<button type="submit" value="submit">提交</button>
						</fieldset>--%>
					</form>
				</div>
			</div>
			<%--bottom保存取消按钮--%>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
				<button type="button" class="btn btn-xs btn-green" id="save" onclick="save()">保 存</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
    var flag;
	//弹出框add
    function newAccount(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length ==0) {
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("添加账户")
            $('form[id=account]').attr('action',"/addAccount");
            $('#account').form('clear');
            //设置phone可编辑
            document.account.phone.readOnly=false;
            flag="add";//在保存submit时候 add要查用户在不在，update不用
        }else {
            $.messager.alert("提示", "请清空选中的行", "info");
        }
    }

    //check的方法
    function initradio(rName,rValue){
        var rObj = document.getElementsByName(rName);
        for(var i = 0;i < rObj.length;i++){
            if(rObj[i].value == rValue){
                rObj[i].checked =  'checked';
            }
        }
    }
    //弹出框edit
    function editAccount(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行用户进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的用户", "info");
        }else{
            //模态框打开之前,ajax先要去请求后台 id获取bean信息显示在form
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("修改账号")
            $('form[id=account]').attr('action',"/updateAccount");
            //设置phone不可编辑
            document.account.phone.readOnly=true;
            flag="update";

            //1、获取选中的行
            var id=rows[0].useraccountid;
            //2、将存在session的值赋值给表单
            $.get("getAccountById?useraccountid=" + id, function (data, status) {
                //3、赋值给表单,必须通过name属性
                document.account.phone.value=data.phone;
                document.account.password.value=data.password;

                initradio('status',data.status);
                initradio('type',data.type);
                document.account.useraccountid.value=id;
            });
        }
    }
    //点击模态框的保存之后，请求后台并将这个模态框关闭
    function save() {
        if(flag=="add"){
			var phone=document.account.phone.value;
			//提交表单之前去后台查看用户phone是否已经注册
			$.get("getAccountByPhone?phone=" + phone, function (data, status) {
				if (data == "true") {
					$.messager.alert("温馨提示", "用户名已被存在或者网络异常", 'info');
				}
				if (data == "false") {
					$('#account').submit();
					$('#newResource').modal('hide');
					//存一个update/add返回的的值，判断，弹框
				}
			});
        }else if (flag=="update") {
            $('#account').submit();
            $('#newResource').modal('hide');
            //存一个update/add返回的的值，判断，弹框
		}

    }


    //绑定搜索链接按钮点击事件	，点击搜索时发送请求
    function searchAccount(){
        var content = $("#key").val();
        $("#dg").datagrid("options").url = "/searchAccount?phone=" + content;
        $("#dg").datagrid('reload');
    }
    //动态匹配的搜索
    function dynamicSearch() {
        //键盘输入搜索事件onkeyup
        var content = $("#key").val();
        $("#dg").datagrid("options").url = "/searchAccount?phone=" + content;
        $("#dg").datagrid('reload');

    }

    //删除的ajax请求
    function deleteAccount(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.useraccountid);
            }
        });

        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {

                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
                    $.get("deleteAccount?accountIds=" + userid, function (data, status) {
                        if (data == "true") {
                            $.messager.alert("温馨提示", "删除成功", 'info');
                            $("#dg").datagrid('reload');
                        }
                        if (data == "false") {
                            $.messager.alert("温馨提示", "删除失败", 'error');
                        }
                    });
                }
            })
        }else {
            $.messager.alert("温馨提示", "请选择要删除的行", 'info');
		}
    }

	//头像显示成照片
    function showImg(touxiang){
        var img=touxiang.replace(/[^0-9]/ig,"");
        return '<img id="img1" src="'+touxiang+'" style="width:30px;height:30px;"/>';
    }


    //自定义整数排序
    function numberSort(a,b){
        var number1 = parseFloat(a);
        var number2 = parseFloat(b);
        return (number1 > number2 ? 1 : -1);
    }

</script>

<%--返回 add/update  "${sessionScope.user.username}" 的消息--%>
<c:choose>
	<c:when  test="${addAccount  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加成功!','info');
		</script>
	</c:when>

	<c:when  test="${addAccount  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>


<c:choose>
	<c:when  test="${updateAccount  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改成功!','info');
		</script>
	</c:when>

	<c:when  test="${updateAccount  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>

</body>
</html>