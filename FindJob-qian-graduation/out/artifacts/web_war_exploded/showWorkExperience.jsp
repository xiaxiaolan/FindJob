<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
<head>
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>工作经验</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>

	<style type="text/css">
		input[id="companyname"],input[id="name"]{
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 130px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 45px;
		}
		input[id="companyname"]:hover,
		input[id="companyname"]:focus,
		input[id="name"]:hover,
		input[id="name"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>

	<style type="text/css">
		body{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>

<body>

<table id="dg" title="工作经验" class="easyui-datagrid" style="width:100%;height:100%"
	   url="/showWork"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="false" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'id',width:100,align:'center',sortable:true" sorter="numberSort">编号</th>
		<th data-options="field:'starttimeyear',width:140,align:'center'">开始年份</th>
		<th data-options="field:'starttimemonth',width:100,align:'center'">开始月份</th>
		<th data-options="field:'endtimeyear',width:140,align:'center'">结束年份</th>
		<th data-options="field:'endtimemonth',width:100,align:'center'">结束月份</th>
		<th data-options="field:'companyname',width:100,align:'center'">入职公司</th>
		<th data-options="field:'department',width:100,align:'center'">描述</th>
		<th data-options="field:'zhiwei',width:100,align:'center'">任职</th>
		<th data-options="field:'workdesc',width:100,align:'center'">职位描述</th>
		<th data-options="field:'jianliid',width:100,align:'center',sortable:true" sorter="numberSort">简历编号</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:20px 20px 17px 25px;">
	<span>
		<input id="companyname" placeholder="曾任职公司" name="companyname" onkeyup="companynameSearch()"/>
		<%--<input id="dutydes" placeholder="职位描述" name="dutydes" onkeyup="dutydesSearch()"/>--%>

		<input id="name" placeholder="求职者姓名" name="name" onkeyup="nameSearch()"/>
    </span>
	<a style="margin-left:40px;margin-bottom:5px" href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="searchall()">Search</a>
	<a style="margin-left:20px;margin-bottom:5px" href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteAccount()">Detete</a>
	<%--<a style="margin-left:20px;margin-top:5px" href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newAccount()">Add</a>--%>
	<%--<a style="margin-left:20px;margin-top:5px" href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editAccount()">Edit</a>--%>
</div>


<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons">
</div>
<div id="dlg-buttons">
	<a href="#" class="easyui-linkbutton" iconCls="icon-ok" onclick="saveAccount()">Save</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="clearForm()" style="width:80px">Clear</a>
	<%--<a href="#" class="easyui-linkbutton" iconCls="icon-cancel" onclick="javascript:$('#dlg').dialog('close')">Cancel</a>--%>
</div>

<script type="text/javascript">
    //弹出框add
    var url;
    function newAccount(){
        $('#dlg').dialog('open').dialog('setTitle','New User');
        $('#fm').form('clear');
        url = 'admin/ShoeAction!addshoe.action';
    }
    //弹出框edit
    function editAccount(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行用户进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的用户", "info");
        }else{
            $('#dlg').dialog('open').dialog('setTitle','Edit User');
            $('#fm').form('load',row);
            url = 'admin/ShoeAction!updateshoe.action';
        }
    }


    //绑定搜索链接按钮点击事件	，点击搜索时发送请求
    function nameSearch(){
        var name = $("#name").val();
        $("#dg").datagrid("options").url = "/searchWorkByName?name=" + name;
        $("#dg").datagrid('reload');
    }

    function all() {
        var companyname = $("#companyname").val();
        $("#dg").datagrid("options").url = "/searchWorkByConditions?companyname=" + companyname;
        $("#dg").datagrid('reload');
    }
    //动态匹配的搜索
    function companynameSearch() {
       all();
    }
    //点击按钮
    function searchall(){ all();}

    //删除的ajax请求
    function deleteAccount(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.id);
            }
        });
        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {
                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
                    $.get("/deleteWork?workIds=" + userid, function (data, status) {
                        if (data == "true") {
                            $.messager.alert("温馨提示", "删除成功", 'info');
                            $("#dg").datagrid('reload');
                        }
                        if (data == "false") {
                            $.messager.alert("温馨提示", "删除失败", 'error');
                        }
                    });
                }
            })
        }else {
            $.messager.alert("温馨提示", "选择要删除的行", 'info');
        }
    }

    //自定义整数排序
    function numberSort(a,b){
        var number1 = parseFloat(a);
        var number2 = parseFloat(b);
        return (number1 > number2 ? 1 : -1);
    }

</script>

</body>
</html>