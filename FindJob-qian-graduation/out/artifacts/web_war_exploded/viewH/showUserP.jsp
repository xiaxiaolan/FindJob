<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>所有用户</title>

	<link rel="stylesheet" type="text/css" href="../../easyui/demo/demo.css">
	<link rel="stylesheet" type="text/css" href="../../easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="../../easyui/themes/icon.css">
	<script type="text/javascript" src="../../easyui/jquery.min.js"></script>
	<script type="text/javascript" src="../../easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="../../easyui/locale/easyui-lang-zh_CN.js"></script>

</head>
<body>

	<div id="tb" style="width:100%;padding:10px">
		<span>用户名：   </span>
		<input id="orderId" style="line-height:26px;margin-right:50px;border:1px solid #ccc">
		<a href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search">Search</a>
        <a href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add">add</a>
        <a href="javascript:void(0)" id="cut" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut">删除</a>
        <a href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit">编辑</a>
        <a href="javascript:void(0)" id="notedit" class="easyui-linkbutton"  plain="true"  iconCls="icon-cancel">取消编辑</a>
        <a href="javascript:void(0)" id="save" class="easyui-linkbutton"  plain="true"  iconCls="icon-save">保存</a>
    </div>
 
	<table id="tt" class="easyui-datagrid" style="width:100%;height:600px"
		url="/showAccount" toolbar="#tb"
		title="全部订单" iconCls="icon-cart"
		rownumbers="true" pagination="true"  singleSelect="false" onClickCell="onClickCell">
		<thead>
			<tr>
			    <th field="ck" checkbox="true"></th>
				<th field="userid" width="90" align="center" >用户编号</th>
				<th field="image" width="130" align="center">头像</th>
				<th field="username" width="150" align="center" editor="text">用户名</th>
				<th field="password" width="170" align="center" editor="text">密码</th>
				<th field="sex" width="100" align="center" editor="text">性别</th>
				<th field="tel" width="170" align="center"  editor="text">电话</th>
				<th field="useraddress" width="190" align="center"  editor="text">地址</th>
				<th field="edit" width="200" align="center">
				   <a href="#" id="delete" class="easyui-linkbutton" plain="true" iconCls="icon-cancel">删除</a>
				   <a href="#" id="modify" class="easyui-linkbutton" plain="true" iconCls="icon-reload">编辑</a>				   
				</th>	
			</tr>
		</thead>
    </table>


<script type="text/javascript">
            /* var pager = $('#tt').datagrid('getPager');    // get the pager of datagrid
            $(pager).pagination({
                showPageList:false,
            }); */


		$(function(){  //绑定搜索链接按钮点击事件	，点击搜索时发送请求 
				$("#search").on('click', function(){
					    var content=$("#orderId").val();
					   alert(content);
						$("#tt").datagrid("options").url="UserAction!showSearchUsers.action?key="+content;
				        $("#tt").datagrid('reload');
				    });
			 
			    $("#orderId").on('keyup',function(){//键盘输入搜索事件
			    	//alert("键盘事件");
			    	var content=$(this).val();
					  //  alert(content);
						$("#tt").datagrid("options").url="UserAction!showSearchUsers.action?key="+content;
				        $("#tt").datagrid('reload');
			    	
			    })
			    
			    //删除行事件
			    $("#cut1").on('click', function(){
			    	 var checkedItems = $("#tt").datagrid('getChecked');
			    	 var userid = [];
			    	// console.log(checkedItems);
			    	 $.each(checkedItems, function(index, item){
					    	if(item=="" || item==null){
	
					    	}else{
					    	    userid.push(item.userid);
					    	}
					  });  
			    	     
			    	/*  for(j=0;j<userid.length;j++){  
				              var index = $("tt").datagrid('getRowIndex',userid[j])//循环Ele里面的数据，根据几条数据（几行）获取到对应数量的索引。  
				              $("tt").datagrid('deleteRow',index)//根据索引删除对应的行
			    	       }  */
	                  if(userid.length>0){
						  $.messager.confirm("提示", "你确定要删除本行数据吗?", function (r) {
	                          if (r) {
	                        	 $("#tt").datagrid("options").url="UserAction!deleteUsers.action?userids="+userid.toString();
	         					 $("#tt").datagrid('reload');
	                          }
						  })
					  }
			      }); 
			    
			    
			    //删除的ajax请求
			    $('#cut').on('click',function(){
			    	 var checkedItems = $("#tt").datagrid('getChecked');
			    	 var userid = [];
			    	// console.log(checkedItems);
			    	 $.each(checkedItems, function(index, item){
					    	if(item=="" || item==null){
	
					    	}else{
					    	    userid.push(item.userid);
					    	}
					  });  
			    	 if(userid.length>0){
						  $.messager.confirm("提示", "你确定要删除本行数据吗?", function (r) {
	                          if (r) {
	                        	  $.get("UserAction!deleteUsers.action?userids="+userid.toString(),function(data,status){
	         			    		 if(data=="true"){
	         					    	 $.messager.alert("温馨提示","删除成功",'info');
	         					       	 $("#tt").datagrid('reload');
	         					     }
	         					     if(data=="false"){
	         					    	 $.messager.alert("温馨提示","删除失败",'error');
	         					     }
	         					 });
	                          }
						  })
					  }	    	 
				});
			    
	    		var uname;
	    		var upass;
	    		var usex;
	    		var utel;
	    		var uadd;
	    		
			    var editRow=undefined;
			    var rows;
			    //编辑事件
			    $('#edit').on('click',function(){
					//修改时要获取选择到的行
			        rows = $('#tt').datagrid("getSelections");
			        //如果只选择了一行则可以进行修改，否则不操作
			        if (rows.length > 1){
			        	$.messager.alert("提示", "请选择单行用户进行修改", "info");
			        }else if (rows.length == 0){
			        	$.messager.alert("提示", "请选择要修改的用户", "info");
			        }
			        else if (rows.length == 1) {
			            //修改之前先关闭已经开启的编辑行，当调用endEdit该方法时会触发onAfterEdit事件
			            if (editRow != undefined) {
			            	$('#tt').datagrid("endEdit", editRow);
			            }
			            //当无编辑行时
			            if (editRow == undefined) {
			                //获取到当前选择行的下标
			                var index = $('#tt').datagrid("getRowIndex", rows[0]);
			               
			                //修改之前先保存原始数据
				    		uname=rows[0].username;
				    		upass=rows[0].password;
				    		usex=rows[0].sex;
				    		utel=rows[0].tel;
				    		uadd=rows[0].useraddress;
			                
			                
			                //开启编辑
			                $('#tt').datagrid("beginEdit", index);
			                //把当前开启编辑的行赋值给全局变量editRow
			                editRow = index;
			                
			                //当开启了当前选择行的编辑状态之后，
			                //应该取消当前列表的所有选择行，要不然双击之后无法再选择其他行进行编辑
							// $('#dg').datagrid("unselectAll");
			            }
			        }
				});

			    $('#notedit').on('click',function(){
			    	if(rows.length > 0){
			    		if (editRow != undefined){
				    		rows[0].username=uname;
				    		rows[0].password=upass;
				    		rows[0].sex=usex;
				    		rows[0].tel=utel;
			            	$('#tt').datagrid("endEdit", editRow);
			            	editRow = undefined;
			            }
			    	}
	                
		    		
	
			    });
			    //点击保存时做的事情
			    $('#save').on('click',function(){
			    	if(rows.length > 0){
			    		if (editRow != undefined) {
			            	$('#tt').datagrid("endEdit", editRow);
			            	editRow = undefined;
			            	
			            	var uid=rows[0].userid;
				    		var uname=rows[0].username;
				    		var upass=rows[0].password;
				    		var usex=rows[0].sex;
				    		var utel=rows[0].tel;
				    		var uadd=rows[0].useraddress;
				    		 $.get("UserAction!updateUsers.action?uname="+uname+"&upass="+upass+"&usex="+usex+"&utel="+utel+"&uadd="+uadd+"&uid="+uid,function(data,status){
	     			    		 if(data=="true"){
	     					    	 $.messager.alert("温馨提示","修改成功",'info');
	     					       	 $("#tt").datagrid('reload');
	     					     }
	     					     if(data=="false"){
	     					    	 $.messager.alert("温馨提示","修改失败",'error');
	     					    	 $("#tt").datagrid('reload');
	     					     }
	     					 });
			            }
			    	}
			    });
			    
			   /*  $("#add").on('click', function(){
			        $("tt").datagrid('insertRow',{ //在右边插入新行。  
		                  index : 0,  
		                  row : {  
		                     
		                  }  
	                })
			    }); */
			    
			    
			    //添加新行
			    /* $("#add").on('click', function(){
			    	 var rows = $("tt").datagrid('getSelections');//这里是用getSelections获取到选中的行的数据。  
					    var Ele = []//新建一个空的数组  
					    for(i=0;i<rows.length;i++){//循环，把行的数组拆分。  
					          Ele.push(rows[i]);//把拆分好的数据用push的方法保存到数组Ele中。  
					    }  
					    for(j=0;j<Ele.length;j++){  
					          var index = $("tt").datagrid('getRowIndex',Ele[j])//循环Ele里面的数据，根据几条数据（几行）获取到对应数量的索引。  
					           $("tt").datagrid('deleteRow',index)//根据索引删除对应的行。  
					            $("tt").datagrid('insertRow',{ //在右边插入新行。  
					                  index : 0,  
					                  row : {  
					                     name : Ele[j].name  
					                  }  
					             })  
					    }
			    }); */
		  
		});
		    
		    /* $("#cut").on('click', function(){
		    	 var checkedItems = $("#tt").datagrid('getChecked');
		    	 var userid = [];
		    	// console.log(checkedItems);
		    	 $.each(checkedItems, function(index, item){
				    	if(item=="" || item==null){

				    	}else{
				    	    userid.push(item.userid);
				    	}
				  });  
				  alert(userid);
				//$("#tt").datagrid("options").url="UserAction!showSearchUsers.action?key="+content;
			      //$("#tt").datagrid('reload');
			    }); */
		   
	  
				//$("#delete").live('click',function(){})
	
				
	   
		
		//var page= $(".pagination-num").val();//取出分页的文本框值	    
		
		/* $('#tt').datagrid('load',{
			orderId: $('#orderId').val(),
			orderTime: $('#orderTime').val()
		}); */
</script>

<script type="text/javascript">

		$.extend($.fn.datagrid.methods, {
		    editCell : function(jq, param) {
		        return jq.each(function() {
		            var opts = $(this).datagrid('options');
		            var fields = $(this).datagrid('getColumnFields', true).concat(
		                    $(this).datagrid('getColumnFields'));
		            for ( var i = 0; i < fields.length; i++) {
		                var col = $(this).datagrid('getColumnOption', fields[i]);
		                col.editor1 = col.editor;
		                if (fields[i] != param.field) {
		                    col.editor = null;
		                }
		            }
		            $(this).datagrid('beginEdit', param.index);
		            for ( var i = 0; i < fields.length; i++) {
		                var col = $(this).datagrid('getColumnOption', fields[i]);
		                col.editor = col.editor1;
		            }
		        });
		    }
		});
		
		var editIndex = undefined;
		//结束编辑 
		function endEditing() {
		    if (editIndex == undefined) {
		        return true
		    }
		    if ($('#tt').datagrid('validateRow', editIndex)) {
		        $('#tt').datagrid('endEdit', editIndex);
		        editIndex = undefined;
		        return true;
		    } else {
		        return false;
		    }
		}
		//单击单元格 
		function onClickCell(index, field) {
		    if (endEditing()) {
		        $('#tt').datagrid('selectRow', index).datagrid('editCell', {
		            index : index,
		            field : field
		        });
		        editIndex = index;
		    }
		}
</script>

<script>
		function alert1(title,message){
			$.messager.alert(title,message);
		}
		
		function errorMessage(title,message){
			$.messager.alert(title,message,'error');
		}
		
		function infoMessage(title,message){
			$.messager.alert(title,message,'info');
		}
		function questionMessage(title,message){
			$.messager.alert(title,message,'question');
		}
		function warningMessage(title,message){
			$.messager.alert(title,message,'warning');
		}
	</script>
	

<c:if test="${requestScope.Message eq 'deleteFail'}">
	 <script type="text/javascript">
	//当用户在后天验证登陆失败时存储一个字段，到这里判断
        $.messager.alert('温馨提示','删除失败!','error');
     </script>
</c:if>

<c:if test="${requestScope.Message eq 'deleteSuccess'}">
	 <script type="text/javascript">
	   infoMessage("消息","删除成功");
	   $("#tt").datagrid('reload');
     </script>
</c:if>


</body>
</html>