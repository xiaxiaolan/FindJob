package main.com.findjob.daoGraduation;

import main.com.findjob.pojoGraduation.Medicine_info;
import main.com.findjob.pojoGraduation.Medicine_infoExample;
import main.com.findjob.pojoGraduation.Medicine_infoWithBLOBs;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface Medicine_infoMapper {
    int countByExample(Medicine_infoExample example);

    int deleteByExample(Medicine_infoExample example);

    int deleteByPrimaryKey(Integer medicineid);

    int insert(Medicine_infoWithBLOBs record);

    /*  */
    int insertSelective(Medicine_info record);

    List<Medicine_infoWithBLOBs> selectByExampleWithBLOBs(Medicine_infoExample example);

    List<Medicine_info> selectByExample(Medicine_infoExample example);

    Medicine_infoWithBLOBs selectByPrimaryKey(Integer medicineid);

    int updateByExampleSelective(@Param("record") Medicine_infoWithBLOBs record, @Param("example") Medicine_infoExample example);

    int updateByExampleWithBLOBs(@Param("record") Medicine_infoWithBLOBs record, @Param("example") Medicine_infoExample example);

    int updateByExample(@Param("record") Medicine_info record, @Param("example") Medicine_infoExample example);

    /*  */
    int updateByPrimaryKeySelective(Medicine_info record);

    int updateByPrimaryKeyWithBLOBs(Medicine_infoWithBLOBs record);

    int updateByPrimaryKey(Medicine_info record);


    public ArrayList<Medicine_info> getAllMedicines(@Param("comid") Integer comid);
    public ArrayList<Medicine_info> getMedicinesByConditions(Medicine_info medicine_info);
    public boolean updateStatusByMedicineid(Medicine_info medicine_info);
}