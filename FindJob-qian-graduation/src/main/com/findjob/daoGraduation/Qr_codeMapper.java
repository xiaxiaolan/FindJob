package main.com.findjob.daoGraduation;

import java.util.List;
import main.com.findjob.pojoGraduation.Qr_code;
import main.com.findjob.pojoGraduation.Qr_codeExample;
import org.apache.ibatis.annotations.Param;

public interface Qr_codeMapper {
    int countByExample(Qr_codeExample example);

    int deleteByExample(Qr_codeExample example);

    int deleteByPrimaryKey(Integer qrcodeid);

    int insert(Qr_code record);

    int insertSelective(Qr_code record);

    List<Qr_code> selectByExample(Qr_codeExample example);

    Qr_code selectByPrimaryKey(Integer qrcodeid);

    int updateByExampleSelective(@Param("record") Qr_code record, @Param("example") Qr_codeExample example);

    int updateByExample(@Param("record") Qr_code record, @Param("example") Qr_codeExample example);

    int updateByPrimaryKeySelective(Qr_code record);

    int updateByPrimaryKey(Qr_code record);
}