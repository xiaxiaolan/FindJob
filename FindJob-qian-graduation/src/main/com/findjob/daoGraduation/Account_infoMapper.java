package main.com.findjob.daoGraduation;

import java.util.ArrayList;
import java.util.List;
import main.com.findjob.pojoGraduation.Account_info;
import main.com.findjob.pojoGraduation.Account_infoExample;
import org.apache.ibatis.annotations.Param;

public interface Account_infoMapper {
    int countByExample(Account_infoExample example);

    int deleteByExample(Account_infoExample example);

    int deleteByPrimaryKey(Integer accountid);

    int insert(Account_info record);

    int insertSelective(Account_info record);

    List<Account_info> selectByExample(Account_infoExample example);

    Account_info selectByPrimaryKey(Integer accountid);

    int updateByExampleSelective(@Param("record") Account_info record, @Param("example") Account_infoExample example);

    int updateByExample(@Param("record") Account_info record, @Param("example") Account_infoExample example);

    int updateByPrimaryKeySelective(Account_info record);

    int updateByPrimaryKey(Account_info record);

    int registerAccount(Account_info record);
    Account_info selectAccountByName(@Param("accountName") String accountName);

    public boolean updateAccount(Account_info account_info);
    public ArrayList<Account_info> getAllAccounts(@Param("accountName") String accountName);
}