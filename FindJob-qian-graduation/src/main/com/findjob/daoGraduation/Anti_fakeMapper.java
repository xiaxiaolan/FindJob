package main.com.findjob.daoGraduation;

import java.util.ArrayList;
import java.util.List;
import main.com.findjob.pojoGraduation.Anti_fake;
import main.com.findjob.pojoGraduation.Anti_fakeExample;
import org.apache.ibatis.annotations.Param;

public interface Anti_fakeMapper {
    int countByExample(Anti_fakeExample example);

    int deleteByExample(Anti_fakeExample example);

    int deleteByPrimaryKey(Integer antiFakeId);

    int insert(Anti_fake record);

    int insertSelective(Anti_fake record);

    List<Anti_fake> selectByExample(Anti_fakeExample example);

    Anti_fake selectByPrimaryKey(Integer antiFakeId);

    int updateByExampleSelective(@Param("record") Anti_fake record, @Param("example") Anti_fakeExample example);

    int updateByExample(@Param("record") Anti_fake record, @Param("example") Anti_fakeExample example);

    int updateByPrimaryKeySelective(Anti_fake record);

    int updateByPrimaryKey(Anti_fake record);


    ArrayList<Anti_fake> searchAntiCode(Anti_fake anti_fake);
    ArrayList<Anti_fake> getAntiCodes();
    Anti_fake selectAntiCodeByInsert(Anti_fake anti_fake);
}