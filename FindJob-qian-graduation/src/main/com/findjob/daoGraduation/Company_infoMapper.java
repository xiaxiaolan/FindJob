package main.com.findjob.daoGraduation;

import main.com.findjob.pojoGraduation.Company_info;
import main.com.findjob.pojoGraduation.Company_infoExample;
import org.apache.ibatis.annotations.Param;

import java.util.ArrayList;
import java.util.List;

public interface Company_infoMapper {
    int countByExample(Company_infoExample example);

    int deleteByExample(Company_infoExample example);

    int deleteByPrimaryKey(Integer comid);

    int insert(Company_info record);

    int insertSelective(Company_info record);

    List<Company_info> selectByExampleWithBLOBs(Company_infoExample example);

    List<Company_info> selectByExample(Company_infoExample example);

    Company_info selectByPrimaryKey(Integer comid);

    int updateByExampleSelective(@Param("record") Company_info record, @Param("example") Company_infoExample example);

    int updateByExampleWithBLOBs(@Param("record") Company_info record, @Param("example") Company_infoExample example);

    int updateByExample(@Param("record") Company_info record, @Param("example") Company_infoExample example);

    int updateByPrimaryKeySelective(Company_info record);

    int updateByPrimaryKeyWithBLOBs(Company_info record);

    int updateByPrimaryKey(Company_info record);



   public  ArrayList<Company_info> getCompanysByConditions(Company_info company_info);
   public  ArrayList<Company_info> getAllCompanys(@Param("accountid") Integer accountid);
   public boolean updateAllByPrimaryKey(Company_info company_info);
}