package main.com.findjob.daoGraduation;

import java.util.List;
import main.com.findjob.pojoGraduation.Shirobean;
import main.com.findjob.pojoGraduation.ShirobeanExample;
import org.apache.ibatis.annotations.Param;

public interface ShirobeanMapper {
    int countByExample(ShirobeanExample example);

    int deleteByExample(ShirobeanExample example);

    int insert(Shirobean record);

    int insertSelective(Shirobean record);

    List<Shirobean> selectByExample(ShirobeanExample example);

    int updateByExampleSelective(@Param("record") Shirobean record, @Param("example") ShirobeanExample example);

    int updateByExample(@Param("record") Shirobean record, @Param("example") ShirobeanExample example);
}