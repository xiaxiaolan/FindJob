package main.com.findjob.daoGraduation;

import java.util.List;
import main.com.findjob.pojoGraduation.Scan_record;
import main.com.findjob.pojoGraduation.Scan_recordExample;
import org.apache.ibatis.annotations.Param;

public interface Scan_recordMapper {
    int countByExample(Scan_recordExample example);

    int deleteByExample(Scan_recordExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Scan_record record);

    int insertSelective(Scan_record record);

    List<Scan_record> selectByExample(Scan_recordExample example);

    Scan_record selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Scan_record record, @Param("example") Scan_recordExample example);

    int updateByExample(@Param("record") Scan_record record, @Param("example") Scan_recordExample example);

    int updateByPrimaryKeySelective(Scan_record record);

    int updateByPrimaryKey(Scan_record record);
}