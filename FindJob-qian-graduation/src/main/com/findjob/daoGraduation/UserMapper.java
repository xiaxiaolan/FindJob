package main.com.findjob.daoGraduation;

import main.com.findjob.pojoGraduation.Shirobean;
import main.com.findjob.pojoGraduation.User;

import java.util.ArrayList;

public interface UserMapper {
//管理员账号
    //添加用户
    public boolean addUser(User user) throws Exception;
    //修改用户
    public boolean updateUser(User user) throws Exception;
    //删除用户
    public boolean deleteUser(int[] ids) throws Exception;
    //根据username查用户信息
    public User getUserByUsername(String username) throws Exception;
    //根据username模糊查用户信息
    public ArrayList<User> getUserByUsernam(String username) throws Exception;
    //查all用户信息
    public ArrayList<User> getAllUsers() throws Exception;
    //查询账号信息byId
    public  User getAdminById(Integer id) throws Exception;


//getRoleByUsername 获取某个用户所拥有的权限
    public ArrayList<String> getRoleByUsername(String username) throws Exception;
    //查询出所有用户的权限 封装成一个shirobean 显示的时候用
    public ArrayList<Shirobean> getAllRole() throws Exception;
    //查询出某个用户的权限
    public ArrayList<Shirobean> getShiroByUsername(String username) throws Exception;
    //为username添加一个权限
    public boolean addShiro(Shirobean shirobean) throws Exception;
    //更新
    public boolean updateShiro(Shirobean shirobean) throws Exception;
    //通过用户名查id
    public Integer getIdByUsername(String username) throws Exception;
    public boolean deleteShiro(Shirobean shirobean) throws Exception;

    //查询账号信息byId
    public  User getAccountByUsername(String username) throws Exception;

    //删除Account
    public boolean deleteAccount(int[] ids) throws Exception;
}
