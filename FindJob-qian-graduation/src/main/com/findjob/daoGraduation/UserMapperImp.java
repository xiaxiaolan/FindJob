package main.com.findjob.daoGraduation;

import main.com.findjob.pojoGraduation.Shirobean;
import main.com.findjob.pojoGraduation.User;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.springframework.stereotype.Component;
import main.com.findjob.daoGraduation.UserMapper;
import java.util.ArrayList;

@Component("userDAO")
public class UserMapperImp extends BaseDAO implements UserMapper {
    private UserMapper dao;
//注册用户
    public boolean addUser(User user) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.addUser(user);
    }
//通过username判断账号是否存在
    public User getUserByUsername(String username)  throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getUserByUsername(username);
    }
//根据username模糊查用户信息
    public ArrayList<User> getUserByUsernam(String username) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getUserByUsernam(username);
    }
//查all用户信息
    public ArrayList<User> getAllUsers() throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getAllUsers();
    }
//修改用户
    public boolean updateUser(User user) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.updateUser(user);
    }
//删除用户
    public boolean deleteUser(int[] ids) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.deleteUser(ids);
    }
//查询账号信息byId
    public  User getAdminById(Integer id) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getAdminById(id);
    }

//getRoleByUsername 获取某个用户所拥有的权限
    public ArrayList<String> getRoleByUsername(String username) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getRoleByUsername(username);
    }
//查询出所有用户的权限
    public ArrayList<Shirobean> getAllRole() throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getAllRole();
    }
//查询出某个用户的权限
    public ArrayList<Shirobean> getShiroByUsername(String username) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getShiroByUsername(username);
    }
//为username添加一个权限
    public boolean addShiro(Shirobean shirobean) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.addShiro(shirobean);
    }
//更新一个权限
    public boolean updateShiro(Shirobean shirobean) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.updateShiro(shirobean);
    }
    //通过用户名查id
    public Integer getIdByUsername(String username) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getIdByUsername(username);
    }
    //删除权限
    public boolean deleteShiro(Shirobean shirobean) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.deleteShiro(shirobean);
    }

//根据id 删除Account
    @RequiresRoles({"role2"})
    public boolean deleteAccount(int[] ids) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.deleteAccount(ids);
    }


    //查询admin
    public  User getAccountByUsername(String username) throws Exception{
        dao=getSqlSession().getMapper(UserMapper.class);
        return dao.getAccountByUsername(username);
    }
}
