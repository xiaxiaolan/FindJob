package main.com.findjob.controler;

import main.com.findjob.pojoGraduation.Account_info;
import main.com.findjob.pojoGraduation.User;
import main.com.findjob.service.AccountService;
import main.com.findjob.service.ResponserByajax;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller("AccountControl")
public class AccountControl {
    private AccountService accountService;

    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private ResponserByajax responserByajax;
    public void setResponserByajax(ResponserByajax responserByajax) {
        this.responserByajax = responserByajax;
    }

    //处理管理员登陆方法
    @RequestMapping(value = "/accountLogin")
    public ModelAndView accountLogin(@ModelAttribute("account_info") Account_info account_info, ModelAndView modelAndView, Model model, HttpSession session) {
        System.out.println(account_info.toString());
        Account_info account = accountService.getAccountByName(account_info.getAccountname());
        if (account != null) {
            Subject currentUser = SecurityUtils.getSubject();
            if (!currentUser.isAuthenticated()) {
                // 把用户名和密码封装为 UsernamePasswordToken 对象
                UsernamePasswordToken token = new UsernamePasswordToken(account_info.getAccountname(), account_info.getPassword());
                // rememberme
                token.setRememberMe(true);
                try {
                    currentUser.login(token);
                    session.setAttribute("account_info",account_info);
                    modelAndView.setViewName("index");
                    return modelAndView;
                } catch (AuthenticationException ae) {
                    System.out.println("登录失败: " + ae.getMessage());
                    model.addAttribute("loginMessage", "fail_password");
                    modelAndView.setViewName("login");
                    return modelAndView;
                }
            }else {
                session.setAttribute("account_info",account_info);
                modelAndView.setViewName("index");
                return modelAndView;
            }

        } else {
            model.addAttribute("loginMessage", "fail_username");
            modelAndView.setViewName("login");
            return modelAndView;
        }
    }


    //处理管理员注册方法
    @RequestMapping(value = "/accountRegister",method = RequestMethod.POST)
    public ModelAndView accountRegister(@ModelAttribute("account_info") Account_info account_info, ModelAndView modelAndView, Model model, HttpSession session) {
        System.out.println(account_info.toString());
        if(account_info.getAccountname() == null || "".equals(account_info.getAccountname().trim())){
            model.addAttribute("registerMessage", "null");
            modelAndView.setViewName("login");
            return modelAndView;
        }
        if(account_info.getPassword() == null || "".equals(account_info.getPassword().trim())){
            model.addAttribute("registerMessage", "null");
            modelAndView.setViewName("login");
            return modelAndView;
        }
        Account_info account = accountService.getAccountByName(account_info.getAccountname());
        if (account == null) {
            int rs = accountService.addAccount(account_info);//1注册成功
            model.addAttribute("registerMessage", rs+"");
            modelAndView.setViewName("login");
            return modelAndView;
        }else{
            model.addAttribute("registerMessage", "fail");
            modelAndView.setViewName("login");
            return modelAndView;
        }
    }

    //显示Account信息的方法
    @RequestMapping("/showAccount")
    public void showAccount(HttpServletRequest request, HttpServletResponse response, HttpSession session){
        Account_info account_info = (Account_info) session.getAttribute("account_info");
        ArrayList<Account_info> allAccounts = accountService.processShowAccount(account_info.getAccountname());
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allAccounts));
    }

    @RequestMapping(value = "/getAccountById",params = "accountid")
    public void getAccountById(HttpServletRequest request, HttpServletResponse response, Integer accountid){
        Account_info account_info = accountService.processGetAccountById(accountid);
        responserByajax.outOneJSONTo(request,response,responserByajax.writeToJSONObject(account_info));
    }

    //表单修改Account信息的方法
    @RequestMapping(value = "/updateAccount",method = RequestMethod.POST)
    public ModelAndView updateAccount(@ModelAttribute("account")Account_info account_info,ModelAndView modelAndView,HttpSession session){
        boolean rs = accountService.processUpdateAccount(account_info);
        System.out.println(rs);
        modelAndView.addObject("updateAccount",rs+"");
        modelAndView.setViewName("showAccount");
        return modelAndView;
    }

}
