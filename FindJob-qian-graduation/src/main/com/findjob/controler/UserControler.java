package main.com.findjob.controler;
import main.com.findjob.pojoGraduation.Account_info;
import main.com.findjob.pojoGraduation.User;
import main.com.findjob.service.ResponserByajax;
import main.com.findjob.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;

//@SessionAttributes(value = "User1",types = {})
//@RequestMapping()
@Controller("UserControler")
public class UserControler {
    private UserService userService;
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    private ResponserByajax responserByajax;
    public void setResponserByajax(ResponserByajax responserByajax) {
        this.responserByajax = responserByajax;
    }

//文件上传即头像上传
    private File image;
    private String imageFileName;
    private String imageContentType;
    private String fileN;

//管理后台的Account模块，包括account账号和user的管理

/*
//显示Account信息的方法
    @RequestMapping("/showAccount")
    public void showAccount(HttpServletRequest request, HttpServletResponse response){
        ArrayList<Account> allAccounts=userService.processShowAccount();
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allAccounts));
    }
*/

/*//添加账号的方法，注意不能为空的字段 前台限制必须选择：(0，1)
    @RequestMapping(value = "/addAccount",method = RequestMethod.POST)
    public ModelAndView addAccount(@ModelAttribute("account")Account account,ModelAndView modelAndView,HttpSession session){
        //前台没给头像，后台进来给一个
        account.setTouxiang("images/touxiang/6.jpg");

        boolean rs=userService.processAddAccount(account);
        System.out.println(rs);
        modelAndView.addObject("addAccount",rs+"");
        modelAndView.setViewName("showAccount");
        return modelAndView;
    }

//表单修改Account信息的方法
    @RequestMapping(value = "/updateAccount",method = RequestMethod.POST)
    public ModelAndView updateAccount(@ModelAttribute("account")Account account,ModelAndView modelAndView,HttpSession session){
        boolean rs=userService.processUpdateAccount(account);
        System.out.println(rs);
        modelAndView.addObject("updateAccount",rs+"");
        modelAndView.setViewName("showAccount");
        return modelAndView;
    }

//ajax删除account的方法
    @RequestMapping(value = "/deleteAccount",params = "accountIds")
    public void deleteAccount(HttpServletRequest request, HttpServletResponse response,int[] accountIds){
        boolean rs=userService.processDeleteAccount(accountIds);
        System.out.println(rs);
        responserByajax.responseToText(request,response,rs+"");
    }

//前端ajax，按照手机号phone，模糊查询用户账号信息
    @RequestMapping(value = "/searchAccount",params = "phone")
    public void searchAccount(HttpServletRequest request, HttpServletResponse response,String phone){
        ArrayList<Account> allAccounts=userService.processSearchAccount(phone);
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allAccounts));
    }

//phone查询用户账号信息
    @RequestMapping(value = "/getAccountByPhone",params = "phone")
    public void getAccountByPhone(HttpServletRequest request, HttpServletResponse response,String phone){
        boolean rs=userService.processSearchAAccount(phone);
        System.out.println(rs);
        responserByajax.responseToText(request,response,rs+"");
    }

//id查询用户账号信息
@RequestMapping(value = "/getAccountById",params = "useraccountid")
    public void getAccountById(HttpServletRequest request, HttpServletResponse response,Integer useraccountid,Model model){
        Account account=userService.processGetAccountById(useraccountid);
        //将对象以json对象格式写出去
        responserByajax.outOneJSONTo(request,response, responserByajax.writeToJSONObject(account));
}*/


//更新头像 ，uploadify，ajax ，且将路径返回给前台，预览图片
   /* @RequestMapping(value = "/uploadImage",method = RequestMethod.POST)
    public void updateUserImage(HttpServletRequest request,HttpServletResponse response){
        System.out.printf("进入修改头像的方法");
        String path=request.getRealPath("upload");
        //文件重命名
        fileN= UUID.randomUUID()+imageFileName.substring(imageFileName.lastIndexOf("."),imageFileName.length());
        //构建存储文件对象
        File f=new File(path, fileN);

        try {
            FileUtils.copyFile(image, f);//用apache的fileupload组件里面的文件帮助类直接讲上传的文件拷贝到我们想放置的文件位置上
            responserByajax.responseToText(request,response ,"upload/"+fileN);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//下面是系统用户admin和权限管理界面shiro


    //显示Admin User信息的方法
    @RequestMapping("/showAdmin")
    public void showAdmin(HttpServletRequest request, HttpServletResponse response){
        ArrayList<User> allUsers=userService.processShowAllUsers();
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allUsers));
    }

    @RequestMapping(value = "/addAdmin",method = RequestMethod.POST)
    public ModelAndView addAdmin(@ModelAttribute("user") User user,ModelAndView modelAndView,HttpSession session){
        boolean rs=userService.processRegister(user);
        System.out.println(rs);
        modelAndView.addObject("addAdmin",rs+"");
        modelAndView.setViewName("showAdmin");
        return modelAndView;
    }

    //表单修改Admin信息的方法
    @RequestMapping(value = "/updateAdmin",method = RequestMethod.POST)
    public ModelAndView updateAdmin(@ModelAttribute("user") User user, ModelAndView modelAndView){
        System.out.println(user);
        boolean rs=userService.processUpdateUser(user);
        System.out.println(rs);
        modelAndView.addObject("updateAdmin",rs+"");
        modelAndView.setViewName("showAdmin");
        return modelAndView;
    }

    //表单修改Admin信息的方法
    @RequestMapping(value = "/getAdminById",params = "id")
    public void getAdminById(HttpServletRequest request, HttpServletResponse response,ModelAndView modelAndView,int id){
        User user=userService.processGetAdminById(id);
        System.out.println(user);
        responserByajax.outOneJSONTo(request,response, responserByajax.writeToJSONObject(user));
    }
    //表单修改Admin信息的方法
    @RequestMapping(value = "/getAdminByUsername",params = "username")
    public void getAdminByusername(HttpServletRequest request, HttpServletResponse response,ModelAndView modelAndView,String username){
        boolean rs=userService.processGetAdminByUsername(username);
        responserByajax.responseToText(request,response,rs+"");
    }

    @RequestMapping(value = "/searchAdmin",params="username")
    public void searchAdmin(HttpServletRequest request, HttpServletResponse response,String username){
        ArrayList<User> allUsers=userService.getUserByUsernam(username);
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allUsers));
    }
    //....增删改查
    //ajax删除user的方法
    @RequestMapping(value = "/deleteUser",params = "userIds")
    public void deleteAdmin(HttpServletRequest request, HttpServletResponse response,int[] userIds){
        boolean rs=userService.processDeleteUser(userIds);
        System.out.println(rs);
        responserByajax.responseToText(request,response,rs+"");
    }

    //获取所有的bean
    @RequestMapping("/showShiro")
    public void showShiro(HttpServletRequest request, HttpServletResponse response){
        ArrayList<Shirobean> allRoleShiro=userService.processGetAllRole();
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allRoleShiro));
    }

    //addShiro
    @RequestMapping("/addShiro")
    public ModelAndView addShiro(HttpServletRequest request, HttpServletResponse response,ModelAndView modelAndView,
                                 @RequestParam("username") String username,@RequestParam("roleid") Integer roleid){
        boolean rs=userService.processAddShiro(username,roleid);
        System.out.println(rs);
        modelAndView.addObject("addShiro",rs+"");
        modelAndView.setViewName("showShiro");
        return modelAndView;
    }

    //updateShiro
    @RequestMapping("/updateShiro")
    public ModelAndView updateShiro(HttpServletRequest request, HttpServletResponse response,ModelAndView modelAndView,
                                    @RequestParam("id") Integer id,@RequestParam("roleid") Integer roleid,
                                    @RequestParam("oldroleid") Integer oldroleid){
        Shirobean shirobean=new Shirobean(id,roleid,oldroleid);
        boolean rs=userService.processUpdateShiro(shirobean);
        System.out.println(rs);
        modelAndView.addObject("updateShiro",rs+"");
        modelAndView.setViewName("showShiro");
        return modelAndView;
    }
    //updateShiro
    @RequestMapping("/deleteShiro")
    public void deleteShiro(HttpServletRequest request, HttpServletResponse response,ModelAndView modelAndView,
                                    @RequestParam("id") Integer id[],@RequestParam("roleid") Integer roleid[]){
        boolean rs=userService.processDeleteShiro(id,roleid);
        responserByajax.responseToText(request,response,rs+"");
    }


    //查询某个用户的权限shirobean
    @RequestMapping(value = "/searchShiro",params = "username")
    public void searchShiro(HttpServletRequest request, HttpServletResponse response,String username){
        ArrayList<Shirobean> allRoleShiro=userService.processGetShiroByName(username);
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allRoleShiro));
    }

*/

}
