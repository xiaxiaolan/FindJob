package main.com.findjob.controler;

import main.com.findjob.pojoGraduation.Account_info;
import main.com.findjob.pojoGraduation.Company_info;
import main.com.findjob.service.AccountService;
import main.com.findjob.service.CompanyService;
import main.com.findjob.service.ResponserByajax;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller("CompanyControl")
public class CompanyControl {
    private CompanyService companyService;
    private AccountService accountService;

    public void setCompanyService(CompanyService companyService) {
        this.companyService = companyService;
    }
    public void setAccountService(AccountService accountService) {
        this.accountService = accountService;
    }

    private ResponserByajax responserByajax;
    public void setResponserByajax(ResponserByajax responserByajax) {
        this.responserByajax = responserByajax;
    }


    //显示Company信息的方法
    @RequestMapping("/showCompany")
    public void showCompany(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
        Account_info account_info = (Account_info) session.getAttribute("account_info");//取出登陆的账号，通过关联关系找出相关公司
        Account_info account_info1 = accountService.getAccountByName(account_info.getAccountname());
        ArrayList<Company_info> allCompanys = companyService.processShowCompany(account_info1.getAccountid());
        if(allCompanys != null && !allCompanys.isEmpty()){
            session.setAttribute("company_info",allCompanys.get(0));//将公司信息存入session，在药品管理的时候使用
        }
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allCompanys));
    }

    //表单修改Company信息的方法
    @RequestMapping(value = "/updateCompany",method = RequestMethod.POST)
    public ModelAndView updateCompany(@ModelAttribute("company_info") Company_info company_info, ModelAndView modelAndView, HttpSession session){
        System.out.println(company_info.getStatus()+company_info.getComid());
        company_info.setStatus("待审核");
        boolean rs = companyService.processUpdateCompany(company_info);
        System.out.println(rs);
        modelAndView.addObject("updateCompany",rs+"");
        modelAndView.setViewName("showCompany");
        return modelAndView;
    }
    //根据companyId查询建立信息，用于更新
    @RequestMapping(value = "/showCompanyById",params = "comid")
    public void showCompanyById(HttpServletRequest request, HttpServletResponse response, Integer comid, Model model, HttpSession session){
        Company_info company_info = companyService.processGetCompanyById(comid);
        responserByajax.outOneJSONTo(request,response,responserByajax.writeToJSONObject(company_info));
    }

    //表单添加Company信息的方法
    @RequestMapping(value = "/addCompany",method = RequestMethod.POST)
    public ModelAndView addCompany(@ModelAttribute("company_info") Company_info company_info, ModelAndView modelAndView, HttpSession session){
        Account_info account_info = (Account_info) session.getAttribute("account_info");//取出登陆的账号  注册时用到accountId
        Account_info account_info1 = accountService.getAccountByName(account_info.getAccountname());
        company_info.setAccountid(account_info1.getAccountid());
        company_info.setStatus("待审核");

        boolean rs = companyService.processAddCompany(company_info);
        System.out.println(rs);
        modelAndView.addObject("addCompany",rs+"");
        modelAndView.setViewName("showCompany");
        return modelAndView;
    }
}
