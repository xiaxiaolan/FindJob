package main.com.findjob.controler;

import org.apache.commons.io.FileUtils;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;

import javax.servlet.http.HttpServletRequest;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

//@Controller
public class Fileuploadcontroler {
    @RequestMapping(value = "/upload" ,method = RequestMethod.POST)
    public String uploadfile(HttpServletRequest request,String des, MultipartFile uploadedfile){
        if(!uploadedfile.isEmpty()){
            System.out.println(des);
            //得到存文件的文件夹的绝对路径
            String destfile=request.getSession().getServletContext().getRealPath("/uploadfile");
            //根据UUID，建几层文件夹，可以均匀的分布文件，便于管理
            String childpath="";
            String uuidname=UUID.randomUUID().toString();
            for(int n=0;n<1;n++){
                childpath+=uuidname.charAt(n)+File.separator;
            }
            File f=new File(destfile,childpath);
            if (!f.exists())
            f.mkdirs();//文件夹已创好

            /** 获取文件的后缀* */
            String suffix = uploadedfile.getOriginalFilename().substring(
                    uploadedfile.getOriginalFilename().lastIndexOf("."));
            String houzhui=uploadedfile.getContentType();
            System.out.println("后缀"+houzhui);
            System.out.println(suffix);
            File file=new File(f,uuidname+suffix);
            System.out.println(file.getAbsolutePath());
            try {
                uploadedfile.transferTo(file);
            } catch (IOException e) {
                e.printStackTrace();
            }

            //根据需要，取文件相对项目的路径存到数据库中

        return "success";
        }else {
            return "error";
        }
    }

    @RequestMapping(value="/downfile")
    public ResponseEntity<byte[]> download(HttpServletRequest request,
                                           @RequestParam("filename") String filename,
                                           Model model)throws Exception {
        //下载文件路径
        String path = request.getRealPath("/uploadfile/");//getServletContext().getRealPath("/uploadfile/")
        File file = new File(path + File.separator + filename);
        HttpHeaders headers = new HttpHeaders();
        //下载显示的文件名，解决中文名称乱码问题
        String downloadFielName = new String(filename.getBytes("UTF-8"),"iso-8859-1");
        //通知浏览器以attachment（下载方式）打开图片
        headers.setContentDispositionFormData("attachment", downloadFielName);
        //application/octet-stream ： 二进制流数据（最常见的文件下载）。
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        return new ResponseEntity<byte[]>(FileUtils.readFileToByteArray(file),
                headers, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/uploads", method = RequestMethod.POST)
    // 这里的MultipartFile[] imgs表示前端页面上传过来的多个文件，imgs对应页面中多个file类型的input标签的name，但框架只会将一个文件封装进一个MultipartFile对象，
    // 并不会将多个文件封装进一个MultipartFile[]数组，直接使用会报[Lorg.springframework.web.multipart.MultipartFile;.<init>()错误，
    // 所以需要用@RequestParam校正参数（参数名与MultipartFile对象名一致），当然也可以这么写：@RequestParam("imgs") MultipartFile[] files。
    public String upload(@RequestParam MultipartFile[] uploadedfiles,HttpServletRequest request)
            throws Exception {
        for (MultipartFile uploadedfile : uploadedfiles) {
            if (uploadedfile.getSize() > 0) {
                String destfile=request.getSession().getServletContext().getRealPath("/uploadfile");
                String fileName = uploadedfile.getOriginalFilename();
                 File file = new File(destfile, fileName);
                uploadedfile.transferTo(file);
            }
        }
        return "success";
    }

}
