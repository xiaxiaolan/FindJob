package main.com.findjob.controler;


import main.com.findjob.pojoGraduation.Anti_fake;
import main.com.findjob.pojoGraduation.Company_info;
import main.com.findjob.pojoGraduation.Medicine_info;
import main.com.findjob.pojoGraduation.Qr_code;
import main.com.findjob.service.MedicineService;
import main.com.findjob.service.ResponserByajax;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;

@Controller("MedicineControl")
public class MedicineControl {
    private MedicineService medicineService;

    public void setMedicineService(MedicineService medicineService) {
        this.medicineService = medicineService;
    }

    private ResponserByajax responserByajax;
    public void setResponserByajax(ResponserByajax responserByajax) {
        this.responserByajax = responserByajax;
    }

    //显示药品信息的方法
    @RequestMapping("/showMedicine")
    public void showMedicine(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
        Company_info company_info = (Company_info) session.getAttribute("company_info");
        ArrayList<Medicine_info> allMedicines = null;
        if (company_info != null){
            allMedicines = medicineService.processShowMedicine(company_info.getComid());
        }
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allMedicines));
    }


    //根据Id查询建立信息，用于更新
    @RequestMapping(value = "/showMedicineById",params = "medicineid")
    public void showMedicineById(HttpServletRequest request, HttpServletResponse response, Integer medicineid, Model model, HttpSession session){
        Medicine_info medicine_info = medicineService.processGetMedicineById(medicineid);
        responserByajax.outOneJSONTo(request,response,responserByajax.writeToJSONObject(medicine_info));
    }

    //？多条件 前端ajax，动态 模糊查询药品信息
    //params = {"name","sex","age","phone","email","startworktime","hopedhangye","hopedworktype"}
    @RequestMapping(value = "/searchMedicineByConditions",params ={"medname","function2","pici","factoryname","status"} )
    public void searchMedicineByConditions(HttpServletRequest request, HttpServletResponse response,  HttpSession session,String medname, String function2,
                                           Integer pici, String factoryname, String status){

        Company_info company_info = (Company_info) session.getAttribute("company_info");
        ArrayList<Medicine_info> allMedicines = null;
        if (company_info != null){
            System.out.println("进入多条件search");
            if(medname==null) medname= "";
            if(function2==null) function2= "";
            if(factoryname==null) factoryname= "";
            if(pici==null) pici=-1;
            if(status==null) status= "";
            Medicine_info medicine_info = new Medicine_info(medname,factoryname,pici,status,function2,company_info.getComid());
            allMedicines = medicineService.processSearchMedicineByConditions(medicine_info);
        }
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(allMedicines));
    }


    //表单修改药品信息的方法
    @RequestMapping(value = "/updateMedicine",method = RequestMethod.POST)
    public ModelAndView updateMedicine(@ModelAttribute("medicine_info") Medicine_info medicine_info, ModelAndView modelAndView, HttpSession session){
        Company_info company_info = (Company_info) session.getAttribute("company_info");
        medicine_info.setStatus("待审核");
        boolean rs = medicineService.processUpdateMedicine(medicine_info);
        System.out.println(rs);
        modelAndView.addObject("updateMedicine",rs+"");
        modelAndView.setViewName("showMedicine");
        return modelAndView;
    }

    //表单添加Medicine信息的方法
    @RequestMapping(value = "/addMedicine",method = RequestMethod.POST)
    public ModelAndView addMedicine(@ModelAttribute("medicine_info") Medicine_info medicine_info, ModelAndView modelAndView, HttpSession session){
        Company_info company_info = (Company_info) session.getAttribute("company_info");
        ArrayList<Medicine_info> allMedicines = null;
        if (company_info != null && "通过".equals(company_info.getStatus())){
            medicine_info.setStatus("待审核");
            medicine_info.setComid(company_info.getComid());

            boolean rs = medicineService.processAddMedicine(medicine_info);
            System.out.println(rs);
            modelAndView.addObject("addMedicine",rs+"");
            modelAndView.setViewName("showMedicine");
            return modelAndView;
        }
        else {
            modelAndView.addObject("NoCompany","false");
            modelAndView.setViewName("showMedicine");
            return modelAndView;
        }
    }

    //显示方法
    @RequestMapping("/showAntiCode")
    public void showAntiCode(HttpServletRequest request, HttpServletResponse response, Model model, HttpSession session){
        ArrayList<Anti_fake> anti_fakes = medicineService.processShowAntiCode();
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(anti_fakes));
    }

    @RequestMapping("/addAntiCode")
    public ModelAndView addAntiCode(@ModelAttribute("anti_fake") Anti_fake anti_fake, ModelAndView modelAndView, HttpSession session){
        anti_fake.setFlag(0);
        boolean rs = medicineService.processAddAntiCode(anti_fake);
        modelAndView.addObject("addAntiCode",rs+"");
        modelAndView.setViewName("showAntiCode");
        return modelAndView;
    }
    @RequestMapping(value = "/selectAntiCode",params = {"antiFakeCode","medicineid"} )
    public void selectAntiCode(HttpServletRequest request, HttpServletResponse response, HttpSession session, String antiFakeCode, Integer medicineid){
        Anti_fake anti_fake = new Anti_fake();
        anti_fake.setAntiFakeCode(antiFakeCode);
        anti_fake.setMedicineid(medicineid);
        Anti_fake anti_fake1 = medicineService.processSelectAntiCode(anti_fake);
        boolean rs = false;
        if(anti_fake1 != null ){
            rs = true;
        }
        responserByajax.responseToText(request,response,""+rs);
    }

    @RequestMapping(value = "/searchAntiCode",params ={"medname","antiFakeCode"} )
    public void searchAntiCode(HttpServletRequest request, HttpServletResponse response,  HttpSession session,String medname, String antiFakeCode){
        if(medname == null) medname = "";
        if(antiFakeCode == null) antiFakeCode = "";
        Anti_fake anti_fake = new Anti_fake();
        anti_fake.setMedname(medname);
        anti_fake.setAntiFakeCode(antiFakeCode);
        ArrayList<Anti_fake> anti_fakes = medicineService.processSearchAntiCode(anti_fake);
        responserByajax.outJSONTo(request,response,responserByajax.writeToJSON(anti_fakes));
    }

}
