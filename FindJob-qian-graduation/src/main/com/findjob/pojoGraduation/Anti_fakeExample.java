package main.com.findjob.pojoGraduation;

import java.util.ArrayList;
import java.util.List;

public class Anti_fakeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public Anti_fakeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andAntiFakeIdIsNull() {
            addCriterion("anti_fake_Id is null");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdIsNotNull() {
            addCriterion("anti_fake_Id is not null");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdEqualTo(Integer value) {
            addCriterion("anti_fake_Id =", value, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdNotEqualTo(Integer value) {
            addCriterion("anti_fake_Id <>", value, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdGreaterThan(Integer value) {
            addCriterion("anti_fake_Id >", value, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("anti_fake_Id >=", value, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdLessThan(Integer value) {
            addCriterion("anti_fake_Id <", value, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdLessThanOrEqualTo(Integer value) {
            addCriterion("anti_fake_Id <=", value, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdIn(List<Integer> values) {
            addCriterion("anti_fake_Id in", values, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdNotIn(List<Integer> values) {
            addCriterion("anti_fake_Id not in", values, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdBetween(Integer value1, Integer value2) {
            addCriterion("anti_fake_Id between", value1, value2, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeIdNotBetween(Integer value1, Integer value2) {
            addCriterion("anti_fake_Id not between", value1, value2, "antiFakeId");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeIsNull() {
            addCriterion("anti_fake_code is null");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeIsNotNull() {
            addCriterion("anti_fake_code is not null");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeEqualTo(String value) {
            addCriterion("anti_fake_code =", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeNotEqualTo(String value) {
            addCriterion("anti_fake_code <>", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeGreaterThan(String value) {
            addCriterion("anti_fake_code >", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeGreaterThanOrEqualTo(String value) {
            addCriterion("anti_fake_code >=", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeLessThan(String value) {
            addCriterion("anti_fake_code <", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeLessThanOrEqualTo(String value) {
            addCriterion("anti_fake_code <=", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeLike(String value) {
            addCriterion("anti_fake_code like", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeNotLike(String value) {
            addCriterion("anti_fake_code not like", value, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeIn(List<String> values) {
            addCriterion("anti_fake_code in", values, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeNotIn(List<String> values) {
            addCriterion("anti_fake_code not in", values, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeBetween(String value1, String value2) {
            addCriterion("anti_fake_code between", value1, value2, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andAntiFakeCodeNotBetween(String value1, String value2) {
            addCriterion("anti_fake_code not between", value1, value2, "antiFakeCode");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("flag is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("flag is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(Integer value) {
            addCriterion("flag =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(Integer value) {
            addCriterion("flag <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(Integer value) {
            addCriterion("flag >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(Integer value) {
            addCriterion("flag >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(Integer value) {
            addCriterion("flag <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(Integer value) {
            addCriterion("flag <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<Integer> values) {
            addCriterion("flag in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<Integer> values) {
            addCriterion("flag not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(Integer value1, Integer value2) {
            addCriterion("flag between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(Integer value1, Integer value2) {
            addCriterion("flag not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andMedicineidIsNull() {
            addCriterion("medicineId is null");
            return (Criteria) this;
        }

        public Criteria andMedicineidIsNotNull() {
            addCriterion("medicineId is not null");
            return (Criteria) this;
        }

        public Criteria andMedicineidEqualTo(Integer value) {
            addCriterion("medicineId =", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotEqualTo(Integer value) {
            addCriterion("medicineId <>", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidGreaterThan(Integer value) {
            addCriterion("medicineId >", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidGreaterThanOrEqualTo(Integer value) {
            addCriterion("medicineId >=", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidLessThan(Integer value) {
            addCriterion("medicineId <", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidLessThanOrEqualTo(Integer value) {
            addCriterion("medicineId <=", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidIn(List<Integer> values) {
            addCriterion("medicineId in", values, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotIn(List<Integer> values) {
            addCriterion("medicineId not in", values, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidBetween(Integer value1, Integer value2) {
            addCriterion("medicineId between", value1, value2, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotBetween(Integer value1, Integer value2) {
            addCriterion("medicineId not between", value1, value2, "medicineid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}