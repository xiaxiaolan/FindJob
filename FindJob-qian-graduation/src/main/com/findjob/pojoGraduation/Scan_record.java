package main.com.findjob.pojoGraduation;
public class Scan_record {
    private Integer id;

    private String wechatAccount;

    private Integer antiFakeId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getWechatAccount() {
        return wechatAccount;
    }

    public void setWechatAccount(String wechatAccount) {
        this.wechatAccount = wechatAccount == null ? null : wechatAccount.trim();
    }

    public Integer getAntiFakeId() {
        return antiFakeId;
    }

    public void setAntiFakeId(Integer antiFakeId) {
        this.antiFakeId = antiFakeId;
    }
}