package main.com.findjob.pojoGraduation;
public class Qr_code {
    private Integer qrcodeid;

    private String codeimg;

    private Integer medicineid;

    public Integer getQrcodeid() {
        return qrcodeid;
    }

    public void setQrcodeid(Integer qrcodeid) {
        this.qrcodeid = qrcodeid;
    }

    public String getCodeimg() {
        return codeimg;
    }

    public void setCodeimg(String codeimg) {
        this.codeimg = codeimg == null ? null : codeimg.trim();
    }

    public Integer getMedicineid() {
        return medicineid;
    }

    public void setMedicineid(Integer medicineid) {
        this.medicineid = medicineid;
    }
}