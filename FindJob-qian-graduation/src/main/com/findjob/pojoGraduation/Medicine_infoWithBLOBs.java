package main.com.findjob.pojoGraduation;
public class Medicine_infoWithBLOBs extends Medicine_info {
    private String content;

    private String function1;

    private String usage1;

    private String effect;

    private String notify;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public String getFunction1() {
        return function1;
    }

    public void setFunction1(String function1) {
        this.function1 = function1 == null ? null : function1.trim();
    }

    public String getUsage1() {
        return usage1;
    }

    public void setUsage1(String usage1) {
        this.usage1 = usage1 == null ? null : usage1.trim();
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect == null ? null : effect.trim();
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify == null ? null : notify.trim();
    }
}