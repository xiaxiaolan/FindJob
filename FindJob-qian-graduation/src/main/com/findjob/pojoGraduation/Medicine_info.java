package main.com.findjob.pojoGraduation;
public class Medicine_info {
    private Integer medicineid;

    private String medname;

    private String jinji;//禁忌 怎么样不能吃

    private String medsize;

    private String factoryname;

    private Integer pici;

    private String permissioncode;

    private String expirationdate;

    private String producedate;

    private Integer comid;

    private String status;//是否审核
    private String content;//成分
    private String function1;//功能主治
    private String usage1;//使用说明
    private String effect;//不良反应
    private String notify;//注意事项

    public Medicine_info() {
    }

    public Medicine_info(String medname, String factoryname, Integer pici, String status, String function1,Integer comid) {
        this.medname = medname;
        this.factoryname = factoryname;
        this.pici = pici;
        this.status = status;
        this.function1 = function1;
        this.comid = comid;

    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getFunction1() {
        return function1;
    }

    public void setFunction1(String function1) {
        this.function1 = function1;
    }

    public String getUsage1() {
        return usage1;
    }

    public void setUsage1(String usage1) {
        this.usage1 = usage1;
    }

    public String getEffect() {
        return effect;
    }

    public void setEffect(String effect) {
        this.effect = effect;
    }

    public String getNotify() {
        return notify;
    }

    public void setNotify(String notify) {
        this.notify = notify;
    }

    public Integer getMedicineid() {
        return medicineid;
    }

    public void setMedicineid(Integer medicineid) {
        this.medicineid = medicineid;
    }

    public String getMedname() {
        return medname;
    }

    public void setMedname(String medname) {
        this.medname = medname == null ? null : medname.trim();
    }

    public String getJinji() {
        return jinji;
    }

    public void setJinji(String jinji) {
        this.jinji = jinji == null ? null : jinji.trim();
    }

    public String getMedsize() {
        return medsize;
    }

    public void setMedsize(String medsize) {
        this.medsize = medsize == null ? null : medsize.trim();
    }

    public String getFactoryname() {
        return factoryname;
    }

    public void setFactoryname(String factoryname) {
        this.factoryname = factoryname == null ? null : factoryname.trim();
    }

    public Integer getPici() {
        return pici;
    }

    public void setPici(Integer pici) {
        this.pici = pici;
    }

    public String getPermissioncode() {
        return permissioncode;
    }

    public void setPermissioncode(String permissioncode) {
        this.permissioncode = permissioncode == null ? null : permissioncode.trim();
    }

    public String getExpirationdate() {
        return expirationdate;
    }

    public void setExpirationdate(String expirationdate) {
        this.expirationdate = expirationdate == null ? null : expirationdate.trim();
    }

    public String getProducedate() {
        return producedate;
    }

    public void setProducedate(String producedate) {
        this.producedate = producedate == null ? null : producedate.trim();
    }

    public Integer getComid() {
        return comid;
    }

    public void setComid(Integer comid) {
        this.comid = comid;
    }
}