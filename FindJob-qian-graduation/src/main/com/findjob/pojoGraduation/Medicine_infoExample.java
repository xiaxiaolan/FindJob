package main.com.findjob.pojoGraduation;
import java.util.ArrayList;
import java.util.List;

public class Medicine_infoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public Medicine_infoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andMedicineidIsNull() {
            addCriterion("medicineId is null");
            return (Criteria) this;
        }

        public Criteria andMedicineidIsNotNull() {
            addCriterion("medicineId is not null");
            return (Criteria) this;
        }

        public Criteria andMedicineidEqualTo(Integer value) {
            addCriterion("medicineId =", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotEqualTo(Integer value) {
            addCriterion("medicineId <>", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidGreaterThan(Integer value) {
            addCriterion("medicineId >", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidGreaterThanOrEqualTo(Integer value) {
            addCriterion("medicineId >=", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidLessThan(Integer value) {
            addCriterion("medicineId <", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidLessThanOrEqualTo(Integer value) {
            addCriterion("medicineId <=", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidIn(List<Integer> values) {
            addCriterion("medicineId in", values, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotIn(List<Integer> values) {
            addCriterion("medicineId not in", values, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidBetween(Integer value1, Integer value2) {
            addCriterion("medicineId between", value1, value2, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotBetween(Integer value1, Integer value2) {
            addCriterion("medicineId not between", value1, value2, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMednameIsNull() {
            addCriterion("medName is null");
            return (Criteria) this;
        }

        public Criteria andMednameIsNotNull() {
            addCriterion("medName is not null");
            return (Criteria) this;
        }

        public Criteria andMednameEqualTo(String value) {
            addCriterion("medName =", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameNotEqualTo(String value) {
            addCriterion("medName <>", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameGreaterThan(String value) {
            addCriterion("medName >", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameGreaterThanOrEqualTo(String value) {
            addCriterion("medName >=", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameLessThan(String value) {
            addCriterion("medName <", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameLessThanOrEqualTo(String value) {
            addCriterion("medName <=", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameLike(String value) {
            addCriterion("medName like", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameNotLike(String value) {
            addCriterion("medName not like", value, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameIn(List<String> values) {
            addCriterion("medName in", values, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameNotIn(List<String> values) {
            addCriterion("medName not in", values, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameBetween(String value1, String value2) {
            addCriterion("medName between", value1, value2, "medname");
            return (Criteria) this;
        }

        public Criteria andMednameNotBetween(String value1, String value2) {
            addCriterion("medName not between", value1, value2, "medname");
            return (Criteria) this;
        }

        public Criteria andJinjiIsNull() {
            addCriterion("jinji is null");
            return (Criteria) this;
        }

        public Criteria andJinjiIsNotNull() {
            addCriterion("jinji is not null");
            return (Criteria) this;
        }

        public Criteria andJinjiEqualTo(String value) {
            addCriterion("jinji =", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiNotEqualTo(String value) {
            addCriterion("jinji <>", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiGreaterThan(String value) {
            addCriterion("jinji >", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiGreaterThanOrEqualTo(String value) {
            addCriterion("jinji >=", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiLessThan(String value) {
            addCriterion("jinji <", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiLessThanOrEqualTo(String value) {
            addCriterion("jinji <=", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiLike(String value) {
            addCriterion("jinji like", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiNotLike(String value) {
            addCriterion("jinji not like", value, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiIn(List<String> values) {
            addCriterion("jinji in", values, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiNotIn(List<String> values) {
            addCriterion("jinji not in", values, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiBetween(String value1, String value2) {
            addCriterion("jinji between", value1, value2, "jinji");
            return (Criteria) this;
        }

        public Criteria andJinjiNotBetween(String value1, String value2) {
            addCriterion("jinji not between", value1, value2, "jinji");
            return (Criteria) this;
        }

        public Criteria andMedsizeIsNull() {
            addCriterion("medSize is null");
            return (Criteria) this;
        }

        public Criteria andMedsizeIsNotNull() {
            addCriterion("medSize is not null");
            return (Criteria) this;
        }

        public Criteria andMedsizeEqualTo(String value) {
            addCriterion("medSize =", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeNotEqualTo(String value) {
            addCriterion("medSize <>", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeGreaterThan(String value) {
            addCriterion("medSize >", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeGreaterThanOrEqualTo(String value) {
            addCriterion("medSize >=", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeLessThan(String value) {
            addCriterion("medSize <", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeLessThanOrEqualTo(String value) {
            addCriterion("medSize <=", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeLike(String value) {
            addCriterion("medSize like", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeNotLike(String value) {
            addCriterion("medSize not like", value, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeIn(List<String> values) {
            addCriterion("medSize in", values, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeNotIn(List<String> values) {
            addCriterion("medSize not in", values, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeBetween(String value1, String value2) {
            addCriterion("medSize between", value1, value2, "medsize");
            return (Criteria) this;
        }

        public Criteria andMedsizeNotBetween(String value1, String value2) {
            addCriterion("medSize not between", value1, value2, "medsize");
            return (Criteria) this;
        }

        public Criteria andFactorynameIsNull() {
            addCriterion("factoryName is null");
            return (Criteria) this;
        }

        public Criteria andFactorynameIsNotNull() {
            addCriterion("factoryName is not null");
            return (Criteria) this;
        }

        public Criteria andFactorynameEqualTo(String value) {
            addCriterion("factoryName =", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameNotEqualTo(String value) {
            addCriterion("factoryName <>", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameGreaterThan(String value) {
            addCriterion("factoryName >", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameGreaterThanOrEqualTo(String value) {
            addCriterion("factoryName >=", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameLessThan(String value) {
            addCriterion("factoryName <", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameLessThanOrEqualTo(String value) {
            addCriterion("factoryName <=", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameLike(String value) {
            addCriterion("factoryName like", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameNotLike(String value) {
            addCriterion("factoryName not like", value, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameIn(List<String> values) {
            addCriterion("factoryName in", values, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameNotIn(List<String> values) {
            addCriterion("factoryName not in", values, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameBetween(String value1, String value2) {
            addCriterion("factoryName between", value1, value2, "factoryname");
            return (Criteria) this;
        }

        public Criteria andFactorynameNotBetween(String value1, String value2) {
            addCriterion("factoryName not between", value1, value2, "factoryname");
            return (Criteria) this;
        }

        public Criteria andPiciIsNull() {
            addCriterion("pici is null");
            return (Criteria) this;
        }

        public Criteria andPiciIsNotNull() {
            addCriterion("pici is not null");
            return (Criteria) this;
        }

        public Criteria andPiciEqualTo(Integer value) {
            addCriterion("pici =", value, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciNotEqualTo(Integer value) {
            addCriterion("pici <>", value, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciGreaterThan(Integer value) {
            addCriterion("pici >", value, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciGreaterThanOrEqualTo(Integer value) {
            addCriterion("pici >=", value, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciLessThan(Integer value) {
            addCriterion("pici <", value, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciLessThanOrEqualTo(Integer value) {
            addCriterion("pici <=", value, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciIn(List<Integer> values) {
            addCriterion("pici in", values, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciNotIn(List<Integer> values) {
            addCriterion("pici not in", values, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciBetween(Integer value1, Integer value2) {
            addCriterion("pici between", value1, value2, "pici");
            return (Criteria) this;
        }

        public Criteria andPiciNotBetween(Integer value1, Integer value2) {
            addCriterion("pici not between", value1, value2, "pici");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeIsNull() {
            addCriterion("permissionCode is null");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeIsNotNull() {
            addCriterion("permissionCode is not null");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeEqualTo(String value) {
            addCriterion("permissionCode =", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeNotEqualTo(String value) {
            addCriterion("permissionCode <>", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeGreaterThan(String value) {
            addCriterion("permissionCode >", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeGreaterThanOrEqualTo(String value) {
            addCriterion("permissionCode >=", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeLessThan(String value) {
            addCriterion("permissionCode <", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeLessThanOrEqualTo(String value) {
            addCriterion("permissionCode <=", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeLike(String value) {
            addCriterion("permissionCode like", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeNotLike(String value) {
            addCriterion("permissionCode not like", value, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeIn(List<String> values) {
            addCriterion("permissionCode in", values, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeNotIn(List<String> values) {
            addCriterion("permissionCode not in", values, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeBetween(String value1, String value2) {
            addCriterion("permissionCode between", value1, value2, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andPermissioncodeNotBetween(String value1, String value2) {
            addCriterion("permissionCode not between", value1, value2, "permissioncode");
            return (Criteria) this;
        }

        public Criteria andExpirationdateIsNull() {
            addCriterion("expirationDate is null");
            return (Criteria) this;
        }

        public Criteria andExpirationdateIsNotNull() {
            addCriterion("expirationDate is not null");
            return (Criteria) this;
        }

        public Criteria andExpirationdateEqualTo(String value) {
            addCriterion("expirationDate =", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateNotEqualTo(String value) {
            addCriterion("expirationDate <>", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateGreaterThan(String value) {
            addCriterion("expirationDate >", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateGreaterThanOrEqualTo(String value) {
            addCriterion("expirationDate >=", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateLessThan(String value) {
            addCriterion("expirationDate <", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateLessThanOrEqualTo(String value) {
            addCriterion("expirationDate <=", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateLike(String value) {
            addCriterion("expirationDate like", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateNotLike(String value) {
            addCriterion("expirationDate not like", value, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateIn(List<String> values) {
            addCriterion("expirationDate in", values, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateNotIn(List<String> values) {
            addCriterion("expirationDate not in", values, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateBetween(String value1, String value2) {
            addCriterion("expirationDate between", value1, value2, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andExpirationdateNotBetween(String value1, String value2) {
            addCriterion("expirationDate not between", value1, value2, "expirationdate");
            return (Criteria) this;
        }

        public Criteria andProducedateIsNull() {
            addCriterion("produceDate is null");
            return (Criteria) this;
        }

        public Criteria andProducedateIsNotNull() {
            addCriterion("produceDate is not null");
            return (Criteria) this;
        }

        public Criteria andProducedateEqualTo(String value) {
            addCriterion("produceDate =", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateNotEqualTo(String value) {
            addCriterion("produceDate <>", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateGreaterThan(String value) {
            addCriterion("produceDate >", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateGreaterThanOrEqualTo(String value) {
            addCriterion("produceDate >=", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateLessThan(String value) {
            addCriterion("produceDate <", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateLessThanOrEqualTo(String value) {
            addCriterion("produceDate <=", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateLike(String value) {
            addCriterion("produceDate like", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateNotLike(String value) {
            addCriterion("produceDate not like", value, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateIn(List<String> values) {
            addCriterion("produceDate in", values, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateNotIn(List<String> values) {
            addCriterion("produceDate not in", values, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateBetween(String value1, String value2) {
            addCriterion("produceDate between", value1, value2, "producedate");
            return (Criteria) this;
        }

        public Criteria andProducedateNotBetween(String value1, String value2) {
            addCriterion("produceDate not between", value1, value2, "producedate");
            return (Criteria) this;
        }

        public Criteria andComidIsNull() {
            addCriterion("comId is null");
            return (Criteria) this;
        }

        public Criteria andComidIsNotNull() {
            addCriterion("comId is not null");
            return (Criteria) this;
        }

        public Criteria andComidEqualTo(Integer value) {
            addCriterion("comId =", value, "comid");
            return (Criteria) this;
        }

        public Criteria andComidNotEqualTo(Integer value) {
            addCriterion("comId <>", value, "comid");
            return (Criteria) this;
        }

        public Criteria andComidGreaterThan(Integer value) {
            addCriterion("comId >", value, "comid");
            return (Criteria) this;
        }

        public Criteria andComidGreaterThanOrEqualTo(Integer value) {
            addCriterion("comId >=", value, "comid");
            return (Criteria) this;
        }

        public Criteria andComidLessThan(Integer value) {
            addCriterion("comId <", value, "comid");
            return (Criteria) this;
        }

        public Criteria andComidLessThanOrEqualTo(Integer value) {
            addCriterion("comId <=", value, "comid");
            return (Criteria) this;
        }

        public Criteria andComidIn(List<Integer> values) {
            addCriterion("comId in", values, "comid");
            return (Criteria) this;
        }

        public Criteria andComidNotIn(List<Integer> values) {
            addCriterion("comId not in", values, "comid");
            return (Criteria) this;
        }

        public Criteria andComidBetween(Integer value1, Integer value2) {
            addCriterion("comId between", value1, value2, "comid");
            return (Criteria) this;
        }

        public Criteria andComidNotBetween(Integer value1, Integer value2) {
            addCriterion("comId not between", value1, value2, "comid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}