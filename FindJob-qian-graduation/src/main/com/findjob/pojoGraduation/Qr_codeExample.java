package main.com.findjob.pojoGraduation;
import java.util.ArrayList;
import java.util.List;

public class Qr_codeExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public Qr_codeExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andQrcodeidIsNull() {
            addCriterion("qrCodeId is null");
            return (Criteria) this;
        }

        public Criteria andQrcodeidIsNotNull() {
            addCriterion("qrCodeId is not null");
            return (Criteria) this;
        }

        public Criteria andQrcodeidEqualTo(Integer value) {
            addCriterion("qrCodeId =", value, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidNotEqualTo(Integer value) {
            addCriterion("qrCodeId <>", value, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidGreaterThan(Integer value) {
            addCriterion("qrCodeId >", value, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidGreaterThanOrEqualTo(Integer value) {
            addCriterion("qrCodeId >=", value, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidLessThan(Integer value) {
            addCriterion("qrCodeId <", value, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidLessThanOrEqualTo(Integer value) {
            addCriterion("qrCodeId <=", value, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidIn(List<Integer> values) {
            addCriterion("qrCodeId in", values, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidNotIn(List<Integer> values) {
            addCriterion("qrCodeId not in", values, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidBetween(Integer value1, Integer value2) {
            addCriterion("qrCodeId between", value1, value2, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andQrcodeidNotBetween(Integer value1, Integer value2) {
            addCriterion("qrCodeId not between", value1, value2, "qrcodeid");
            return (Criteria) this;
        }

        public Criteria andCodeimgIsNull() {
            addCriterion("codeImg is null");
            return (Criteria) this;
        }

        public Criteria andCodeimgIsNotNull() {
            addCriterion("codeImg is not null");
            return (Criteria) this;
        }

        public Criteria andCodeimgEqualTo(String value) {
            addCriterion("codeImg =", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgNotEqualTo(String value) {
            addCriterion("codeImg <>", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgGreaterThan(String value) {
            addCriterion("codeImg >", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgGreaterThanOrEqualTo(String value) {
            addCriterion("codeImg >=", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgLessThan(String value) {
            addCriterion("codeImg <", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgLessThanOrEqualTo(String value) {
            addCriterion("codeImg <=", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgLike(String value) {
            addCriterion("codeImg like", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgNotLike(String value) {
            addCriterion("codeImg not like", value, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgIn(List<String> values) {
            addCriterion("codeImg in", values, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgNotIn(List<String> values) {
            addCriterion("codeImg not in", values, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgBetween(String value1, String value2) {
            addCriterion("codeImg between", value1, value2, "codeimg");
            return (Criteria) this;
        }

        public Criteria andCodeimgNotBetween(String value1, String value2) {
            addCriterion("codeImg not between", value1, value2, "codeimg");
            return (Criteria) this;
        }

        public Criteria andMedicineidIsNull() {
            addCriterion("medicineId is null");
            return (Criteria) this;
        }

        public Criteria andMedicineidIsNotNull() {
            addCriterion("medicineId is not null");
            return (Criteria) this;
        }

        public Criteria andMedicineidEqualTo(Integer value) {
            addCriterion("medicineId =", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotEqualTo(Integer value) {
            addCriterion("medicineId <>", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidGreaterThan(Integer value) {
            addCriterion("medicineId >", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidGreaterThanOrEqualTo(Integer value) {
            addCriterion("medicineId >=", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidLessThan(Integer value) {
            addCriterion("medicineId <", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidLessThanOrEqualTo(Integer value) {
            addCriterion("medicineId <=", value, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidIn(List<Integer> values) {
            addCriterion("medicineId in", values, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotIn(List<Integer> values) {
            addCriterion("medicineId not in", values, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidBetween(Integer value1, Integer value2) {
            addCriterion("medicineId between", value1, value2, "medicineid");
            return (Criteria) this;
        }

        public Criteria andMedicineidNotBetween(Integer value1, Integer value2) {
            addCriterion("medicineId not between", value1, value2, "medicineid");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}