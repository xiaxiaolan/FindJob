package main.com.findjob.pojoGraduation;
public class Anti_fake {
    private Integer antiFakeId;

    private String antiFakeCode;

    private Integer flag;

    private Integer medicineid;


    public Integer getAntiFakeId() {
        return antiFakeId;
    }

    public void setAntiFakeId(Integer antiFakeId) {
        this.antiFakeId = antiFakeId;
    }

    public String getAntiFakeCode() {
        return antiFakeCode;
    }

    public void setAntiFakeCode(String antiFakeCode) {
        this.antiFakeCode = antiFakeCode == null ? null : antiFakeCode.trim();
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }

    public Integer getMedicineid() {
        return medicineid;
    }

    public void setMedicineid(Integer medicineid) {
        this.medicineid = medicineid;
    }

    private String medname;

    public String getMedname() {
        return medname;
    }

    public void setMedname(String medname) {
        this.medname = medname;
    }
}