package main.com.findjob.pojoGraduation;

public class Shirobean {
    private Integer id;
    private Integer roleid;
    private Integer oldroleid;
    private String username;
    private String password;
    private String rolename;
    private String miaoshu;


    public Shirobean() {
    }

    public Shirobean(Integer id, Integer roleid,Integer oldroleid) {
        this.id = id;
        this.roleid = roleid;
        this.oldroleid=oldroleid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRoleid() {
        return roleid;
    }

    public void setRoleid(Integer roleid) {
        this.roleid = roleid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRolename() {
        return rolename;
    }

    public void setRolename(String rolename) {
        this.rolename = rolename;
    }

    public String getMiaoshu() {
        return miaoshu;
    }

    public void setMiaoshu(String miaoshu) {
        this.miaoshu = miaoshu;
    }

    public Integer getOldroleid() {
        return oldroleid;
    }

    public void setOldroleid(Integer oldroleid) {
        this.oldroleid = oldroleid;
    }

    @Override
    public String toString() {
        return "Shirobean{" +
                "id=" + id +
                ", roleid=" + roleid +
                ", oldroleid=" + oldroleid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", rolename='" + rolename + '\'' +
                ", miaoshu='" + miaoshu + '\'' +
                '}';
    }
}
