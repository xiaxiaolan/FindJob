package main.com.findjob.service;

import main.com.findjob.daoGraduation.Account_infoMapper;
import main.com.findjob.daoGraduation.BaseDAO;
import main.com.findjob.pojoGraduation.Account_info;

import java.util.ArrayList;

public interface AccountService{
    //注册用户
    public int addAccount(Account_info account_info);

    //通过用户名accountName查询账号是否存在
    public Account_info getAccountByName(String accountName);

    //显示所有的account
    public ArrayList<Account_info> processShowAccount(String accountName);
    public boolean processUpdateAccount(Account_info account_info);
    public Account_info processGetAccountById(Integer accountid);

}
