package main.com.findjob.service;

import main.com.findjob.pojoGraduation.Anti_fake;
import main.com.findjob.pojoGraduation.Medicine_info;
import main.com.findjob.pojoGraduation.Qr_code;

import java.util.ArrayList;

public interface MedicineService {
    public ArrayList<Medicine_info> processShowMedicine(Integer comid);
    public Medicine_info processGetMedicineById(Integer medicineid);
    public ArrayList<Medicine_info> processSearchMedicineByConditions(Medicine_info medicine_info);
    public boolean processUpdateMedicine(Medicine_info medicine_info);
    public boolean processAddMedicine(Medicine_info medicine_info);

    public ArrayList<Anti_fake> processShowAntiCode ();
    public boolean  processAddAntiCode(Anti_fake anti_fake);
    public ArrayList<Anti_fake> processSearchAntiCode(Anti_fake anti_fake);
    Anti_fake  processSelectAntiCode(Anti_fake anti_fake);
}
