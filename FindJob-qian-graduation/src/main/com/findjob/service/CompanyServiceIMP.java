package main.com.findjob.service;

import main.com.findjob.daoGraduation.BaseDAO;
import main.com.findjob.daoGraduation.Company_infoMapper;
import main.com.findjob.pojoGraduation.Company_info;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component("companyService")
public class CompanyServiceIMP extends BaseDAO implements CompanyService {
    Company_infoMapper company_infoMapper;
    public void setCompany_infoMapper(Company_infoMapper company_infoMapper) {
        this.company_infoMapper = company_infoMapper;
    }


    public ArrayList<Company_info> processSearchCompanyByConditions(Company_info company_info) {
        company_infoMapper = getSqlSession().getMapper(Company_infoMapper.class);
        try {
            ArrayList<Company_info> company_infos = company_infoMapper.getCompanysByConditions(company_info);
            return company_infos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Company_info> processShowCompany(Integer accountid) {
        company_infoMapper = getSqlSession().getMapper(Company_infoMapper.class);
        try {
            ArrayList<Company_info> company_infos = company_infoMapper.getAllCompanys(accountid);
            return company_infos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Company_info processGetCompanyById(Integer comid) {
        company_infoMapper = getSqlSession().getMapper(Company_infoMapper.class);
        try {
            Company_info company_info = company_infoMapper.selectByPrimaryKey(comid);
            return company_info;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean processUpdateCompany(Company_info company_info) {
        try {
            boolean rs = company_infoMapper.updateAllByPrimaryKey(company_info);
            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean processAddCompany(Company_info company_info) {
        try {
            int rs = company_infoMapper.insertSelective(company_info);
            return rs==0?false:true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
