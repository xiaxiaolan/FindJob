package main.com.findjob.service;

import main.com.findjob.daoGraduation.UserMapper;
import main.com.findjob.pojoGraduation.Shirobean;
import main.com.findjob.pojoGraduation.User;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/*  @RequiresAuthentication()
    @RequiresGuest
    @RequiresPermissions(value = "")
    @RequiresUser
    @RequiresRoles({"role2"})
*/
@Component("userService")
public class UserServiceIMP implements UserService {
    private UserMapper userDAO;
    public void setUserDAO(UserMapper userDAO) {
        this.userDAO = userDAO;
    }
//Admin
//注册
    public boolean processRegister(User user) {
        String hashAlgorithmName = "MD5";
        int hashIterations = 1024;
        Object credentials = user.getPassword();
        Object salt = ByteSource.Util.bytes(user.getUsername());
        //注册时可调用该方法获取加密后的密码
        Object newpass = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);
        user.setPassword(newpass.toString());

        try {
            return userDAO.addUser(user);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
//根据username查用户信息
    public User getUserByUsername(String username) {
        try {
            return userDAO.getUserByUsername(username);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
//根据username模糊查用户信息
    public ArrayList<User> getUserByUsernam(String username){
        try {
            return userDAO.getUserByUsernam(username);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
//查询出所有的user
    public ArrayList<User> processShowAllUsers() {
        try {
            return userDAO.getAllUsers();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
//处理更新用户的方法
    public boolean processUpdateUser(User user) {
        String hashAlgorithmName = "MD5";
        int hashIterations = 1024;
        Object credentials = user.getPassword();
        System.out.println(user.toString());
        Object salt = ByteSource.Util.bytes(user.getUsername());
        //注册时可调用该方法获取加密后的密码
        Object newpass = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);

        user.setPassword(newpass.toString());
        try {
            return userDAO.updateUser(user);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
//处理删除用户的方法
    public boolean processDeleteUser(int[] ids) {
        try {
            return userDAO.deleteUser(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    //获取用户
    public User processGetAdminById(Integer id){
        try {
            return userDAO.getAdminById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //getRoleByUsername 获取某个用户所拥有的权限
    public ArrayList<String> processGetRoleByUsername(String username){
        try {
            return userDAO.getRoleByUsername(username);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    //获取所有用户的权限信息
    public ArrayList<Shirobean> processGetAllRole(){
        try {
            return userDAO.getAllRole();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    //获取某个用户所拥有的权限 shirobean
    public ArrayList<Shirobean> processGetShiroByName(String username){
        try {
            return userDAO.getShiroByUsername(username);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    //给某个用户添加权限
    public boolean processAddShiro(String username,Integer roleid){
        Integer id=null;
        try {
           id = userDAO.getIdByUsername(username);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        if (id!=null){
            Shirobean shirobean=new Shirobean(id,roleid,null);
            try {
                return userDAO.addShiro(shirobean);
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            }
        }
        else {
            return false;
        }
    }

    //给某个update权限
    public boolean processUpdateShiro(Shirobean shirobean){
        try {
            return userDAO.updateShiro(shirobean);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //删除权限
    public boolean processDeleteShiro(Integer id[],Integer roleid[]){
        Shirobean shirobean=new Shirobean();
        try {
            for(int i=0;i<id.length;i++){
                shirobean.setId(id[i]);
                shirobean.setRoleid(roleid[i]);
                userDAO.deleteShiro(shirobean);
            }
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//Account 的方法
/*//处理显示Account方法
    public ArrayList<Account> processShowAccount(){
        try {
            ArrayList<Account> accounts=userDAO.getAllAccounts();
            return accounts;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }*/

    /*//处理修改Account的方法
    public boolean processUpdateAccount(Account account) {
        try {
            return userDAO.updateAccount(account);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    // 处理添加account的方法
    public boolean processAddAccount(Account account) {
        try {
            return userDAO.addAccount(account);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    //处理按照条件phone模糊查询Acount的方法
    public ArrayList<Account> processSearchAccount(String phone) {
        try {
            return userDAO.getAccountsByPhone(phone);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    //处理按照条件phone模糊查询Acount的方法
    public boolean processSearchAAccount(String phone) {
        try {
            if (userDAO.getAccountByPhone(phone)!=null){
                return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }

    //查询账号信息byId
    public  Account processGetAccountById(Integer id){
        try {
            return userDAO.getAccountById(id);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    */

     public boolean processGetAdminByUsername(String username) {
        try {
            if (userDAO.getAccountByUsername(username) != null) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
    }
    //处理批量删除Account的方法
    public boolean processDeleteAccount(int[] ids) {
        try {
            return userDAO.deleteAccount(ids);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
