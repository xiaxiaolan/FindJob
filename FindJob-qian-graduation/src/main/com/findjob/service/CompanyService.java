package main.com.findjob.service;

import main.com.findjob.pojoGraduation.Company_info;

import java.util.ArrayList;

public interface CompanyService {
    public ArrayList<Company_info> processSearchCompanyByConditions(Company_info company_info);
    public ArrayList<Company_info> processShowCompany(Integer accountid);
    public Company_info processGetCompanyById(Integer comid);
    public boolean processUpdateCompany(Company_info company_info);
    public boolean processAddCompany(Company_info company_info);



}
