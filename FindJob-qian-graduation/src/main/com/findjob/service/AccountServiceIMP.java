package main.com.findjob.service;

import main.com.findjob.daoGraduation.Account_infoMapper;
import main.com.findjob.daoGraduation.BaseDAO;
import main.com.findjob.pojoGraduation.Account_info;
import org.apache.ibatis.javassist.compiler.AccessorMaker;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component("accountService")
public class AccountServiceIMP extends BaseDAO implements AccountService {
    private Account_infoMapper account_infoMapper;

    public void setAccount_infoMapper(Account_infoMapper account_infoMapper) {
        this.account_infoMapper = account_infoMapper;
    }

    //注册用户
    public int addAccount(Account_info account_info){
        account_infoMapper=getSqlSession().getMapper(Account_infoMapper.class);
        String hashAlgorithmName = "MD5";
        int hashIterations = 1024;
        Object credentials = account_info.getPassword();
        Object salt = ByteSource.Util.bytes(account_info.getAccountname());
        //注册时可调用该方法获取加密后的密码
        Object newpass = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);
        account_info.setPassword(newpass.toString());
        try {
            return account_infoMapper.registerAccount(account_info);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    //通过用户名accountName查询账号是否存在
    public Account_info getAccountByName(String accountName){
        account_infoMapper=getSqlSession().getMapper(Account_infoMapper.class);
        return account_infoMapper.selectAccountByName(accountName);
    }
    //处理显示Account方法
    public ArrayList<Account_info> processShowAccount(String accountName) {
        account_infoMapper=getSqlSession().getMapper(Account_infoMapper.class);
        try {
            ArrayList<Account_info> accounts=account_infoMapper.getAllAccounts(accountName);
            return accounts;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean processUpdateAccount(Account_info account_info) {
        account_infoMapper=getSqlSession().getMapper(Account_infoMapper.class);
        String hashAlgorithmName = "MD5";
        int hashIterations = 1024;
        Object credentials = account_info.getPassword();
        Object salt = ByteSource.Util.bytes(account_info.getAccountname());
        //注册时可调用该方法获取加密后的密码
        Object newpass = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);
        account_info.setPassword(newpass.toString());
        try {
            boolean rs = account_infoMapper.updateAccount(account_info);
            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public Account_info processGetAccountById(Integer accountid) {
        account_infoMapper=getSqlSession().getMapper(Account_infoMapper.class);
        try {
            Account_info account_info = account_infoMapper.selectByPrimaryKey(accountid);
            return account_info;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
