package main.com.findjob.service;

import main.com.findjob.pojoGraduation.Shirobean;
import main.com.findjob.pojoGraduation.User;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;

public interface UserService {
    //添加用户
    @Transactional
    public boolean processRegister(User user);
    //根据username查用户信息
    @Transactional
    public User getUserByUsername(String username);
    //根据username模糊查用户信息
    @Transactional
    public ArrayList<User> getUserByUsernam(String username);
    //查all用户信息
    @Transactional
    public ArrayList<User> processShowAllUsers();
    //修改用户
    @Transactional
    public boolean processUpdateUser(User user);
    //删除用户
    @Transactional
    public boolean processDeleteUser(int[] ids);
    //获取用户
    @Transactional
    public User processGetAdminById(Integer id);
    //获取用户
    @Transactional
    public boolean processGetAdminByUsername(String username);



    //getRoleByUsername 获取某个用户所拥有的权限role
    @Transactional
    public ArrayList<String> processGetRoleByUsername(String username);
    //获取所有的shirobean
    @Transactional
    public ArrayList<Shirobean> processGetAllRole();
    //获取某个用户所拥有的权限 shirobean
    @Transactional
    public ArrayList<Shirobean> processGetShiroByName(String username);
    //给某个用户添加权限
    @Transactional
    public boolean processAddShiro(String username, Integer roleid);
    //给某个用户添加权限
    @Transactional
    public boolean processUpdateShiro(Shirobean shirobean);
    //给某个用户添加权限
    @Transactional
    public boolean processDeleteShiro(Integer id[], Integer roleid[]);

//处理批量删除Account的方法
    @Transactional
    public boolean processDeleteAccount(int[] ids);

}
