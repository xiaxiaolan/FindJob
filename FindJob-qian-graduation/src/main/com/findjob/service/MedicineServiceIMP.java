package main.com.findjob.service;

import main.com.findjob.daoGraduation.Anti_fakeMapper;
import main.com.findjob.daoGraduation.BaseDAO;
import main.com.findjob.daoGraduation.Medicine_infoMapper;
import main.com.findjob.daoGraduation.Qr_codeMapper;
import main.com.findjob.pojoGraduation.Anti_fake;
import main.com.findjob.pojoGraduation.Medicine_info;
import main.com.findjob.pojoGraduation.Qr_code;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component("medicineService")
public class MedicineServiceIMP extends BaseDAO implements MedicineService {

    Medicine_infoMapper medicine_infoMapper;
    Anti_fakeMapper anti_fakeMapper;
    public void setAnti_fakeMapper(Anti_fakeMapper anti_fakeMapper) {
        this.anti_fakeMapper = anti_fakeMapper;
    }

    public void setMedicine_infoMapper(Medicine_infoMapper medicine_infoMapper) {
        this.medicine_infoMapper = medicine_infoMapper;
    }

    public ArrayList<Medicine_info> processShowMedicine(Integer comid) {
        medicine_infoMapper = getSqlSession().getMapper(Medicine_infoMapper.class);
        try {
            ArrayList<Medicine_info> medicine_infos = medicine_infoMapper.getAllMedicines(comid);
            return medicine_infos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Medicine_info processGetMedicineById(Integer medicineid) {
        medicine_infoMapper = getSqlSession().getMapper(Medicine_infoMapper.class);
        try {
            Medicine_info medicine_info = medicine_infoMapper.selectByPrimaryKey(medicineid);
            return medicine_info;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<Medicine_info> processSearchMedicineByConditions(Medicine_info medicine_info) {
        medicine_infoMapper = getSqlSession().getMapper(Medicine_infoMapper.class);
        try {
            ArrayList<Medicine_info> medicine_infos = medicine_infoMapper.getMedicinesByConditions(medicine_info);
            return medicine_infos;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean processUpdateMedicine(Medicine_info medicine_info) {
        medicine_infoMapper = getSqlSession().getMapper(Medicine_infoMapper.class);
        try {
            int rs = medicine_infoMapper.updateByPrimaryKeySelective(medicine_info);
            return rs==0?false:true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean processAddMedicine(Medicine_info medicine_info) {
        medicine_infoMapper = getSqlSession().getMapper(Medicine_infoMapper.class);
        try {
            int rs = medicine_infoMapper.insertSelective(medicine_info);
            return rs==0?false:true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList<Anti_fake> processShowAntiCode() {
        anti_fakeMapper = getSqlSession().getMapper(Anti_fakeMapper.class);
        try {
            ArrayList<Anti_fake> anti_fakes = anti_fakeMapper.getAntiCodes();
            return anti_fakes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean processAddAntiCode(Anti_fake anti_fake) {
        anti_fakeMapper = getSqlSession().getMapper(Anti_fakeMapper.class);
        try {
           int rs = anti_fakeMapper.insert(anti_fake);
           return rs==0?false:true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ArrayList<Anti_fake> processSearchAntiCode(Anti_fake anti_fake) {
        anti_fakeMapper = getSqlSession().getMapper(Anti_fakeMapper.class);
        try {
            ArrayList<Anti_fake> anti_fakes = anti_fakeMapper.searchAntiCode(anti_fake);
            return anti_fakes;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Anti_fake processSelectAntiCode(Anti_fake anti_fake) {
        anti_fakeMapper = getSqlSession().getMapper(Anti_fakeMapper.class);
        try {
            Anti_fake anti_fake1 = anti_fakeMapper.selectAntiCodeByInsert(anti_fake);
            return anti_fake1;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
