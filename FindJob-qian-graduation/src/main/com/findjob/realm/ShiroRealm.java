package main.com.findjob.realm;

import main.com.findjob.daoGraduation.UserMapperImp;
import main.com.findjob.pojoGraduation.Account_info;
import main.com.findjob.pojoGraduation.User;
import main.com.findjob.service.AccountService;
import main.com.findjob.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Controller("ShiroRealm")
public class ShiroRealm extends AuthorizingRealm {
    @Resource(name = "accountService")
    AccountService accountService;

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken token) throws AuthenticationException {
        UserMapperImp userDAOImp=new UserMapperImp();
        System.out.println("[firstRealm]登录认证过程");
        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        String accountName = upToken.getUsername();

        System.out.println(accountName);

        //以下信息是从数据库中获取的.
        Account_info account_info= accountService.getAccountByName(accountName);

        System.out.println(account_info.toString());

        Object principal = accountName;
        Object credentials =account_info.getPassword();// "fc1709d0a95a6be30bc5926fdb7f22f4";123456

        //3). realmName: 当前 realm 对象的 name. 调用父类的 getName() 方法即可
        String realmName = getName();
        //4). 盐值.即使原始密码相同，存入数据库时也是不同的
        ByteSource credentialsSalt = ByteSource.Util.bytes(accountName);
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(principal, credentials,credentialsSalt, realmName);
        return info;
    }

      //授权会被 shiro 回调的方法
      //只有当点击某个需要授权的页面，才会进入授权方法进行判断有没有权限
    /*@Override*/
   /* protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principals) {
        System.out.println("【授权】授权角色过程");
        //1. 从 PrincipalCollection 中来获取登录用户的信息
        Object principal = principals.getPrimaryPrincipal();
        Set<String> roles = new HashSet<String>();

        //数据库查出某个用户的角色
        ArrayList<String> rolenames=userService.processGetRoleByUsername(principal.toString());

        System.out.println(rolenames.size());
        for (String r:rolenames){
            roles.add(r);
            System.out.println(rolenames);
        }
//
//        //如果时admin用户，则有crud权限
//        if("admin".equals(principal)){
//            System.out.println("1admin授权角色");
//            roles.add("role1");
//            roles.add("role3");
//            roles.add("role3");
//        }
        //3. 创建 SimpleAuthorizationInfo, 并设置其 reles 属性.
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
        //4. 返回 SimpleAuthorizationInfo 对象.
        return info;
    }
*/
   public static void main(String[] args) {
        String hashAlgorithmName = "MD5";
        Object credentials = "123456";
        Object salt = ByteSource.Util.bytes("18827552611");
        // 038bdaf98f2037b31f1e75b5b4c9b26e  admin
        // d6e75e048d00e4d0378d904bfdf81870  user1
        // 2fec513e98ba7033049be21d92f55fa3   2
        // 24f833a3f19020ee99cbedbfe911ed32   3
        // b4dbc873ae81f7ea06585a63059d5b33  user9
        int hashIterations = 1024;
        //注册时可调用该方法获取加密后的密码
        Object result = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);
        System.out.println(result);
    }

    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }
}
