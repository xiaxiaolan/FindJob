package main.com.findjob.realm;

import main.com.findjob.daoGraduation.UserMapperImp;
import main.com.findjob.pojoGraduation.Account_info;
import main.com.findjob.pojoGraduation.User;
import main.com.findjob.service.AccountService;
import main.com.findjob.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.springframework.stereotype.Controller;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Controller("SecondRealm")
public class SecondRealm extends AuthorizingRealm {
    @Resource(name = "accountService")
    AccountService accountService;

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(
            AuthenticationToken token) throws AuthenticationException {
        UserMapperImp userDAOImp=new UserMapperImp();
        System.out.println("[SsecondRealm]登录认证过程");
        UsernamePasswordToken upToken = (UsernamePasswordToken) token;
        String accountName = upToken.getUsername();

        System.out.println(accountName);

        //以下信息是从数据库中获取的.
        Account_info account_info= accountService.getAccountByName(accountName);

        System.out.println(account_info.toString());

        Object principal = accountName;
        Object credentials =account_info.getPassword();// "fc1709d0a95a6be30bc5926fdb7f22f4";123456

        //3). realmName: 当前 realm 对象的 name. 调用父类的 getName() 方法即可
        String realmName = getName();
        //4). 盐值.即使原始密码相同，存入数据库时也是不同的
        ByteSource credentialsSalt = ByteSource.Util.bytes(accountName);
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(principal, credentials,credentialsSalt, realmName);
        return info;
    }

    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    //授权会被 shiro 回调的方法
    //只有当点击某个需要授权的页面，才会进入授权方法进行判断有没有权限
    /*@Override
    protected AuthorizationInfo doGetAuthorizationInfo(
            PrincipalCollection principals) {
        //1. 从 PrincipalCollection 中来获取登录用户的信息
        Object principal = principals.getPrimaryPrincipal();
        Set<String> roles = new HashSet<String>();

        //数据库查出某个用户的角色
        ArrayList<String> rolenames=userService.processGetRoleByUsername(principal.toString());
        System.out.println(rolenames.size());
        for (String r:rolenames){
            roles.add(r);
            System.out.println(rolenames);
        }
//
//        //如果时admin用户，则有crud权限
//        if("admin".equals(principal)){
//            System.out.println("1admin授权角色");
//            roles.add("role1");
//            roles.add("role3");
//            roles.add("role3");
//        }
        //3. 创建 SimpleAuthorizationInfo, 并设置其 reles 属性.
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo(roles);
        //4. 返回 SimpleAuthorizationInfo 对象.
        return info;
    }*/


}
