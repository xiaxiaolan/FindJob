package main.com.findjob.realm;

import java.util.LinkedHashMap;

public class FilterChainDefinitionMapBuilder {

	public LinkedHashMap<String, String> buildFilterChainDefinitionMap(){
		LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        //加载静态资源
		map.put("/css/**", "anon");
		map.put("/easyui/**", "anon");
		map.put("/images/**", "anon");
		map.put("/img/**", "anon");
		map.put("/Scripts/**", "anon");
		map.put("/Styles/**", "anon");

		//登录界面和请求登录界面的地址不拦截
		map.put("/login.jsp", "anon");
		map.put("/accountLogin", "anon");
		map.put("/accountRegister", "anon");
		//验证码的获取和验证请求也不拦截
		map.put("/codeGenerate", "anon");
		//map.put("/codeGenerate?time=","anon");
		map.put("/identifyCode", "anon");
		//退出登录
		map.put("/userLogout", "logout");
		//配置前台jsp不拦截

		//访问和admin、shiro有关的方法，界面需要的权限
		/*map.put("/showShiro.jsp", "authc,roles[role1,role2,role3,role4]");
		map.put("/showAdmin.jsp", "authc,roles[role1,role2,role3,role4]");
		map.put("/showShiro", "authc,roles[role1,role2,role3,role4]");
		map.put("/showAdmin", "authc,roles[role1,role2,role3,role4]");
		map.put("/searchAdmin", "authc,roles[role1,role2,role3,role4]");
		map.put("/searchShiro", "authc,roles[role1,role2,role3,role4]");


		//除了搜索以外都需要权限
		//前端账号
		map.put("/addAccount", "authc,roles[role1]");
		map.put("/deleteAccount", "authc,roles[role2]");
		map.put("/updateAccount", "authc,roles[role3]");*/

		map.put("/**", "authc");
		return map;
	}
}
