<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<%String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";%>
	<base href="<%=basePath%>">

	<%--<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">--%>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>药品</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
	<%--模态框--%>
	<%--这个问题搞了半天主要是因为 我引用了eaeasyUi的包 又引用了jquery的包 --%>
	<link rel="stylesheet" href="ditu/bootstrap1.min.css">
	<script src="ditu/bootstrap.min.js"></script>

	<style type="text/css">
		/*分别定义HTML中和标记之的距离样式*/
		h1, form, fieldset, legend, ol, li {
			margin: 0;
			padding: 0;
		}
		/*定义内容的样式*/
		form#position {
			background:#CCCCCC;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			padding: 20px;
			width: 400px;
			margin:auto;
		}
		form#position fieldset {
			border: none;
			margin-bottom: 10px;
		}
		form#position fieldset:last-of-type { margin-bottom: 0; }
		form#position legend {
			color:#993399;
			font-size: 16px;
			font-weight: bold;
			padding-bottom: 10px;
			text-shadow: 0 1px 1px #c0d576;
		}

		form#position fieldset fieldset legend {
			color: #111111;
			font-size: 13px;
			font-weight: normal;
			padding-bottom: 0;
		}
		form#position ol li {
			background: #b9cf6a;
			background: rgba(255, 255, 255, .3);
			border-color: #e3ebc3;
			border-color: rgba(255, 255, 255, .6);
			border-style: solid;
			border-width: 2px;
			-webkit-border-radius: 5px;
			line-height: 30px;
			list-style: none;
			padding: 5px 10px;
			margin-bottom: 2px;
		}
		form#position ol ol li {
			background: none;
			border: none;
			float: left;
		}
		form#position label {
			float: left;
			font-size: 13px;
			width: 100px;
			vertical-align:top;
		}
		form#position fieldset fieldset label {
			background: none no-repeat left 50%;
			line-height: 20px;
			padding: 0 0 0 20px;
			width: auto;
		}
		form#position fieldset fieldset label:hover { cursor: pointer; }
		form#position input:not([type=radio]), form#payment textarea {
			background: #ffffff;
			border: #FC3 solid 1px;
			-webkit-border-radius: 3px;
			font: italic 13px Georgia, "Times New Roman", Times, serif;
			outline: none;
			padding: 5px;
			width: 200px;
		}
		form#position input:not([type=submit]):focus, form#payment textarea:focus {
			background: #eaeaea;
			border: #F00 solid 1px;
		}
		form#position input[type=radio] {
			float: left;
			margin-right: 0px;
			display:inline;
			vertical-align: top}
		form#position select{
			height:24px;
			width:120px;}
	</style>


	<style type="text/css">
		input[id="medname2"],input[id="factoryname2"],input[id="pici2"],input[id="status2"],input[id="function2"]{
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 130px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 45px;
		}
		input[id="medname2"]:hover,
		input[id="medname2"]:focus,
		input[id="factoryname2"]:hover,
		input[id="factoryname2"]:focus,
		input[id="pici2"]:hover,
		input[id="pici2"]:focus,
		input[id="status2"]:hover,
		input[id="status2"]:focus,
		input[id="function2"]:hover,
		input[id="function2"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>

	<style type="text/css">
		body{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>

<body>

<table id="dg" title="职位记录" class="easyui-datagrid" style="width:100%;height:100%"
	   url="showMedicine"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="false" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'medicineid',width:40,align:'center',sortable:true" sorter="numberSort">id</th>
		<th data-options="field:'medname',width:100,align:'center'">药品名称</th>
		<th data-options="field:'medsize',width:70,align:'center'">药品规格</th>
		<th data-options="field:'factoryname',width:100,align:'center'">生产厂家</th>
		<th data-options="field:'pici',width:40,align:'center'">批次号</th>
		<th data-options="field:'permissioncode',width:90,align:'center'">批准文号</th>
		<th data-options="field:'producedate',width:90,align:'center'">生产日期</th>
		<th data-options="field:'expirationdate',width:50,align:'center'">保质期</th>
		<th data-options="field:'content',width:100,align:'center'">成分</th>
		<th data-options="field:'function1',width:100,align:'center'">功能主治</th>
		<th data-options="field:'usage1',width:100,align:'center'">使用说明</th>
		<th data-options="field:'effect',width:90,align:'center'">不良反应</th>
		<th data-options="field:'notify',width:150,align:'center'">注意事项</th>
		<th data-options="field:'jinji',width:80,align:'center'">禁忌</th>
		<th data-options="field:'status',width:70,align:'center'">审核状态</th>
		<th data-options="field:'comid',width:70,align:'center',sortable:true">研发公司id</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:27px 20px 17px 25px;">
	<span>
		<input id="medname2" placeholder="药品名" name="medname2" onkeyup="mednameSearch()"/>
		<input id="function2" placeholder="功能主治" name="function2" onkeyup="functionSearch()"/>
        <input id="pici2" placeholder="批次" name="pici2" onkeyup="piciSearch()"/>
        <input id="factoryname2" placeholder="生产厂家" name="factoryname2" onkeyup="factorynameSearch()"/>

		<select id="status2"  onchange="statusSearch()" style="height: 33px;width: 80px;margin-bottom:5.6px;margin-left:43px;font-family:sans-serif;" >
			  <option value="" selected="selected"></option>
              <option value="通过">通过</option>
              <option value="未通过">未通过</option>
              <option value="待审核">待审核</option>
		</select>

		<a style="margin-left:43.5px;margin-top:5px" href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="searchall()">Search</a>
	    <a style="margin-left:20px;margin-top:5px" href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newPosition()">Add</a>
		<a style="margin-left:20px;margin-top:5px" href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editPosition()">Edit</a>
		<%--<a href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteAccount()">Detete</a>--%>
		<%--<button type="button" style="margin-left:20px;margin-top:5px" class="btn btn-xs btn-green" onclick="generateCode()">点击生成二维码</button>--%>
	</span>

</div>

<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons"></div>
<div id="dlg-buttons"></div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="newResource">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<%--head--%>
			<div class="modal-header">
				<h4 class="modal-title"  id="gridSystemModalLabel">修改资源</h4>
			</div>
			<%--body--%>
			<div class="modal-body">
				<div class="container-fluid">
					<form id="position" action="" method="post" name="position">
						<fieldset>
							<legend>药品信息</legend>
							<ol>
								<li style="position: absolute">
									<input id="medicineid" name="medicineid" type="hidden">
								</li>
								<li>
									<label for="medname">药品名：</label>
									<input id="medname" name="medname" type="text">
								</li>
								<li>
									<label for="medsize">规格：</label>
									<input id="medsize" name="medsize" type="text">
								</li>
								<li>
									<label for="factoryname">生产厂家：</label>
									<input id="factoryname"  name="factoryname" type="text">
								</li>
								<li>
									<label for="pici">批次号：</label>
									<input id="pici" name="pici" type="text" placeholder="如：3">
								</li>
								<li>
									<label for="permissioncode">批准文号：</label>
									<input id="permissioncode" name="permissioncode" type="text">
								</li>
								<li>
									<label for="producedate">生产日期：</label>
									<input id="producedate" name="producedate" type="text"  placeholder="年/月/日" required>
								</li>
								<li>
									<label for="expirationdate">保质期：</label>
									<input id="expirationdate" name="expirationdate" type="text" placeholder="如：12个月">
								</li>
								<li>
									<label for="content">成分：</label>
									<input id="content" name="content" type="text">
								</li>
								<li>
									<label for="function1">功能：</label>
									<input id="function1" name="function1" type="text">
								</li>
								<li>
									<label for="usage1">使用说明：</label>
									<input id="usage1" name="usage1" type="text">
								</li>
								<li>
									<label for="effect">不良反应：</label>
									<input id="effect" name="effect" type="text">
								</li>
								<li>
									<label for="notify">注意事项：</label>
									<input id="notify" name="notify" type="text">
								</li>
								<li>
									<label for="jinji">禁忌：</label>
									<input id="jinji" name="jinji" type="text">
								</li>
                                <%--<li>
                                    <label for="comid">公司id</label>
									<input id="comid" name="comid" type="text">
                                </li>--%>
								<%--<li>
									<label for="status">审核状态</label>
									<select name="status" id="status">
										<option value="通过">通过</option>
										<option value="未通过">未通过</option>
										<option value="待审核">待审核</option>
									</select>
								</li>--%>
							</ol>
						</fieldset>
						<%--<fieldset>
							<button type="submit">提交</button>
						</fieldset>--%>
					</form>
				</div>
			</div>
			<%--bottom保存取消按钮--%>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
				<button type="button" class="btn btn-xs btn-green" id="save" onclick="save()">提 交</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<script type="text/javascript">
    //弹出框add
    function newPosition(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length ==0) {
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("添加药品");
            $('form[id=position]').attr('action', "addMedicine");
            $('#position').form('clear');

            /*document.position.combasemsgid.disabled=false;//修改：combasemsgid不可编辑
            document.position.poitiontypeid.disabled=false;//修改：combasemsgid不可编辑

            //去后台请求公司账号id，添加职位用到
            var length1=document.getElementById("combasemsgid").length;
            var length2=document.getElementById("poitiontypeid").length;

            if (length1!=null){
                for (var i;i<=length1;i++) {
                    $("#combasemsgid").options.remove(i);
                }
            }
            if (length2!=null){
                for (var i;i<=length2;i++) {
                    $("#poitiontypeid").options.remove(i);
                }
            }
            $.get("selectAllCompanyId", function (data, status) {
                for(var i=0;i<data.length;i++){
                    $("#combasemsgid").append('<option value ="'+data[i]+'">'+data[i]+'</option>');
                }
            });
            $.get("selectAllPosirionTypeId", function (data, status) {
                for(var i=0;i<data.length;i++){
                    $("#poitiontypeid").append('<option value ="'+data[i]+'">'+data[i]+'</option>');
                }
            });*/
        }else {
            $.messager.alert("提示", "请清空选中的行", "info");
        }
    }

    //弹出框edit
    function editPosition(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的药品", "info");
        }else{
            $('#newResource').modal("show");
            $("#gridSystemModalLabel").text("修改药品信息");
            $('form[id=position]').attr('action',"updateMedicine");

            //document.position.medicineid.disabled=true;//可让下面赋值操作
            /*document.position.medname.disabled=true;
            document.position.medsize.disabled=true;
            document.position.factoryname.disabled=true;
            document.position.pici.disabled=true;
            document.position.permissioncode.disabled=true;
            document.position.producedate.disabled=true;
            document.position.expirationdate.disabled=true;
            document.position.content.disabled=true;
            document.position.function1.disabled=true;
            document.position.jinji.disabled=true;
            document.position.usage1.disabled=true;
            document.position.effect.disabled=true;
            document.position.notify.disabled=true;
            document.position.comid.disabled=true;*/

            //1、获取选中的行
            var id=rows[0].medicineid;
            //2、将存在session的值赋值给表单
            $.get("showMedicineById?medicineid=" + id, function (data, status) {
                //3、赋值给表单,必须通过name属性
                document.position.medicineid.value=id;
                document.position.medname.value=data.medname;
                document.position.medsize.value=data.medsize;
                document.position.factoryname.value=data.factoryname;
                document.position.pici.value=data.pici;
                document.position.permissioncode.value=data.permissioncode;
                document.position.producedate.value=data.producedate;
                document.position.expirationdate.value=data.expirationdate;
                document.position.content.value=data.content;
                document.position.function1.value=data.function1;
                document.position.jinji.value=data.jinji;
                document.position.usage1.value=data.usage1;
                document.position.effect.value=data.effect;
                document.position.notify.value=data.notify;
                //document.position.comid.value=data.comid;
                //$("#status option[value='"+data.status+"']").attr("selected","selected");
            });
        }
    }

	//弹出框generateCode
	/*function generateCode(){
		var rows = $('#dg').datagrid("getSelections");
		if (rows.length > 1) {
			$.messager.alert("提示", "请选择单行数据", "info");
		}else if(rows.length == 0) {
			$.messager.alert("提示", "请选择要生成二维码的数据行", "info");
		}else{
			$('#newResource').modal("show");
			$("#gridSystemModalLabel").text("生成二维码");
			$('form[id=position]').attr('action',"generateCode");

			//document.position.medicineid.disabled=true;//可让下面赋值操作
			document.position.medname.readOnly=true;
			document.position.medsize.readOnly=true;
			document.position.factoryname.readOnly=true;
			document.position.pici.readOnly=true;
			document.position.permissioncode.readOnly=true;
			document.position.producedate.readOnly=true;
			document.position.expirationdate.readOnly=true;
			document.position.content.readOnly=true;
			document.position.function1.readOnly=true;
			document.position.jinji.readOnly=true;
			document.position.usage1.readOnly=true;
			document.position.effect.readOnly=true;
			document.position.notify.readOnly=true;
			document.position.comid.readOnly=true;
			document.position.status.readOnly=true;

			//1、获取选中的行
			var id=rows[0].medicineid;
			//2、将存在session的值赋值给表单
			$.get("showMedicineById?medicineid=" + id, function (data, status) {
				//3、赋值给表单,必须通过name属性
				document.position.medicineid.value=id;
				document.position.medname.value=data.medname;
				document.position.medsize.value=data.medsize;
				document.position.factoryname.value=data.factoryname;
				document.position.pici.value=data.pici;
				document.position.permissioncode.value=data.permissioncode;
				document.position.producedate.value=data.producedate;
				document.position.expirationdate.value=data.expirationdate;
				document.position.content.value=data.content;
				document.position.function1.value=data.function1;
				document.position.jinji.value=data.jinji;
				document.position.usage1.value=data.usage1;
				document.position.effect.value=data.effect;
				document.position.notify.value=data.notify;
				document.position.comid.value=data.comid;
				$("#status option[value='"+data.status+"']").attr("selected","selected");
			});
		}
	}*/
//保存按钮
    //点击模态框的保存之后，请求后台并将这个模态框关闭
    function save() {
        $('#position').submit();
        $('#newResource').modal('hide');
        //存一个update/add返回的的值，判断，弹框
    }

//绑定搜索链接按钮点击事件，点击搜索时发送请求
    function searchall(){
        all();
    }
    //ajax多条件
    function all() {
        var medname = $("#medname2").val();
        var function2 = $("#function2").val();
        var pici = $("#pici2").val();
        var factoryname = $("#factoryname2").val();
		var status=$('#status2 option:selected').text();
        $("#dg").datagrid("options").url = "searchMedicineByConditions?medname="+medname+"&function2="+function2+"&pici="+ pici+"&factoryname="+factoryname+"&status="+status;
        $("#dg").datagrid('reload');
    }
    function mednameSearch(){
        all();
	}
    function functionSearch(){
        all();
	}
    function piciSearch(){
        all();
	}
    function factorynameSearch(){
        all();
    }
    function statusSearch(){
        all();
    }


    //删除的ajax请求
    function deleteAccount(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.medicineid);
            }
        });
        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {
                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
                    $.get("deletePosition?positionIds=" + userid, function (data, status) {
                        if (data == "true") {
                            $.messager.alert("温馨提示", "删除成功", 'info');
                            $("#dg").datagrid('reload');
                        }
                        if (data == "false") {
                            $.messager.alert("温馨提示", "删除失败", 'error');
                        }
                    });
                }
            })
        }else {
            $.messager.alert("温馨提示", "选择要删除的行", 'info');
		}
    }



    //自定义整数排序
    function numberSort(a,b){
        var number1 = parseFloat(a);
        var number2 = parseFloat(b);
        return (number1 > number2 ? 1 : -1);
    }
</script>


<%--返回 add/update  "${sessionScope.user.username}" 的消息--%>
<c:choose>
	<c:when  test="${updateMedicine  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改成功!','info');
		</script>
	</c:when>

	<c:when  test="${updateMedicine  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改失败!','error');
		</script>
	</c:when>

	<c:when  test="${addMedicine eq  'true'}">
		<script type="text/javascript">
			//当用户在后天验证登陆失败时存储一个字段，到这里判断
			$.messager.alert('温馨提示','添加成功!','info');
		</script>
	</c:when>

	<c:when  test="${addMedicine  eq  'false'}">
		<script type="text/javascript">
			//当用户在后天验证登陆失败时存储一个字段，到这里判断
			$.messager.alert('温馨提示','添加失败!','error');
		</script>
	</c:when>

	<c:when  test="${NoCompany  eq  'false'}">
		<script type="text/javascript">
			//当用户在后天验证登陆失败时存储一个字段，到这里判断
			$.messager.alert('温馨提示','先添加公司信息并等待审核通过!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>

</c:choose>
</body>
</html>