<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
		 pageEncoding="UTF-8"%>
<html>
<head>
	<%String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath()+"/";%>
	<base href="<%=basePath%>">

	<%--<base href="${pageContext.request.scheme }://${pageContext.request.serverName}:${pageContext.request.serverPort}">--%>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>公告</title>

	<link rel="stylesheet" type="text/css" href="easyui/themes/default/easyui.css" />
	<link rel="stylesheet" type="text/css" href="css/wu.css" />
	<link rel="stylesheet" type="text/css" href="css/icon.css" />
	<script type="text/javascript" src="easyui/jquery.min.js"></script>
	<script type="text/javascript" src="easyui/jquery.easyui.min.js"></script>
	<script type="text/javascript" src="easyui/locale/easyui-lang-zh_CN.js"></script>
	<%--模态框--%>
	<%--这个问题搞了半天主要是因为 我引用了eaeasyUi的包 又引用了jquery的包 --%>
	<link rel="stylesheet" href="ditu/bootstrap1.min.css">
	<script src="ditu/bootstrap.min.js"></script>

    <%--弹出框样式--%>
	<style type="text/css">
		/*分别定义HTML中和标记之的距离样式*/
		h1, form, fieldset, legend, ol, li {
			margin: 0;
			padding: 0;
		}
		/*定义内容的样式*/
		form#gonggao {
			background:#CCCCCC;
			-webkit-border-radius: 5px;
			border-radius: 5px;
			padding: 20px;
			width: 400px;
			margin:auto;
		}
		form#gonggao fieldset {
			border: none;
			margin-bottom: 10px;
		}
		form#gonggao fieldset:last-of-type { margin-bottom: 0; }
		form#gonggao legend {
			color:#993399;
			font-size: 16px;
			font-weight: bold;
			padding-bottom: 10px;
			text-shadow: 0 1px 1px #c0d576;
		}

		form#gonggao fieldset fieldset legend {
			color: #111111;
			font-size: 13px;
			font-weight: normal;
			padding-bottom: 0;
		}
		form#gonggao ol li {
			background: #b9cf6a;
			background: rgba(255, 255, 255, .3);
			border-color: #e3ebc3;
			border-color: rgba(255, 255, 255, .6);
			border-style: solid;
			border-width: 2px;
			-webkit-border-radius: 5px;
			line-height: 30px;
			list-style: none;
			padding: 5px 10px;
			margin-bottom: 2px;
		}
		form#gonggao ol ol li {
			background: none;
			border: none;
			float: left;
		}
		form#gonggao label {
			float: left;
			font-size: 13px;
			width: 110px;
		}
		form#gonggao fieldset fieldset label {
			background: none no-repeat left 50%;
			line-height: 20px;
			padding: 0 0 0 30px;
			width: auto;
		}
		form#gonggao fieldset fieldset label:hover { cursor: pointer; }
		form#gonggao input:not([type=radio]), form#gonggao textarea {
			background: #ffffff;
			border: #FC3 solid 1px;
			-webkit-border-radius: 3px;
			font: italic 13px Georgia, "Times New Roman", Times, serif;
			outline: none;
			padding: 5px;
			width: 200px;
		}
		form#account input:not([type=submit]):focus, form#payment textarea:focus {
			background: #eaeaea;
			border: #F00 solid 1px;
		}

	</style>

<%--搜索框的样式--%>
	<style type="text/css">
		input[id="tittle1"]{
			border-color: #bbb;
			height: 33px;
			font-size: 14px;
			border-radius: 2px;
			outline: 0;
			border: #ccc 1px solid;
			padding: 0 10px;
			width: 130px;
			-webkit-transition: box-shadow .5s;
			margin-bottom: 5px;
			margin-left: 15px;
		}

		input[id="tittle1"]:hover,
		input[id="tittle1"]:focus{
			border: 1px solid #56b4ef;
			box-shadow: inset 0 1px 3px rgba(0,0,0,.05),0 0 8px rgba(82,168,236,.6);
			-webkit-transition: box-shadow .5s;
		}
		input::-webkit-input-placeholder {
			color: #999;
			-webkit-transition: color .5s;
		}

		input:focus::-webkit-input-placeholder,  input:hover::-webkit-input-placeholder {
			color: #c2c2c2;
			-webkit-transition: color .5s;
		}
	</style>


	<style type="text/css">
		body{
			margin:0px;
			padding: 0px;
		}
	</style>
</head>

<body>

<table id="dg" title="公告" class="easyui-datagrid" style="width:100%;height:100%"
	   url="/showMessage"
	   toolbar="#tb" fitColumns="true" rownumbers="true" pagination="true"
	   singleSelect="false" onClickCell="onClickCell"  nowrap="false" remoteSort="false">
	<thead>
	<tr>
		<th field="ck" checkbox="true"></th>
		<th data-options="field:'id',width:50,align:'center',sortable:true" sorter="numberSort">id</th>
		<th data-options="field:'tittle',width:90,align:'center'">公告标题</th>
		<th data-options="field:'messae',width:350,align:'center'">公告内容</th>
		<th data-options="field:'pubtime',width:90,align:'center'">发布时间</th>
	</tr>
	</thead>
</table>

<div id="tb" style="width:100%;padding:27px 20px 17px 40px;">
	<span>
		<input id="tittle1" placeholder="公告标题" name="tittle1" onkeyup="tittleSearch()"/>
	</span>

	<a href="javascript:void(0)" id="search" class="easyui-linkbutton"  plain="true" iconCls="icon-search" onclick="allSearch()">Search</a>
	<a href="javascript:void(0)" id="delete" class="easyui-linkbutton"  plain="true"  iconCls="icon-cut"onclick="deleteAccount()">Detete</a>
	<a href="javascript:void(0)" id="add" class="easyui-linkbutton"  plain="true"  iconCls="icon-add" onclick="newAccount()">Add</a>
	<a href="javascript:void(0)" id="edit" class="easyui-linkbutton"  plain="true"  iconCls="icon-edit" onclick="editAccount()">Edit</a>
</div>

<div id="dlg" class="easyui-dialog" style="width:440px;height:500px;padding:3px;padding-right: 8px" closed="true" buttons="#dlg-buttons"></div>
<div id="dlg-buttons"></div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel" id="newResource">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<%--head--%>
			<div class="modal-header">
				<h4 class="modal-title"  id="gridSystemModalLabel">修改资源</h4>
			</div>
			<%--body--%>
			<div class="modal-body">
				<div class="container-fluid">
					<form id="gonggao" action="" name="gonggao" method="post">
						<fieldset>
							<legend>添加公告</legend>
							<ol>
								<li style="position: absolute">
									<input id="id" name="id" type="hidden">
								</li>
								<li>
									<label for="tittle">公告标题：</label>
									<input id="tittle" name="tittle" type="text" placeholder="请输入标题" required >
								</li>
								<li>
									<label for="pubtime">上传时间：</label>
									<input id="pubtime" name="pubtime" type="text"  placeholder="年/月/日" required>
								</li>
								<li>
									<label for="messae">公告内容：</label>
									<%--<textarea id="messae" name="messae" style="height:100px;" required/>--%>
									<input id="messae" name="messae" rows="2" cols="20" wrap="hard"
										   style="height:40px;" required>
								</li>
							</ol>
						</fieldset>
						<%--<fieldset>
							<button type="submit" value="submit">提交</button>
						</fieldset>--%>
					</form>
				</div>
			</div>
			<%--bottom保存取消按钮--%>
			<div class="modal-footer">
				<button type="button" class="btn btn-xs btn-white" data-dismiss="modal">取 消</button>
				<button type="button" class="btn btn-xs btn-green" id="save" onclick="save()">保 存</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script type="text/javascript">
//弹出框add
    function newAccount(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length ==0) {
            $('#newResource').modal('show');
            $("#gridSystemModalLabel").text("添加公告");
            $('form[id=gonggao]').attr('action',"/addMessage");
            $('#gonggao').form('clear');
        }else {
            $.messager.alert("提示", "请清空选中的行", "info");
        }
    }
//弹出框edit
    function editAccount(){
        var rows = $('#dg').datagrid("getSelections");
        if (rows.length > 1) {
            $.messager.alert("提示", "请选择单行用户进行修改", "info");
        }else if(rows.length == 0) {
            $.messager.alert("提示", "请选择要修改的用户", "info");
        }else{
            $('#newResource').modal();
            $("#gridSystemModalLabel").text("修改职位信息");
            $('form[id=gonggao]').attr('action',"/updateMessage");
            //1、获取选中的行
            var id=rows[0].id;
            //2、将存在session的值赋值给表单
            $.get("showMessageById?messageid=" + id, function (data, status) {
                //3、赋值给表单,必须通过name属性pubtime title messae
                document.gonggao.id.value=id;
                document.gonggao.pubtime.value=data.pubtime;
                document.gonggao.tittle.value=data.tittle;
                document.gonggao.messae.value=data.messae;
            });
        }
    }
	//点击模态框的保存之后，请求后台并将这个模态框关闭
	function save() {
		$('#gonggao').submit();
		$('#newResource').modal('hide');
		//存一个update/add返回的的值，判断，弹框
	}

//绑定搜索链接按钮点击事件	，点击搜索时发送请求
    function tittleSearch(){
        var tittle = $("#tittle1").val();
        $("#dg").datagrid("options").url = "/searchMessageByConditions?tittle=" + tittle;
        $("#dg").datagrid('reload');
    }
//动态匹配的搜索
    function allSearch() {
        tittleSearch();
    }

//删除的ajax请求
    function deleteAccount(){
        var checkedItems = $("#dg").datagrid('getChecked');
        var userid = [];
        $.each(checkedItems, function (index, item) {
            if (item == "" || item == null) {

            } else {
                userid.push(item.id);
            }
        });
        if (userid.length > 0) {
            $.messager.confirm("提示", "你确定要删除数据吗?", function (r) {
                if (r) {
                    if ("admin"!="${sessionScope.user.username}") {
                        $.messager.alert("温馨提示", "您没有操作删除权限", 'info');
                    }
                    $.get("deleteMessage?messageIds=" + userid, function (data, status) {
                        if (data == "true") {
                            $.messager.alert("温馨提示", "删除成功", 'info');
                            $("#dg").datagrid('reload');
                        }
                        if (data == "false") {
                            $.messager.alert("温馨提示", "删除失败", 'error');
                        }
                    });
                }
            })
        }else {
            $.messager.alert("温馨提示", "请选择要删除的行", 'info');
		}
    }

	//自定义整数排序
	function numberSort(a,b){
		var number1 = parseFloat(a);
		var number2 = parseFloat(b);
		return (number1 > number2 ? 1 : -1);
	}

</script>

<%--返回 add/update  "${sessionScope.user.username}" 的消息--%>
<c:choose>
	<c:when  test="${addMessage  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加成功!','info');
		</script>
	</c:when>

	<c:when  test="${addMessage  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','添加失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>


<c:choose>
	<c:when  test="${updateMessage  eq  'true'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改成功!','info');
		</script>
	</c:when>

	<c:when  test="${updateMessage  eq  'false'}">
		<script type="text/javascript">
            //当用户在后天验证登陆失败时存储一个字段，到这里判断
            $.messager.alert('温馨提示','修改失败!','error');
		</script>
	</c:when>
	<c:otherwise></c:otherwise>
</c:choose>

</body>
</html>